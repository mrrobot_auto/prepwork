package geeksforgeeks;

public class LargestInArrays {
    public static void main(String[] args) {
        int[] o = {1, 99, 1000, 121, 2, 2, 3, 7};
        System.out.println(largest(o, 8));

    }

    public static int largest(int[] arr, int n) {
        int large = 0;

        if (n == 0) return Math.max(large,arr[0]);
        if (arr[n - 1] >= arr[Math.max(0, n - 2)]) {
            large = arr[Math.max(0, n - 1)];
        }
        return Math.max(large, largest(arr, n - 1));
    }
}

