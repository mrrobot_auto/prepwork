package geeksforgeeks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class GetPairsCount {
    public static void main(String[] args) {
        int[] arr = {1, 5, 7, 1, 6, 0, 1, 2};
        int n = 8;
        int k = 6;
        new GetPairsCount().getPairsCount(arr, n, k);
    }

    int getPairsCount(int[] arr, int n, int k) {
        int count = 0;
        HashMap<Integer, Integer> mp = new HashMap<>();
        for (int i = 0; i < n; i++) {
            if (mp.containsKey(k - arr[i]))
                count += mp.get(k - arr[i]);

            mp.put(arr[i], mp.getOrDefault(arr[i], 0) + 1);
        }

        return count;


    }
}
