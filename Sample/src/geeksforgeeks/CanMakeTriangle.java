package geeksforgeeks;

public class CanMakeTriangle {
    int[] canMakeTriangle(int A[], int N)
    {
        // code here
        if(A.length<2) return new int[]{-1};
        int[] res = new int[N-2];
        for(int i = 0; i<N-2 ; i++){
            int a = A[i];
            int b = A[i+1];
            int c = A[i+2];
            if(a+b>c && a+c>b && b+c>a){
                res[i] = 1;
            }else{
                res[i] = 0;
            }
        }
        return res;
    }
}
