package geeksforgeeks;

import java.util.Arrays;
import java.util.List;

public class MoveRobots {
    public static void main(String[] args) {
        moveRobots( "AA#AA#BB#ABA#BBAA#BB###BBBAABBA##BAA#ABA#BBBB##A#A#AAABBBB#B##A###A#A#ABBA#BBBAA#AAABB##B#B###BAA#AA",
                    "AAAA##B#BABA#BBAA#B###BBBBAABBA##BAAA#BA#BBB##BAAAAA##BBB#B##BAAAA#####BBA#BBBAAAAA#B##B#B###BBAAAA#");
    }

    public static String moveRobots(String s1, String s2) {
        String res = "No";
        char[] s1s = s1.toCharArray();
        char[] s2s = s2.toCharArray();
        int index = 0;
        int it = 0;
        while (it<s1.length()) {
            if (index < s1.length()-1 && s1s[index] == 'B' && s1s[index + 1] != 'A' && s1s[index + 1] != 'B') {
                swap(s1s, index, index + 1);
            } else if (index > 0 && s1s[index] == 'A' && s1s[index - 1] != 'B' && s1s[index - 1] != 'A') {
                swap(s1s, index, index - 1);
            }
            index++;
            if(index>=s1.length()){
                index = 0;
                it++;
            }
            if(Arrays.equals(s1s,s2s)) {
                res = "Yes";
                break;
            }
        }
        return res;
    }

    static void swap(char[] arr, int i, int j) {
        char temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static List<Integer> checkDiff(char[] chars1, char[] chars2, List<Integer> list) {
        for (int i = 0; i < chars1.length; i++) {
            if (chars1[i] != chars2[i]) {
                list.add(i);
            }
        }
        return list;
    }
}
