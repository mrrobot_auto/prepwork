package geeksforgeeks;

import java.util.Arrays;
import java.util.Comparator;

public class MaximumSelections {
    public static void main(String[] args) {
        int[][] ranges = {{10,16},
                {2,8},
                {1,3},
                {10,11},
                {7,9}};
        max_non_overlapping(ranges, 5);
    }

    static int max_non_overlapping(int ranges[][], int n) {
        // code here
        Arrays.sort(ranges, Comparator.comparingInt(o -> o[1]));
        int count = 0;
        int maxcount = 0;
        int comparer;
        for (int i = 0; i < n; i++) {
            comparer = ranges[i][1];
            for (int j = 0; j < n; j++) {
                if (i != j && comparer <= ranges[j][0]) {
                    count++;
                    comparer = ranges[j][1];
                }
            }
            maxcount = Math.max(count, maxcount);
            count = 0;
        }
        return maxcount + 1;
    }
}
