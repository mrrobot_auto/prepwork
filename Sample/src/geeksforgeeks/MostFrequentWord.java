package geeksforgeeks;

/**
 * @TAG : TRIE DS
 */
public class MostFrequentWord {
    public static void main(String[] args) {
        mostFrequentWord(new String[]{"budxdu", "budxdu", "akfwn", "akfwn", "budxdu", "akfwn", "suoko", "akfwn", "budxdu", "dhxeg", "suoko", "akfwn", "dhxeg"}, 13);
    }

    public static String mostFrequentWord(String[] arr, int n) {
        // code here
        String res = arr[0];
        int max_f = 0;
        String maxRes = "";
        int frequency = 1;
        for (int i = 1; i < n; i++) {
            if (res.equals(arr[i])) {
                frequency++;
                int temp = max_f;
                max_f = Math.max(max_f, frequency);
                if(temp!=max_f){
                    maxRes = res;
                }
            } else {
                frequency--;
            }
            if (frequency <= 0) {
                res = arr[i];
                frequency = 1;
                if(max_f<1){
                    maxRes = res;
                }
            }
        }
        return maxRes;/*
        Map<String,Integer> map = new HashMap<>();
        for(String s : arr){
            if(map.containsKey(s)){
                int t = map.get(s);
                map.put(s,t+1);
            }else{
                map.put(s,1);
            }
        }
        return map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).findFirst().get().getKey();*/
    }
}
