package geeksforgeeks;

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode nextRight;

    public TreeNode() {
    }

    public TreeNode(int val) {
        this.val = val;
        left = null;
        right = null;
    }
    public TreeNode(int val, int left, int right) {
        this.val = val;
        this.left = new TreeNode(left);
        this.right = new TreeNode(right);
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
