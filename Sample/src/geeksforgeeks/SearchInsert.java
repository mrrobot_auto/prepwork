package geeksforgeeks;

public class SearchInsert {
    public static void main(String[] args) {
        new SearchInsert().searchInsert(new int[]{1, 3, 5, 6}, 2);
    }

    public int searchInsert(int[] nums, int target) {
        int mid = nums.length / 2;
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            if (nums[mid] == target) {
                return mid;
            }
            if (target > nums[mid]) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
            mid = (left + right) / 2;
        }
        return left;
    }
}
