package geeksforgeeks;

public class TransitionPoint {
    public static void main(String[] args) {
        transitionPoint(new int[]{0,0},2);
    }
    static int transitionPoint(int[] a, int n) {
        // code here
        if(n == 1){
            if(a[0] == 0){
                return -1;
            }else{
                return 0;
            }
        }
        int i = 0;
        int j = 1;
        while(j<n){
            if(a[i] == a[j]){
                i++;
                j++;
            }else{
                return j;
            }
            if(j >= n){
                if(a[i] == 0){
                    return -1;
                }else{
                    return 0;
                }
            }
        }
        return -1;
    }
}
