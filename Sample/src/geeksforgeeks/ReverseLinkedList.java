package geeksforgeeks;


public class ReverseLinkedList {
    public static void main(String[] args) {
        Node node = new Node(1);
        node.next = new Node(2);
        node.next.next = new Node(3);
        node.next.next.next = new Node(4);
        node.next.next.next.next = new Node(5);
        node.next.next.next.next.next = new Node(6);
        //node.next.next.next.next.next.next = new Node(10);
        //getNthFromLast(node,2);
        //System.out.println(getMiddle(node));
        //System.out.println(getMiddleNode(node));
        node = reverse(node);
        /*while (node != null) {
            System.out.println(node.value);
            node = node.next;
        }*/
    }

    static int getMiddle(Node head) {
        int count = 0;
        Node temp = head;
        while (temp != null) {
            temp = temp.next;
            count++;
        }
        int n = 0;
        while (head != null) {
            head = head.next;
            n++;
            if (n >= count / 2) break;
        }

        return head.value;
    }

    static int getMiddleNode(Node head) {
        Node slow = head;
        Node fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow.value;
    }

    static int getNthFromLast(Node head, int n) {
        // Your code here
        Node reversedList = reverseLinkedLinked(head);
        int limit = 1;
        while (reversedList != null && limit <= n) {
            reversedList = reversedList.next;
            limit++;
        }
        return reversedList.value;
    }

    public static Node reverseLinkedLinked(Node node) {
        Node prev = null;
        Node curr = node;
        Node next;
        while (curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }

    public static Node reverse(Node head) {
        Node currentNode = head;
        Node previousNode = null;
        Node temporaryNode = null;
        while (currentNode != null) {
            temporaryNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = temporaryNode;
        }
        return previousNode;
    }
}
