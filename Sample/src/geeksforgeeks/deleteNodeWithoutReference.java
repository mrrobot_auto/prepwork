package geeksforgeeks;

public class deleteNodeWithoutReference {
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5};
        Node1 head = null;
        for (int o : a) {
            if (head == null) {
                head = new Node1(o);
            } else {
                Node1 n = head;
                head = new Node1(o);
                head.next = n;
            }
        }

        print(head);
        Node1 del = head.next.next;
        deleteNode(del);
        System.out.println();
        print(head);
    }

    public static void deleteNode(Node1 del) {
        if(del == null) System.out.println("Node to delete is null.");
        Node1 temp = del.next;
        temp.value = del.value;
        del = temp;
        temp = null;
        //del.value = del.next.value;
        /*while(del.next == null){
            del = del.next;
        }*/
//        Node n = new Node(del.next.value);
//        n = del.next;
//        del = n;
    }

    public static void print(Node1 head) {
        Node1 temp = head;
        while (temp != null) {
            System.out.println(temp.value);
            temp = temp.next;
        }
    }

    }


class Node1 {
    int value;
    Node1 next;
    Node1(){};
    Node1(int value) {
        this.value = value;
        this.next = null;
    }
}
