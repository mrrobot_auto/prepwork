package geeksforgeeks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

public class MajorityElement {
    public static void main(String[] args) throws IOException {
        List<String> lines = Files.readAllLines(Path.of("C:\\Users\\z003v0kh\\Downloads\\fileInput (5).txt"));
        int[] arr = new int[Integer.parseInt(lines.get(0))];
        String[] is = lines.get(1).split(" ");
        int index = 0;
        for (String h : is) {
            arr[index] = Integer.parseInt(h);
            index++;
        }
        //majorityElement(arr, Integer.parseInt(lines.get(0)));
        majorityElement(new int[]{3,1,3,3,2,6,7,8,9,0},10);
    }

    static int majorityElement(int a[], int size) throws IOException {
        int res = 0;
        int len = 0;
        int count = 1;
        for (int i = 1; i < a.length; i++) {
            if (a[res] == a[i]) {
                count++;
            } else {
                count--;
            }
            
            if (count <= 0) {
                res = i;
                count = 1;
            }
        }
        for (int j : a) {
            if (j == a[res]) {
                len++;
            }
        }
        return len > (a.length / 2) ? a[res] : -1;
    }
}
