package geeksforgeeks;

public class DetectLoopInLL {
    public static void main(String[] args) {

    }
    public static boolean detectLoop(Node head){
        // Add code here
        Node fast,slow;
        fast=head.next;
        slow=head;
        boolean flag=false;
        while(fast!=null && fast.next!=null)
        {
            fast=fast.next.next;
            slow=slow.next;
            if(fast==slow)
            {
                flag=true;
                break;
            }
        }
        return flag;
        // Set<Node> set = new HashSet<>();
        // while(head != null){
        //     if(!set.add(head)){
        //         return true;
        //     }
        //     head = head.next;
        // }
        // return true;
    }
}
