package geeksforgeeks;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class FirstNonRepeatingCharacter {
    public static void main(String[] args) {
        nonrepeatingCharacter("hello");
    }
    static char nonrepeatingCharacter(String S)
    {
        //Your code here
        Set<Character> set = new LinkedHashSet<>();
        Set<Character> unwanted = new HashSet<>();
        for(char c : S.toCharArray()){
            if(!unwanted.contains(c) && !set.add(c)){
                set.remove(c);
                unwanted.add(c);
            }
        }
        return set.size()>0?set.stream().findFirst().get():'$';
    }
}
