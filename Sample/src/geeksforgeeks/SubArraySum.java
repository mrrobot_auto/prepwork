package geeksforgeeks;

import java.util.ArrayList;

public class SubArraySum {
    public static void main(String[] args) {

        int[] li = {135, 101, 170, 125, 79, 159, 163, 65, 106, 146, 82, 28, 162, 92, 196, 143, 28, 37, 192, 5, 103, 154, 93,
                183, 22, 117, 119, 96, 48, 127, 172, 139, 70, 113, 68, 100, 36, 95, 104, 12, 123, 134};
        subarraySum(li, 42, 468);
    }

    static ArrayList<Integer> subarraySum(int[] arr, int n, int s) {
        // Your code here
        ArrayList<Integer> list = new ArrayList<>();
        int l = 0;
        int r = 1;
        int sum = arr[l];
        while (l <n) {
            if (sum == s) {
                list.add(++l);
                list.add(r);
                break;
            }
            if (sum < s && r<n ) {
                sum += arr[r];
                r++;
            } else {
                sum -= arr[l];
                l++;
            }

        }
        return list;
    }
}
