package geeksforgeeks.graph;

import DataStructure.Graph.AdjacencySetWeightedGraph;
import DataStructure.Graph.WeightedGraph;

import java.util.*;

public class dijkstra {
    public static void main(String[] args) {
        WeightedGraph g = new AdjacencySetWeightedGraph(WeightedGraph.GraphType.UNDIRECTED, 7);
        g.addEdge(0, 1, 1);
        g.addEdge(0, 4, 2);
        g.addEdge(4, 2, 5);
        g.addEdge(2, 3, 3);
        g.addEdge(2, 5, 6);
        g.addEdge(3, 4, 4);
        g.addEdge(3, 6, 7);
        g.addEdge(5, 6, 8);
       /* IntStream.rangeClosed(0,6).forEach(i -> {
            System.out.println("source = "+i);
            g.getAdjacentVertices(i).forEach(node -> System.out.println("Destination : "+node.destination +" ; Weight : "+node.weight));
        });
*/
        new dijkstra().shortestPath(7, g, 0, 6);
    }

    public List<Integer> shortestPath(int V, WeightedGraph g, int source, int destination) {
        Map<Integer, WeightedGraph.Node> parentMap = new HashMap<>();
        Map<Integer, Integer> valueMap = new HashMap<>();
        Queue<Integer> q = new LinkedList<>();
        q.offer(source);
        boolean[] visited = new boolean[V];
        WeightedGraph.Node recentParent = new WeightedGraph.Node(0, 0);
        parentMap.put(source, recentParent);
        int distance = 0;
        valueMap.put(source, distance);
        while (!q.isEmpty()) {
            int n = q.poll();
            if (!visited[n]) {
                visited[n] = true;
                for (WeightedGraph.Node node : g.getAdjacentVertices(n)) {
                    if (!visited[node.destination]) {
                        q.offer(node.destination);
                        if (parentMap.containsKey(node.destination) && valueMap.get(node.destination) + parentMap.get(node.destination).weight < valueMap.get(node.destination)) {
                            parentMap.put(node.destination, new WeightedGraph.Node(n, node.weight));
                            valueMap.put(node.destination, valueMap.get(node.destination) + node.weight);
                            continue;
                        }
                        parentMap.put(node.destination, new WeightedGraph.Node(n, node.weight));
                        valueMap.put(node.destination, distance + node.weight);
                        distance += node.weight;

                    }
                }

            }
        }
        return null;
    }
}