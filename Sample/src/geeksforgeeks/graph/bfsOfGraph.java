package geeksforgeeks.graph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class bfsOfGraph {
    public static void main(String[] args) throws IOException {
        ArrayList<Integer> l1 = new ArrayList<>(List.of(1, 2));
        ArrayList<Integer> l2 = new ArrayList<>(List.of(4));
        ArrayList<ArrayList<Integer>> input = new ArrayList<>();
        input.add(l1);
        input.add(new ArrayList<>());
        input.add(new ArrayList<>());
        input.add(l2);
        input.add(new ArrayList<>());
        new bfsOfGraph().bfs(5, input);
    }


    public ArrayList<Integer> bfs(int V, ArrayList<ArrayList<Integer>> adj) {
        ArrayList<Integer> res = new ArrayList<>();
        Queue<Integer> q = new LinkedList<>(); //queue of units to be processed
        boolean[] visited = new boolean[V]; //status of processed/unprocessed units
        int index = 0;
        q.offer(index); //First index is 0
        visited[index] = true; //0 is first to be processed
        while (!q.isEmpty()) {
            int p = q.poll(); // out for process
            if (visited[p]) {
                res.add(p); // process
                for (int n : adj.get(p)) { // get all connected units
                    if (!visited[n]) {// make sure that already processed unit is not added in the queue again
                        q.offer(n); // add units in queue to process
                        visited[n] = true; // mark an unit as considered for processing queue
                    }
                }
            }
        }
        return res;
    }
}
