package geeksforgeeks.graph;

import DataStructure.Graph.AdjacencySetGraph;
import DataStructure.Graph.Graph;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

public class dfsForGraph {
    public static void main(String[] args) {
        Graph g = new AdjacencySetGraph(Graph.GraphType.UNDIRECTED, 5);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(0, 3);
        g.addEdge(2, 4);
        ArrayList<ArrayList<Integer>> adj = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            adj.add(new ArrayList<>());
        }
        for (int i = 0; i < 5; i++) {
            for (int n : g.getAdjacentVertices(i)) {
                adj.get(i).add(n);
            }
        }
        new dfsForGraph().dfs(5, adj);
    }

    public ArrayList<Integer> dfsRec(int V, int index, ArrayList<ArrayList<Integer>> adj, boolean[] visited, ArrayList<Integer> res) {
        if (index > V) return res;
        if (!visited[index]) {
            visited[index] = true;
            res.add(index);
            for (int n : adj.get(index)) {
                dfsRec(V, n, adj, visited, res);
            }
        }
        return res;
    }

    public ArrayList<Integer> dfs(int V, ArrayList<ArrayList<Integer>> adj) {
        boolean[] visited = new boolean[V];
        int index = 0;
        ArrayList<Integer> res = new ArrayList<>();
        return dfsRec(V, index, adj, visited, res);
    }

    public ArrayList<Integer> dfsOfGraph(int V, ArrayList<ArrayList<Integer>> adj) {
        // Code here
        ArrayList<Integer> res = new ArrayList<>();
        Deque<Integer> stack = new LinkedList<>();
        boolean[] visited = new boolean[V];
        stack.push(0);
        visited[0] = true;
        while (!stack.isEmpty()) {
            int p = stack.pop();
            if (visited[p]) {
                res.add(p);
                for (int n : adj.get(p)) {
                    if (!visited[n]) {
                        stack.push(n);
                        visited[n] = true;
                    }
                }
            }
        }
        return res;
    }
}
