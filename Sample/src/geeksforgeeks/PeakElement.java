package geeksforgeeks;

import java.util.Arrays;

public class PeakElement {
    public static void main(String[] args) {
        peakElement(new int[]{1,2,3},3);
    }

    public static int peakElement(int[] arr, int n) {
        //add code here.
       /*int max = Integer.MIN_VALUE;
       for(int i = 0; i< n ; i++){

       }*/
        int[] brr = Arrays.copyOf(arr, n);
        Arrays.sort(brr);
        for (int i = 0; i < n; i++) {
            if(arr[i] == brr[n-1]){
                return i;
            }
        }
        return -1;
    }
}
