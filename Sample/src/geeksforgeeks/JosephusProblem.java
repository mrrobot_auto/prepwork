package geeksforgeeks;

public class JosephusProblem {
    public static void main(String[] args) {
        int[] arr = {0, 1, 2, 3, 4, 5};
        //System.out.println(whoIsAlive(arr, 0, 1));
        System.out.println(winner(5, 2));
    }

    public static int whoIsAlive(int[] arr, int start, int skip) {
        if (start >= arr.length) {
            start = start % arr.length;
        }
        if (arr[start] == -1) {
            start++;
            return whoIsAlive(arr, start, skip);
        }
        if (skip >= arr.length) {
            return arr[skip % arr.length] == -1 ? whoIsAlive(arr, start, skip + 1) : arr[skip % arr.length];
        }
        if (arr[(start + skip) % arr.length] == -1) {
            skip++;
            return whoIsAlive(arr, start, skip);
        }
        if (arr[(start + skip) % arr.length] != -1) {
            arr[(start + skip) % arr.length] = -1;
            start = start + skip + 1;
            return whoIsAlive(arr, start, skip);
        }
        return -2;
    }

    public static int winner(int n, int k) {
        if(n==1) return 1;
        return (winner(n - 1, k) + k - 1) % n + 1;
    }
}