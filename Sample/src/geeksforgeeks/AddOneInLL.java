package geeksforgeeks;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AddOneInLL {
    public static void main(String[] args) {
        Node node = new Node(8);
        node.next = new Node(5);
        node.next.next = new Node(2);
        Node out = null;
        Node res = null;
        List<Integer> list = new ArrayList<>(Arrays.asList(3,5,8,7,9));
        for(int i : list){
            if(res == null){
                res = new Node(i);
                out = res;
            }else{
                while(res.next != null){
                    res = res.next;
                }
                res.next = new Node(i);
            }
        }
        System.out.println();
        addOne(node);
    }
    public static Node addOne(Node head)
    {
        //code here.
        Node rev = reverse(head);
        BigInteger multiplier = BigInteger.ONE;
        BigInteger sum = BigInteger.ZERO;
        while(rev != null){
            sum = sum.add(multiplier.multiply(BigInteger.valueOf(rev.value)));
            multiplier = multiplier.multiply(BigInteger.valueOf(10));
            rev = rev.next;
        }
        sum = sum.add(BigInteger.valueOf(1));
        multiplier = BigInteger.ONE;
        Node dig = null;
        Node res = null;
        while(!sum.equals(BigInteger.ZERO)){
            if(res == null){
                res = new Node(sum.mod(BigInteger.valueOf(10)).intValue());
                dig = res;
            }else{
                while(res.next != null){
                    res = res.next;
                }
                res.next = new Node(sum.mod(BigInteger.valueOf(10)).intValue());
            }
            sum =sum.divide(BigInteger.valueOf(10));
        }
        return reverse(dig);
    }
    public static Node reverse(Node node){
        Node prev = null;
        Node curr = node;
        Node next;
        while (curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }
}
