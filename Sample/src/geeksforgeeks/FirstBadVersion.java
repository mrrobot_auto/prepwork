package geeksforgeeks;

public class FirstBadVersion {
    public final int badVersion = 1702766719;
    public static void main(String[] args) {
        new FirstBadVersion().firstBadVersion(2126753390);
    }
    public int firstBadVersion(int n) {
        long mid = n / 2;
        long left = 0;
        long right = n;
        while (left <= right) {
            System.out.println(mid);
            if(isBadVersion((int)mid) && !isBadVersion((int)mid-1)){
                return (int)mid;
            }
            if (!isBadVersion((int)mid)) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
            mid = (left + right) / 2;
        }
        return 1;
    }

    private boolean isBadVersion(int mid) {
        return mid >= badVersion;
    }
}