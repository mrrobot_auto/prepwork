package geeksforgeeks;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class PrimeDivision {
    public static void main(String[] args) {
        primeDivision(Integer.parseInt("70")).forEach(System.out::println);
    }

    static List<Integer> primeDivision(int N) {
        // code here
        if (isPrime(N)) {
            System.out.println(N + " is a prime.");
            return new ArrayList<>(List.of(N));
        }
        List<Integer> res = new ArrayList<>();
        int[] primes = IntStream.rangeClosed(2, N).filter(PrimeDivision::isPrime).toArray();
        int l = 0;
        int r = primes.length - 1;
        while (l <= r) {
            if (primes[l] + primes[r] == N) {
                res.add(primes[l]);
                res.add(primes[r]);
                break;
            } else if (primes[l] + primes[r] < N) {
                l++;
            } else {
                r--;
            }

        }
        return res;
    }

    static boolean isPrime(int n) {
        return IntStream.rangeClosed(2, (int) Math.sqrt(n)).noneMatch(num -> n % num == 0);
    }
}
