package geeksforgeeks;

public class PalindromeLinkedList {
    public static void main(String[] args) {
        Node node = new Node(8);
        node.next = new Node(5);
        node.next.next = new Node(2);
        node.next.next.next = new Node(2);
        node.next.next.next.next = new Node(5);
        node.next.next.next.next.next = new Node(8);
        isPalindrome(node);
    }
    static boolean isPalindrome(Node head)
    {
        //Your code here
        Node mid =  reverse(getMiddle(head));
        while (mid != null) {
            if(head.value != mid.value){
                return false;
            }
            head = head.next;
            mid = mid.next;
        }

        return true;
    }
    static Node getMiddle(Node head){
        Node slow = head;
        Node fast = head;
        while(fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
    static Node reverse(Node head){
        Node prev = null;
        Node curr = head;
        Node next;
        while (curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }
}
