package geeksforgeeks;

public class FinMaximumSumOfNonAjdacentElements {
    public static void main(String[] args) {
        System.out.println(new FinMaximumSumOfNonAjdacentElements().maxSum(new int[]{5, 5, 10, 100, 10, 5}));
    }

    int findMaxSum(int arr[], int n) {
        // code here
        int index = 0;
        int increment = index;
        int shift = 2;
        int sum = 0;
        int tSum = 0;
        while (shift < n) {
            tSum += arr[index + increment];
            System.out.print(arr[index + increment] + " + ");
            increment = shift + increment;
            if (index + increment >= n) {
                System.out.print(" = " + tSum);
                System.out.println();
                increment = 0;
                index++;
                if (index == n - 1) {
                    shift++;
                    index = 0;
                }
                sum = Math.max(sum, tSum);
                tSum = 0;
            }
        }
        return sum;
    }

    int findMax(int[] arr, int n) {
        // code here
        int[] dp = new int[n];
        dp[0] = arr[0];
        if (n > 1) {
            dp[1] = Math.max(arr[0], arr[1]);
            int max = Math.max(arr[0], arr[1]);

            for (int i = 2; i < n; i++) {
                max = Math.max(dp[i - 2] + arr[i], max);
                dp[i] = max;
            }
        }
        return dp[n - 1];
    }
    public int maxSum(int arr[]) {
        int excl = 0;
        int incl = arr[0];
        for (int i = 1; i < arr.length; i++) {
            int temp = incl;
            incl = Math.max(excl + arr[i], incl);
            excl = temp;
        }
        return incl;
    }
}