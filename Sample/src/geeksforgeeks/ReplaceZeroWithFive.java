package geeksforgeeks;

import java.util.Arrays;

public class ReplaceZeroWithFive {
    public static void main(String[] args) {
        new ReplaceZeroWithFive().convertFive(105);
    }
    public int replace(int t){
        String s = String.valueOf(t);
        char[] chars = s.toCharArray();
        int i = 0;
        for(char c : chars){
            if(c == '0'){
                chars[i] = '5';
            }
            i++;
        }
        return Integer.parseInt(String.valueOf(chars));
    }
    public int convertFive(int t){
        int multiplier = 1;
        int res = 0;
        while(t>0){
            int k = t%10;
            t = t/10;
            if(k == 0){
                k = 5;
            }
            res += (multiplier*k);
            multiplier *= 10;
        }
        return res;
    }
}
