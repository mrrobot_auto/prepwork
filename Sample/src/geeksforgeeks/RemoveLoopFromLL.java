package geeksforgeeks;

import java.util.HashSet;
import java.util.Set;

public class RemoveLoopFromLL {
    public static void main(String[] args) {
        Node node = new Node(2);
        node.next = new Node(5);
        node.next.next = new Node(6);
        node.next.next.next = new Node(7);
        node.next.next.next.next = node.next.next.next;
        /*node.next.next.next.next.next = new Node(9);
        node.next.next.next.next.next.next = new Node(10);*/
        //removeLoop(node);
        while (node != null) {
            System.out.println(node.value);
            node = node.next;
        }
    }

    public static void removeLoop(Node head) {
        // code here
        // remove the loop without losing any nodes
        Set<Node> nodes = new HashSet<>();
        Node preHead = null;
        while (head != null) {
            if (!nodes.add(head)) {
                assert preHead != null;
                preHead.next = null;
                break;
            }
            preHead = head;
            head = head.next;
        }
    }
}
