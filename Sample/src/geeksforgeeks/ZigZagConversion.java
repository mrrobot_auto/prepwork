package geeksforgeeks;

public class ZigZagConversion {
    public static void main(String[] args) {
        convert("PAYPALISHIRING",4);
    }
    public static String convert(String s, int numRows) {
        StringBuilder out = new StringBuilder();
        StringBuilder[] res = new StringBuilder[numRows];
        for (int k = 0; k < res.length; k++) {
            res[k] = new StringBuilder();

        }
        int i = 0;
        char[] chars = s.toCharArray();
        int n = chars.length;
        while(i < n){
            for(int id=0; id<numRows && i<n; id++){
                res[id] = res[id].append(chars[i]);
                i++;
            }
            for(int r = numRows-2; r>= 1 && i<n ; r--){
                res[r] = res[r].append(chars[i]);
                i++;
            }
        }
        for(StringBuilder p : res){
            out.append(p);
        }
        return out.toString();
    }
}
