package geeksforgeeks;

public class CountOfElements {
    public static void main(String[] args) {

    }

    public static long countOfElements(long arr[], long n, long x) {
        int count = 0;
        int index = 0;
        while(index<n){
            if(arr[index]<=x){
                count ++;
            }
            index ++;
        }
        return count;
    }
}
