package geeksforgeeks;

public class Node {
    public int value;
    public Node next;

    public Node(Node next) {
        this.next = next;
    }

    public Node(){}
    public Node(int value) {
        this.value = value;
        this.next = null;
    }
}
