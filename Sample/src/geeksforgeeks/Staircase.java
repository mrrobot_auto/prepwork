package geeksforgeeks;

public class Staircase {
    public static void main(String[] args) {
        staircase(Integer.parseInt(args[0]));
    }
    public static void staircase(int n) {
        // Write your code here
        for(int i=1;i<=n;i++){
            for(int k=n; k>i; k--){
                System.out.print(" ");
            }
            for(int j=1;j<=i;j++){
                System.out.print("#");
            }
            System.out.println();
        }

    }
}
