package geeksforgeeks;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class OptimalBST {
    public static void main(String[] args) {
        int[] keys = {1, 4, 32, 34, 5};
        int[] freq = {39, 14, 14, 12, 40};
        System.out.println(optimalSearchTree(keys,freq,2));
    }
    static int optimalSearchTree(int keys[], int freq[], int n)
    {
        // code here
        int sum = 0;
        Map<Integer,Integer> map = new HashMap<>();
        for(int i =0 ; i < n; i++){
            map.put(keys[i],freq[i]);
        }
        Map<Integer,Integer> newMap =map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue,
                (o,p) -> o, LinkedHashMap::new));
        System.out.println(newMap);
        int k = 1;
        for(Map.Entry l : newMap.entrySet()){
            sum=sum + ((int)l.getValue()*k);
            k++;
        }

        return sum;
    }
}
