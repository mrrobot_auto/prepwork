package geeksforgeeks;

import java.util.function.Predicate;
import java.util.stream.IntStream;

public class PrediTest {
    public static void main(String[] args) {
        int n = 5;
        Predicate<Integer> multipleOfThree = i -> i%3 ==0;
        Predicate<Integer> multipleOfFive = i -> i%5 ==0;

        IntStream.range(1, n+1).forEach(i -> {
            if(multipleOfThree.and(multipleOfFive).test(i)) System.out.println("FizzBuzz");
            if(multipleOfThree.test(i) && !multipleOfFive.test(i)) System.out.println("Fizz");
            if(!multipleOfThree.test(i) && multipleOfFive.test(i)) System.out.println("Buzz");
            if(!multipleOfThree.test(i) && !multipleOfFive.test(i)) System.out.println(i);
        });
    }
}
