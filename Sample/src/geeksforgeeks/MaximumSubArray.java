package geeksforgeeks;

public class MaximumSubArray {
    public static void main(String[] args) {
        new MaximumSubArray().findMaxAverage(new int[]{1, 12, -5, -6, 50, 3}, 6, 4);
    }

    int findMaxAverage(int[] arr, int n, int k) {
        // code here
        if (k < 0) return -1;
        int max = Integer.MIN_VALUE;
        int sum = 0;
        int temp = 0;
        for (int i = 0; i <= n - k; i++) {
            sum = 0;
            for (int j = i; j < i + k; j++) {
                sum += arr[j];
            }
            int tMax = max;
            max = Math.max(sum, max);
            if (tMax != max) {
                temp = i;
            }
        }
        return temp;
    }
}
