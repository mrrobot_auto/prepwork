package geeksforgeeks;

import java.util.Stack;

public class SortStack {
    public static void main(String[] args) {
        Stack<Integer> s = new Stack<>();
        s.push(2);
        s.push(5);
        s.push(3);
        s.push(4);
        s.push(7);
        s.push(9);
        s.push(8);
        s.push(1);
        s.push(0);
        var a = 10;
        var b = 200;
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        s.forEach(System.out::println);
        sortone(s,0);
       // s = sort(s);
        System.out.print("Post Sort");
        s.forEach(System.out::println);
    }

    public static Stack<Integer> sort(Stack<Integer> s) {
        Stack<Integer> s1 = new Stack<>();
        while (s.size() > 1) {
            int temp = s.pop();
            int current = s.peek();
            if (temp > current) {
                s.push(temp);
            } else {
                s1.push(temp);
            }
        }
        return s;
    }

    public static void sortone(Stack<Integer> s, int first) {
        if (s.size() < 1) return;
        int temp = s.pop();
        if (temp < first) {
            s.push(first);
        }
        sortone(s, s.pop());
    }
}
