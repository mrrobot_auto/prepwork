package geeksforgeeks;

public class RotateLinkedList {
    public static void main(String[] args) {
        Node node = new Node(1);
        node.next = new Node(2);
        /*node.next.next = new Node(3);
        node.next.next.next = new Node(4);
        node.next.next.next.next = new Node(5);
        node.next.next.next.next.next = new Node(6);
        node.next.next.next.next.next.next = new Node(7);
        node.next.next.next.next.next.next.next = new Node(8);*/
        rotate(node, 1);
    }

    public static Node rotate(Node head, int k) {
        // add code here
        if(head == null) return head;
        if(k == 0) return head;
        Node node = head;
        int count = 0;
        int size = 0;
        Node tHead = head;
        while (tHead != null) {
            tHead = tHead.next;
            size++;
        }
        if(k>=size){
            k = k % size;
        }

        while (node != null && count < k) {
            node = node.next;
            count++;
        }
        Node res = node;
        while (node != null && node.next != null) {
            node = node.next;
        }
        count = 0;
        Node tailNode = head;

        while (tailNode != null) {
            if (count >= k-1) {
                tailNode.next = null;
                break;
            }else {
                tailNode = tailNode.next;
                ++count;
            }
        }
        if(node != null)node.next = head;
        return res==null?head:res;
    }
}
