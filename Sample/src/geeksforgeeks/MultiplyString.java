package geeksforgeeks;

import java.math.BigInteger;

public class MultiplyString {
    public static void main(String[] args) {
        System.out.println(multiplyStrings("1321234234234234234234","6548"));
    }
    public static String multiplyStrings(String s1,String s2)
    {
        return myAtoi(s1).multiply(myAtoi(s2)) + "";

    }

    static BigInteger myAtoi(String str)
    {
        // Initialize result
        BigInteger res = BigInteger.ZERO;
        int startIndex = 0;
        if(str.startsWith("-")){
            startIndex = 1;
        }
        for (int i = startIndex; i < str.length(); ++i)
            res = res.multiply(BigInteger.valueOf(10)).add(BigInteger.valueOf((long)str.charAt(i) - '0'));

        // return result
        return startIndex == 1? res.negate():res;
    }
}
