package geeksforgeeks;

import java.util.Arrays;

public class GameWithNumbers {
    public static void main(String[] args) {
        int[] a = {5, 9, 7, 6};
        Arrays.stream(getXorArray(a, 4)).forEach(System.out::println);
    }

    public static int[] getXorArray(int[] a, int n) {

        for (int i = 0; i < n - 1; i++) {
            a[i] = a[i] ^ a[i + 1];
        }
        return a;
    }

}
