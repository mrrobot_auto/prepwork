package geeksforgeeks;

import java.util.Arrays;

public class GetMinDiff {
    public static void main(String[] args) {
        new GetMinDiff().getMinDiff(new int[]{2, 6, 3, 4, 7, 2, 10, 3, 2, 1},10,5);
    }
    int getMinDiff(int[] arr, int n, int k) {
        // code here
        int[] add = new int[n];
        for(int i = 0; i<n ; i++){
            add[i] = arr[i] + k;
        }
        int[] red = new int[n];
        for(int j = 0; j<n ; j++){
            red[j] = arr[j] - k;
        }

        int firstPos = 0;
        for(int l : red){
            if(l>0){
                firstPos = l;
                break;
            }
        }
        return Math.abs((add[0]) - (red[n-1]));
    }
}
