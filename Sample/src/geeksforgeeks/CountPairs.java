package geeksforgeeks;

public class CountPairs {
    public static void main(String[] args) {

    }
    static long countPairs(int N, int X, int numbers[]){
        if (N == 1) return 0;
        long count = 0;
        String num = String.valueOf(X);
        int len = num.length();
        for (int j = 0; j < N; j++) {
            if((numbers[j]+"").length()>=len) continue;
            for (int k = 0; k < N; k++) {
                if(j != k && (numbers[j] + "" + numbers[k]).length()==len && (numbers[j] + "" + numbers[k]).equals(num)){
                    count++;
                }
            }
        }
        return count;
    }
    long countPairs1(int N, int X, int numbers[]) {
        // code here
        if (N == 1) return 0;
        long count = 0;
        int i = 0;
        int a = 1;
        String num = String.valueOf(X);
        while (a < N) {
            if ((numbers[i] + "" + numbers[a]).equals(num)) {
                count++;
            }
            i++;
            a++;
        }
        i = N - 1;
        a = N;
        while (i >= 0) {
            if ((numbers[a] + "" + numbers[i]).equals(num)) {
                count++;
            }
            i++;
            a++;
        }
        return count;
    }

}
