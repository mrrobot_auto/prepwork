package geeksforgeeks;

public class PallindromeSkipper {

    public static void main(String[] args) {
        saveIronman("tt15 %6 ##1T T");
    }
    public static boolean saveIronman (String s) {
        //Complete the function
        s = s.toLowerCase();
        StringBuilder sb = new StringBuilder();
        char[] cs = s.toCharArray();
        for(char c : cs){
            if(c>='a' && c<='z'){
                sb.append(c);
            }
        }
        String newS = sb.toString();
        int l = 0;
        int r = newS.length()-1;
        while(l<=r){
            if(newS.charAt(l) != newS.charAt(r)){
                return false;
            }
            l++;
            r--;
        }
        return true;
    }
}
