package geeksforgeeks;

public class sortArrayWithFrequency {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 3, 2, 2, 2, 2, 2, 2, 2, 5, 5, 9, 9, 9, 0, 0};
        int[][] res = new int[arr.length][2];
        for (int i = 0; i < arr.length; i++) {
            if (!exists(res, arr[i])) {
                res[i][0] = arr[i];
                setCount(res, arr[i]);
            } else setCount(res, arr[i]);
        }
        res = getNonZeroArray(res);
        sort(res);
        for (int i = 0; i < res.length; i++) {
            System.out.println(res[i][0] + " " + res[i][1]);
        }
    }

    public static void sort(int[][] res) {
        for (int i = 0; i < res.length - 2; i++) {
            if (res[i][0] > res[i + 1][0]) {
                res[i][0] = res[i][0] ^ res[i + 1][0];
                res[i+1][0] = res[i][0] ^ res[i + 1][0];
                res[i][0] = res[i][0] ^ res[i + 1][0];
            }
        }
    }

    public static boolean exists(int[][] arr, int element) {
        for (int[] i : arr) {
            if (i[0] == element) return true;
        }
        return false;
    }

    public static void setCount(int[][] arr, int element) {
        for (int[] i : arr) {
            if (i[0] == element) {
                i[1]++;
                break;
            }
        }
    }

    public static int[][] getNonZeroArray(int[][] res) {
        int count = 0;
        for (int[] i : res) {
            if (i[1] != 0) {
                count++;
            }
        }
        int[][] newRes = new int[count][2];
        count = 0;
        for (int[] i : res) {
            if (i[1] != 0) {
                newRes[count][0] = i[0];
                newRes[count][1] = i[1];
                count++;
            }
        }
        return newRes;
    }
}