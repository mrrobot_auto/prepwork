package geeksforgeeks;

public class DeleteANodeFromLL {
    public static void main(String[] args) {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        Node node = deleteNode(head, 2);
    }

    static Node deleteNode(Node head, int x) {
        // Your code here
        if (x == 1) {
            return head.next;
        }
        Node adjust = head;
        int count = 2;
        while (adjust != null) {
            if (count == x) {
                adjust.next = adjust.next.next;
                break;
            } else {
                adjust = adjust.next;
                ++count;
            }
        }
        return head;
    }
}
