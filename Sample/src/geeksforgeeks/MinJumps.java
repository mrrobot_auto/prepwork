package geeksforgeeks;

import java.util.Arrays;

public class MinJumps {
    public static void main(String[] args) {

        System.out.println(minJumps(new int[]{2, 63, 81, 78, 91, 64, 91, 2, 70, 97, 73, 64, 97, 39, 21}));
    }
    static int minJumps(int[] arr){
        // your code here
        int jumps = 0;
        int pointer = 0;
        while(pointer<arr.length-1){
            if(arr[pointer] == 0) return -1;
            if(arr[pointer]>1){
                int maxpointer = 0;
                for(int i = pointer ; i< Math.min(pointer+arr[pointer],arr.length-1) ; i ++){
                    if(Math.max(maxpointer,arr[i]) < arr.length){
                        maxpointer = Math.max(maxpointer,arr[i]);
                        if(pointer + maxpointer >= arr.length){
                            break;
                        }
                    }
                }
                if(maxpointer != 0) {
                    pointer += maxpointer;
                }else break;
            }else {
                pointer += arr[pointer];}
            jumps ++;
        }

        return jumps;
    }
}
