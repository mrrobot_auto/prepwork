package geeksforgeeks;

import java.util.Scanner;
import java.util.Stack;

public class MinStack {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        while (T > 0) {
            int q = sc.nextInt();
            GfG g = new GfG();
            while (q > 0) {
                int qt = sc.nextInt();

                if (qt == 1) {
                    int att = sc.nextInt();
                    g.push(att);
                    //System.out.println(att);
                } else if (qt == 2) {
                    System.out.print(g.pop() + " ");
                } else if (qt == 3) {
                    System.out.print(g.getMin() + " ");
                }

                q--;
            }
            System.out.println();
            T--;
        }

    }
}


// } Driver Code Ends


class GfG {
    int minEle = Integer.MAX_VALUE;
    Stack<Integer> minStack = new Stack<>();
    Stack<Integer> s = new Stack<>();

    /*returns min element from stack*/
    int getMin() {
        // Your code here
        return s.isEmpty() || minStack.isEmpty() ? -1 : minStack.peek();
    }

    /*returns poped element from stack*/
    int pop() {
        // Your code here
        int popEle;
        if (s.isEmpty()) {
            return -1;
        }
        popEle = s.pop();
        if (popEle == getMin()) {
            minStack.pop();
        }
        return popEle;
    }

    /*push element x into the stack*/
    void push(int x) {
        // Your code here
        //System.out.println("x="+x+" Min="+getMin());
        if (x < minEle) {
            minStack.push(x);
            minEle = getMin() == -1 ? Integer.MAX_VALUE : getMin();
        }
        s.push(x);
    }
}

