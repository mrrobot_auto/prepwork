package geeksforgeeks;

import java.util.Arrays;
import java.util.Comparator;

public class NthLargestFromArray {
    public static void main(String[] args) {
        Integer[] o = {1, 99, 1000, 121, 2, 2, 3, 7};
        System.out.println(nthLargest(o, 3, 8));
    }

    public static int nthLargest(Integer[] arr, int n, int len) {
        Arrays.sort(arr, Comparator.reverseOrder());
        Arrays.stream(arr).forEach(System.out::println);
        return arr[n-1];
    }
}
