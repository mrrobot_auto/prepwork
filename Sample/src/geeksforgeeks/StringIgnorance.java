package geeksforgeeks;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
//It sa longdy ear.
public class StringIgnorance {
    public static void main(String[] args) {
        //code
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        sc.nextLine();
        java.util.stream.IntStream.range(0, t).forEach(i ->
                {
                    String s = sc.nextLine();
                    System.out.println(ignoredString(s));
                }

        );
    }

    public static String ignoredString(String s) {
        List<Character> q = new LinkedList<>();
        char[] ss = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : ss) {
            if (q.isEmpty()) {
                q.add(c);
                if(c>='A' && c<='Z'){
                    q.add((char)((c-'A')+'a'));
                }else if(c>='a' && c<='z'){
                    q.add((char)((c-'a')+'A'));
                }
                sb.append(c);
            } else {
                if (!q.contains(c)) {
                    sb.append(c);
                    q.add(c);
                    if(c>='A' && c<='Z'){
                        q.add((char)((c-'A')+'a'));
                    }else if(c>='a' && c<='z'){
                        q.add((char)((c-'a')+'A'));
                    }
                } else{
                    q.remove(Character.valueOf(c));
                    if(c>='A' && c<='Z'){
                        q.remove(Character.valueOf((char)((c-'A')+'a')));
                    }else if(c>='a' && c<='z'){
                        q.remove(Character.valueOf((char)((c-'a')+'A')));
                    }
                }
            }
        }
        return sb.toString();
    }
}
