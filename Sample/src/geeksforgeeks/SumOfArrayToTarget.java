package geeksforgeeks;

public class SumOfArrayToTarget {
    public static void main(String[] args) {
        int[] intArr = {1, 3, 4, 7, 9, 11};
        int[] res = getSum(intArr, 11, 6, 0);
        for (int t : res) {
            System.out.println(t);
        }
    }

    public static int[] getSum(int[] arr, int target, int end, int start) {
        if (end == 0 || start == arr.length) return new int[]{start, end - 1};
        if (arr[end - 1] > target || arr[start] + arr[end - 1] > target) {
            end--;
            getSum(arr, target, end, start);
        } else if (arr[start] + arr[end - 1] < target) {
            start++;
            getSum(arr, target, end, start);
        } else return new int[]{start, end - 1};

        return getSum(arr, target, end, start);
    }

}
