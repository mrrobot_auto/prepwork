package geeksforgeeks;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveDuplicatesFromLinkedList {
    public static void main(String[] args) {
        Node node = new Node(8);
        node.next = new Node(5);
        node.next.next = new Node(2);
        node.next.next.next = new Node(2);
        node.next.next.next.next = new Node(5);
        node.next.next.next.next.next = new Node(8);
        removeDuplicates(node);
    }

    public static Node removeDuplicates(Node head) {
        // Your code here
        Set<Integer> set = new LinkedHashSet<>();
        while (head != null) {
            set.add(head.value);
            head = head.next;
        }
        Node res = null;
        for (int i : set) {
            if (res == null) {
                res = new Node(i);
            } else {
                Node temp = res;
                while (temp.next != null) {
                    temp = temp.next;
                }
                temp.next = new Node(i);
            }
        }
        return res;
    }

    public Node removeDuplicatesOP(Node head) {
        Set<Integer> set = new HashSet<>();
        Node pre = null;
        Node fakeHead = head;
        while (fakeHead != null) {
            if (!set.add(fakeHead.value)) {
                pre.next = fakeHead.next;
            } else {
                pre = fakeHead;
            }
            fakeHead = fakeHead.next;
        }
        return head;
    }
}
