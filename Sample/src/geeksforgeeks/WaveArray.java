package geeksforgeeks;

public class WaveArray {
    public static void main(String[] args) {
        int[] arr = {2,4,7,8,9,10};
        waveArray(arr, 6);
        for (int i :
                arr) {
            System.out.println(i);
        }
    }

    public static void waveArray(int[] arr, int n) {
        for (int i = 0; i < n-1; i+=2) {
            swap(arr,  i, i+1);
        }
    }

    public static void swap(int[] arr, int index1, int index2) {
        int temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
    }
}
