package geeksforgeeks;

import java.util.ArrayList;

public class Atoi {
    public static void main(String[] args) {
        myAtoi("321564");
    }

    /**
     *  <li>Iterate through all characters</li>
     *  <li>of input string and update result</li>
     *  <li>take ASCII character of corresponding digit and</li>
     *  <li>subtract the code from '0' to get numerical</li>
     *  <li>value and multiply res by 10 to shuffle</li>
     * <li>digits left to update running total</li>
     */
    static int myAtoi(String str) {
        // Initialize result
        int res = 0;


        for (int i = 0; i < str.length(); ++i)
            res = res * 10 + str.charAt(i) - '0';

        // return result.
        return res;
    }
}
