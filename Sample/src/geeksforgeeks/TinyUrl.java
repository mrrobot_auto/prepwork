package geeksforgeeks;

public class TinyUrl {
    public static void main(String[] args) {
     idToShortURL(30540);
    }
    static char intToChar(long n){
        if(n < 26){
            return (char) ('a' + n);
        }
        if(n < 52){
            return (char) ('A' + n - 26);
        }
        return (char) ('0' + n - 52);
    }

    static String idToShortURL(long n) {
        // code here
        StringBuilder sb = new StringBuilder();
        while(n>0){
            char k = intToChar(n%62);
            sb.append(k);
            n/=62;
        }
        return sb.reverse().toString();
    }
}