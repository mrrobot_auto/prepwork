package geeksforgeeks;

public class MinMaxArray {
    public static void main(String[] args) {
        long[] inta = {9, 7, 4, 8, 3, 3, 3, 1};
        long[] l = getMinMax(inta, 8);
        for (long k :
                l) {
            System.out.println(k);
        }
    }

    public static long[] getMinMax(long[] a, long n) {
        long min = a[0];
        long max = a[(int) n - 1];
        int count = 0;
        while (count != n) {
            if (a[count] > max)
                max = a[count];
            else if (a[count] < min)
                min = a[count];
            count++;
        }
        return new long[]{max, min};
    }
}
