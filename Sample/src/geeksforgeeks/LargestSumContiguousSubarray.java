package geeksforgeeks;

import java.util.ArrayList;
import java.util.List;

public class LargestSumContiguousSubarray {
    /**
     *<h>Problem Statement</h>
     * <p>Given an array arr[] of N positive integers,
     * the task is to find the subarray having maximum sum among all subarrays having unique elements and print its sum. </p>
     *
     * <i>Input arr[] = {1, 2, 3, 3, 4, 5, 2, 1}
     * Output: 15
     * Explanation:
     * The subarray having maximum sum with distinct element is {3, 4, 5, 2, 1}.
     * Therefore, sum is = 3 + 4 + 5 + 2 + 1 = 15 </i>
     */


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 3, 4, 5, 2, 1};
        new LargestSumContiguousSubarray().sum(arr);
    }
    public int sum(int[] a){
        int sum = 0;
        int max = Integer.MIN_VALUE;
        List<Integer> visited = new ArrayList<>();
        for (int n : a){
            if(!visited.contains(n)){
                visited.add(n);
                sum += n;
            }else{
                visited.clear();
                sum = n;
            }
            max = Math.max(max,sum);
        }
        return max;
    }
}
