package geeksforgeeks;

import java.util.ArrayList;
import java.util.List;

public class SmallestAbsoluteDifference {
    public static void main(String[] args) {
        long[] a = {5, 7, 9, 3, 4, 2, 1, 8, 9, 10};
        System.out.println(kthDiff(a, 10, 10));
    }

    public static long kthDiff(long arr[], long n, long k) {
        if (n == 2) return arr[1] - arr[0];
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                list.add(Math.abs((int) (arr[i] - arr[j])));
                System.out.println(list);
            }
            System.out.println();
        }
        list.sort(Integer::compare);
        System.out.println("final: " + list);

        return list.get((int) k);
    }
}
