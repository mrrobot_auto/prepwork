package geeksforgeeks;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LeftView {
    public static void main(String[] args) {

    }
    static ArrayList<Integer> leftView(TreeNode root) throws CloneNotSupportedException {
        // Your code here
        ArrayList<Integer> list = new ArrayList<>();
        Queue<Integer> q = new LinkedList<>();
        q.offer(root.val);
        while(!q.isEmpty()){
            list.add(q.poll());

            if(root.left != null){
                root = root.left;
                q.offer(root.val);
            }else {
                root = root.right;
                q.offer(root.val);
            }
        }
        return list;
    }
}
