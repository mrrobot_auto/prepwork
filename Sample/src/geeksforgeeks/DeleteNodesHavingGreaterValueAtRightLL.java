package geeksforgeeks;

public class DeleteNodesHavingGreaterValueAtRightLL {
    public static void main(String[] args) {
        Node node = new Node(12);
        node.next = new Node(15);
        node.next.next = new Node(10);
        node.next.next.next = new Node(11);
        node.next.next.next.next = new Node(5);
        node.next.next.next.next.next = new Node(6);
        node.next.next.next.next.next.next = new Node(2);
        node.next.next.next.next.next.next.next = new Node(3);
        compute(node);
    }

    static Node compute(Node head) {
        // your code here
        Node temp = head;
        Node res = null;
        Node one = null;
        try {
            while (temp != null) {
                if (temp.value < head.value) {
                    if (res == null) {
                        res = new Node(temp.value);
                        one = res;
                        head.value = temp.value;
                    } else {
                        while (res.next != null) {
                            res = res.next;
                        }
                        res.next = new Node(temp.value);
                        head.value = temp.value;
                    }
                }
                temp = temp.next;
            }
        } catch (Exception e) {
            if (res == null) {
                res = new Node(temp.value);
                one = res;
            } else {
                while (res.next != null) {
                    res = res.next;
                }
                res.next = new Node(temp.value);
            }
        }
        return one;
    }
}
