package geeksforgeeks;

public class ValidIP {
    public static void main(String[] args) {
        System.out.println(isValid("00.00.00.00"));
    }

    public static boolean isValid(String s) {
        // Write your code here
        String[] p = s.split("\\.");
        int sum = 0;
        if (p.length != 4) return false;
        for (String str : p) {
            try {
                if (str.length() >= 4 || Integer.parseInt(str) < 0 || Integer.parseInt(str) > 255) {
                    return false;
                } else {
                    if (str.length() > 1 && str.startsWith("0")) {
                        return false;
                    }
                    sum += Integer.parseInt(str);
                }
            } catch (Exception e) {
                return false;
            }
        }
        return sum != 0 || s.equals("0.0.0.0");
    }
}
