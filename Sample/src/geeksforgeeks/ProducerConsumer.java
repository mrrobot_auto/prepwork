package geeksforgeeks;

import java.util.ArrayList;
import java.util.List;

public class ProducerConsumer {
    public static void main(String[] args) {
        List<Integer> sharedList = new ArrayList<>();

        Thread prod = new Thread(new Producer(sharedList));
        Thread cons = new Thread(new Consumer(sharedList));
        prod.start();
        cons.start();
        Thread stopper = new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("in stopper "+sharedList.get(0));
            if(sharedList.get(0) > 0){
                prod.interrupt();
                cons.interrupt();
            }
        });
        stopper.start();

    }
}

class Producer implements Runnable {
    List<Integer> sharedList = null;
    int i = 0;

    @Override
    public void run() {
        try {
            while (true) {
                produce(i++);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Producer(List<Integer> sharedList) {
        this.sharedList = sharedList;
    }

    public void produce(int i) throws InterruptedException {
        synchronized (sharedList) {
            while (sharedList.size() == 5) {
                System.out.println("Waiting for consumer to consume");
                sharedList.wait();
            }
        }
        synchronized (sharedList) {
            System.out.println("producing : " + i);
            sharedList.add(i);
            Thread.sleep(100);
            sharedList.notify();
        }
    }
}

class Consumer implements Runnable {
    List<Integer> sharedList = null;

    @Override
    public void run() {
        try {
            while (true) {
                consume();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Consumer(List<Integer> sharedList) {
        this.sharedList = sharedList;
    }

    public void consume() throws InterruptedException {
        synchronized (sharedList) {
            while (sharedList.isEmpty()) {
                System.out.println("Waiting for producer to produce");
                sharedList.wait();
            }

        }
        synchronized (sharedList) {
            System.out.println("consumed :" + sharedList.remove(0));
            Thread.sleep(1000);
            sharedList.notify();
        }
    }
}
