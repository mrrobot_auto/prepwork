package geeksforgeeks;

import java.util.HashSet;
import java.util.Set;

public class ExactlyOneSwap {
    public static void main(String[] args) {
        countStrings("geek");
    }
    static long countStrings(String S)
    {
        // code here
        int[] count = new int[26];
        int n = S.length();
        long ans = getCom(n);
        for(int i = 0; i < n; i++){
            count[S.charAt(i)-'a']++;
        }
        boolean temp = false;
        for(int i = 0; i < 26; i++){
            if(count[i]>1){
                temp = true;
                ans = ans - getCom(count[i]);
            }
        }
        if(temp){
            ans = ans + 1;
        }
        return ans;
    }
    static long getCom(long n){
        return n*(n-1)/2;
    }
    static int factorial(int i) {
        if (i <= 1) return 1;
        return i * factorial(i - 1);
    }

    static int swap(String str) {
        char[] chars = str.toCharArray();
        Set<Character> set = new HashSet<>();
        for (char c : chars) {
            set.add(c);
        }
        return factorial(set.size());
    }

    static long countStringsl(String S) {
        // code here
        Set<String> hashSet = new HashSet<>();
        for (int i = 0; i < S.length(); i++) {
            for (int j = 0; j < S.length(); j++) {
                if (i != j) {
                    hashSet.add(swap(S, i, j));
                }
            }
        }
        return hashSet.size();
    }

    static void swap(char[] s, int i, int j) {
        char temp = s[i];
        s[i] = s[j];
        s[j] = temp;
    }

    static String swap(String s, int i, int j) {
        StringBuilder sb = new StringBuilder(s);
        char temp = s.charAt(i);
        sb.setCharAt(i, s.charAt(j));
        sb.setCharAt(j, temp);
        return sb+"";
    }
}
