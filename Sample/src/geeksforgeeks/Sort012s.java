package geeksforgeeks;

public class Sort012s {
    public static void main(String[] args) {
        sort012(new int[]{0,2,1,2,0},5);
    }

    public static void sort012(int a[], int n)
    {
        int zeros = 0,ones = 0,twos = 0;
        // code here
        for(int i = 0; i < a.length; i++){
            switch(a[i]){
                case 0 : zeros ++;
                    break;
                case 1 : ones ++;
                    break;
                case 2 : twos ++;
                    break;
            }
        }
        for(int j = 0; j < a.length ; j++){
            if(zeros != 0){
                a[j] = 0;
                zeros --;
            }else if(ones != 0){
                a[j] = 1;
                ones --;
            }else if(twos != 0){
                a[j] = 2;
                twos --;
            }
        }
    }
}
