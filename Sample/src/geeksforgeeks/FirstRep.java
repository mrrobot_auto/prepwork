package geeksforgeeks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FirstRep {
    public static void main(String[] args) {
        firstRep("geeksforgeeks");
    }
    static char firstRep(String S)
    {
        // your code here
        char[] cs = S.toCharArray();
        Set<Character> set = new HashSet<>();
        List<Character> list = new ArrayList<>();
        for(char c : cs){
            if(!set.add(c)){
                list.add(c);
            }
        }
        for(char c : cs){
            if(list.contains(c)){
                return c;
            }
        }
        return '#';
    }
}
