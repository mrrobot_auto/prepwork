package geeksforgeeks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CamelCaseMatching {
    public static void main(String[] args) {
        camelMatch(new String[]{"FaceBook", "FooBar", "FooBarTest"}, "FB");
    }

    public static List<Boolean> camelMatch(String[] queries, String pattern) {
        List<Boolean> res = new ArrayList<>();
        char[] patArray = pattern.toCharArray();
        int n = queries.length;
        for (int i = 0; i < n; i++) {
            char[] sts = queries[i].toCharArray();
            char[] caps = new char[patArray.length];
            int index = 0;
            boolean equal = true;
            for (char c : sts) {
                if (index< patArray.length && c == patArray[index]) {
                    caps[index] = c;
                    index++;
                }else if(c>64 && c<91){
                    equal = false;
                    break;
                }
            }
            if (equal && Arrays.equals(caps, patArray)) {
                res.add(i, true);
            } else res.add(i, false);
        }
        return res;
    }
}
