package geeksforgeeks;

public class BinarySearchArray {
    public static void main(String[] args) {
        search(new int[]{-1, 0, 3, 5, 9, 12, 15}, 9);
    }

    public static int search(int[] nums, int target) {
        int mid = nums.length / 2;
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            if (nums[mid] == target) {
                return mid;
            }
            if (target > nums[mid]) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
            mid = (left + right) / 2;
        }
        return -1;
    }
}
