package geeksforgeeks;
//@TAG : BitMasking
import java.util.Arrays;

public class LongestSubstringPalindrome {
    public static void main(String[] args) {
        longestSubstring("vizfcfc");
    }

    static int longestSubstring(String S) {
        // code here
        if (S.length() < 4) {
            if (isPalindrome(S)) {
                return S.length();
            } else {
                return 1;
            }
        }
        char[] schars = S.toCharArray();
        //Arrays.sort(schars);
        //String reS = new String(schars);
        int size = 3;
        int init = 0;
        for (int i = init; i < S.length() - 1; i++) {
            if (isPalindrome(S.substring(init, init + size))) {
                if (size <= S.length()) size++;
            } else {
                init++;
            }
        }
        return size - 1;
    }

    static boolean isPalindrome(String s) {
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        int n = chars.length % 2 == 0 ? chars.length : chars.length - 1;
        int index = 0;
        int inc = 1;
        while (index + inc < n) {
            if (chars[index] != chars[index + inc]) {
                return false;
            }
            index = index + 2;
            inc++;
        }
        return true;
    }
}