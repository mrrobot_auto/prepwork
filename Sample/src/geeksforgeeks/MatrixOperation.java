package geeksforgeeks;

public class MatrixOperation {
    public static void main(String[] args) {
        endPoints(new int[][]{{0, 1}, {1, 0}}, 2, 2);
    }

    static int[] endPoints(int[][] a, int m, int n) {
        //code here
        int j = 0;
        int i = 0;
        int right = 1, left = 0, up = 0, down = 0;
            while (i < m*n) {
                if(i>=m || j>=n || i<0 || j< 0){
                    i = i >= m ? i - 1 : i;
                    j = j >= n ? j - 1 : j;
                    i = Math.max(i, 0);
                    j = Math.max(j,0);
                    break;
                }
                if (down == 1) {
                    if (a[i][j] == 0) {
                        i++;
                    } else {
                        a[i][j] = 0;
                        down = 0;
                        left = 1;
                    }
                } else if (left == 1) {
                     if (a[i][j] == 0) {
                        j--;
                    } else {
                        a[i][j] = 0;
                        up = 1;
                        left = 0;
                    }
                } else if (up == 1) {
                    if (a[i][j] == 0) {
                        i--;
                    } else {
                        a[i][j] = 0;
                        up = 0;
                        right = 1;
                    }
                } else if (right == 1) {
                    if (a[i][j] == 0) {
                        j++;
                    } else {
                        a[i][j] = 0;
                        right = 0;
                        down = 1;
                    }
                }
            }
        return new int[]{i, j};
    }
}
