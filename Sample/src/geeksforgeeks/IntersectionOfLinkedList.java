package geeksforgeeks;

public class IntersectionOfLinkedList {
    public static void main(String[] args) {
        Node head = new Node(20);
        head.next = new Node(30);
        Node head2 = new Node(10);
        head.next.next = new Node(2);
        head.next.next.next = new Node(3);
        head.next.next.next.next = new Node(4);
        head2.next = head.next.next;
        int y = new IntersectionOfLinkedList().intersectPoint1(head, head2);
        System.out.println(y);
    }

    boolean isEqual(Node head1, Node head2){
        while (head1!= null && head2!=null){
            if(head1.value != head2.value) return false;
        }
        return true;
    }

    int intersectPoint(Node head1, Node head2) {
        // code here
        Node it = head1;
        while (head2 != null) {
            System.out.println("value from first LL : " + it.value);
            System.out.println("value from second LL : " + head2.value);
            if (it.value == head2.value && isEqual(it.next,head2.next)) {
                    return it.value;
            } else {
                it = it.next;
                if (it == null) {
                    head2 = head2.next;
                    it = head1;
                }
            }
        }
        return -1;
    }
    int intersectPoint1(Node head1, Node head2) {
        // code here
        Node ptr1=head1;
        Node ptr2=head2;
        if(ptr1==null||ptr2==null){
            return -1;
        }
        while(ptr1!=ptr2){
            ptr1=ptr1.next;
            ptr2=ptr2.next;
            if(ptr1==ptr2){
                return ptr1.value;
            }
            if (ptr1 == null) {

                ptr1 = head2;
            }
            if (ptr2== null) {

                ptr2 = head1;
            }
        }
        return ptr1.value;
    }
}
