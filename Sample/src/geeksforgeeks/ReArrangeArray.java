package geeksforgeeks;

import java.util.Arrays;

public class ReArrangeArray {
    public static void main(String[] args) {


        long[] arr = {4, 0, 2, 1, 3};
        arrange(arr, 5);
        Arrays.stream(arr).forEach(System.out::println);
    }

    public static void rearrange(long[] arr, int n) {
        long[] ee = Arrays.copyOf(arr, n);
        for (int i = 0; i < n; i++) {
            arr[i] = ee[(int) arr[i]];
        }
    }

    public static void arrange(long[] arr, int n) {
        for (int i = 0; i < n; i++) {
            arr[i] = arr[i] + (arr[(int) arr[i]] % n) * n;

        }
        for (int i = 0; i < n; i++) {
            arr[i] = arr[i] / n;
        }
    }
}
