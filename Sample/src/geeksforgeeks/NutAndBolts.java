package geeksforgeeks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class NutAndBolts {
    public static void main(String[] args) {
        char[] nuts = {'@', '%', '$', '#', '^'};
        char[] bolts = {'%', '@', '#', '$', '^'};
        matchPairs1(nuts, bolts, 5);
    }

   /* public static void matchPairs(char nuts[], char bolts[], int n) {
        Character[] kash = new Character[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (nuts[i] == bolts[j]) {
                    kash[i] = nuts[i];
                    nuts[i] = 0;
                    bolts[j] = 0;
                    break;
                }
            }
        }
        Arrays.stream(kash).sorted().forEach(o -> System.out.print(o + " "));
        System.out.println();
        Arrays.stream(kash).sorted().forEach(o -> System.out.print(o + " "));
    }*/

    public static void matchPairs1(char nuts[], char bolts[], int n) {
        Map<Character, Integer> hashmap = new HashMap<>();
        countOccurrences(hashmap, nuts);
        countOccurrences(hashmap, bolts);
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry e:
             hashmap.entrySet()) {
            if((Integer) e.getValue()%2 == 0){
                stringBuilder.append(e.getKey()).append(" ");
            }
        }
        System.out.print(stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(" ")));
        System.out.println();
        System.out.print(stringBuilder);
    }

    private static void countOccurrences(Map<Character, Integer> map, char[] chars) {
        for (Character c : chars) {
            if (map.containsKey(c))
                map.put(c, map.get(c) + 1);
            else
                map.put(c, 1);
        }
    }
}
