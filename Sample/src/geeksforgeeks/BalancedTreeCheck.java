package geeksforgeeks;

public class BalancedTreeCheck {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(5);
        node.left = new TreeNode(4);
        node.right = new TreeNode(6);
        node.left.left = new TreeNode(2);
        node.left.right = new TreeNode(3);
        new BalancedTreeCheck().isBalanced(node);

    }

    boolean isBalanced(TreeNode root) {
        // Your code here
        return count(root.left) - count(root.right) == 1 || count(root.left) == count(root.right);
    }
    private static int count(TreeNode root){
        if(root == null) return 0;
        if(root.left == null && root.right == null)
            return 0;
        int left = 1+count(root.left);
        int right = 1+count(root.right);
        return Math.max(left,right);
    }
    private static int countR(TreeNode root){
        if(root == null) return -1;
        return 1+countR(root.right);
    }

}
