package geeksforgeeks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DuplicatesInArray {
    public static void main(String[] args) {

    }
    public static ArrayList<Integer> duplicates(int arr[], int n) {
        // code here
        Set<Integer> setr = new HashSet<>();
        Set<Integer> set = new HashSet<>();
        for(int i : arr){
            if(!set.add(i)){
                setr.add(i);
            }
        }
        ArrayList<Integer> list = new ArrayList<>(setr);
        if(list.isEmpty()) list.add(-1);
        Collections.sort(list);
        return list;
    }
}
