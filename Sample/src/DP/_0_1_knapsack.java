package DP;

public class _0_1_knapsack {
    public static void main(String[] args) {
        System.out.println(knapSack(9,new int[]{4,5,6},new int[]{1,2,3},3));
    }

    private static int knapSack(int W, int[] wt, int[] val, int n) {
        if(W == 0 || n == 0){
            return 0;
        }
        if(wt[n-1]<=W){
            return Math.max(val[n - 1]
                            + knapSack(W - wt[n - 1], wt,
                            val, n - 1),
                    knapSack(W, wt, val, n - 1));
        }else
            return knapSack(W, wt, val, n - 1);
    }

    static int knapSack1(int W, int wt[], int val[], int n)
    {
        // your code here
        int globalVal = 0;
        int tempWt = W;
        int index = 0;
        int rep = 0;
        int res = 0;
        while(rep < wt.length){
            if(index != rep && wt[index]<=tempWt){
                int t = res;
                res = Math.max(res , res+val[index]);
                if(res != t){
                    tempWt -= wt[index];
                }
            }
            ++index;
            if(index>=wt.length || tempWt==0){
                globalVal = Math.max(res,globalVal);
                res = 0;
                index = 0;
                tempWt = W;
                rep++;
            }
        }

        return globalVal;
    }
}
