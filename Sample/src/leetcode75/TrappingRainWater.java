package leetcode75;
//@TAG: Rewrite and understand better
public class TrappingRainWater {
    public static void main(String[] args) {
        new TrappingRainWater().trapin(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1});
    }

    public int trap(int[] height) {
        int cap = height[0];
        int water = 0;
        for (int i = 1; i < height.length; i++) {
            cap = Math.max(height[i], cap);
            water += (cap - height[i]);
            if (water < 0) water = 0;
        }
        return water;
    }

    public int trapin(int[] height) {
        if (height == null || height.length == 0) {
            return 0;
        }
        int left = 0;
        int right = height.length - 1;
        int maxLeft = 0;
        int maxRight = 0;

        int totalWater = 0;
        while (left < right) {
            System.out.println("left = "+left+" right = "+right);
            System.out.println("left value = "+height[left]+" right value = "+height[right]);
            if (height[left] < height[right]) {
                if (height[left] >= maxLeft) {

                    maxLeft = height[left];
                    System.out.println("maxLeft = "+maxLeft);
                } else {
                    totalWater += maxLeft - height[left];
                    System.out.println("totalWater = "+totalWater);
                }
                left++;
            } else {
                if (height[right] >= maxRight) {
                    maxRight = height[right];
                    System.out.println("maxRight = "+maxRight);
                } else {
                    totalWater += maxRight - height[right];
                    System.out.println("totalWater = "+totalWater);
                }
                right--;
            }
        }
        return totalWater;
    }
}
