package leetcode75;

public class MaximumProductSubArray {
    public static void main(String[] args) {
        maxP(new int[]{3,-1,4});
    }
    public static int maxProduct(int[] nums) {
        int sum = 1;
        int max = Integer.MIN_VALUE;
        for(int i = 0 ; i<nums.length; i++){
            sum *= nums[i];
            max = Math.max(sum, max);

            if(sum<=0) sum = 1;
        }
        return max;
    }
    public static int maxP(int[] nums){
        int max = nums[0];
        int min = nums[0];
        int ans = nums[0];

        for (int i = 1; i < nums.length; i++) {

            int temp = max;  // store the max because before updating min your max will already be updated

            max = Math.max(Math.max(max * nums[i], min * nums[i]), nums[i]);
            min = Math.min(Math.min(temp * nums[i], min * nums[i]), nums[i]);

            if (max > ans) {
                ans = max;
            }
        }

        return ans;
    }
}
