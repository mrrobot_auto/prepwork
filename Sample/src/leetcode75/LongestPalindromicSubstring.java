package leetcode75;

public class LongestPalindromicSubstring {
    public static void main(String[] args) {
        System.out.println(longestPalindrome("ccc"));
    }

    public static String longestPalindrome(String s) {
        var res = "";
        //if(s.length()<3) return s;
        var max = 0;
        for (var i = 0; i <= s.length(); i++) {
            for (var j = i+1; j <= s.length(); j+=max) {
                if(isPalindrome(s.substring(i,j))){
                    var length = s.substring(i, j).length();
                    if(length > max){
                        max = length;
                        res = s.substring(i,j);
                    }
                }
            }
        }
        return res;
    }

    static boolean isPalindrome(String s){
        var res = true;
        var l = 0;
        var r = s.length()-1;
        while(l<=r){
            if(s.charAt(l)!=s.charAt(r)){
                return false;
            }
            l ++;
            r --;
        }
        return res;
    }
}
