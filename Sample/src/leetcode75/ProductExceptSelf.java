package leetcode75;

public class ProductExceptSelf {

    public static void main(String[] args) {
        productExceptSelfsmart(new int[]{2,3,4,5});
    }

    public static int[] productExceptSelf(int[] nums) {
        int prod = 1;
        int[] res = new int[nums.length];
        int index = 0;
        for (int i : nums) {
            if(i == 0) i = 1;
            prod *= i;
        }
        for (int i:
             nums) {
            if(i == 0) i = 1;
            res[index] = prod/i;
            index++;
        }
        return res;
    }
    public static int[] productExceptSelf1(int[] nums) {
        int[] res = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            int prod = 1;
            for (int j = 0; j < nums.length; j++) {
                if(i == j) continue;
                prod *= nums[j];
                res[i] = prod;
            }
        }
        return res;
    }
    public static int[] productExceptSelfsmart(int[] nums) {
        int n = nums.length;
        int[] res = new int[n];
        // Calculate lefts and store in res.
        int left = 1;
        for (int i = 0; i < n; i++) {
            if (i > 0)
                left = left * nums[i - 1];
            res[i] = left;
        }
        // Calculate rights and the product from the end of the array.
        int right = 1;
        for (int i = n - 1; i >= 0; i--) {
            if (i < n - 1)
                right = right * nums[i + 1];
            res[i] *= right;
        }
        return res;
    }
}
