package leetcode75;

public class MaxArea {
    public static void main(String[] args) {
        new MaxArea().maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7});
    }

    public int maxArea(int[] height) {
        var area = 0;
        var start = 0;
        var end = height.length-1;
        while(start<end){
            area = Math.max(area, (end - start) * Math.min(height[start], height[end]));
            if(height[start]<height[end])
                start++;
            else
                end--;
        }
        return area;
    }
    int area(int[] h, int start, int end){
        if(start>end) return 0;
        return Math.max(((end-start)*(Math.min(h[start],h[end]))),
                Math.max(
                        area(h,start,end-1),
                        area(h,start+1,end)
                ));
    }
}
