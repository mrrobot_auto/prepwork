package leetcode75;

import java.util.HashMap;
import java.util.Map;

public class NumbersToWords {
    public static void main(String[] args) {
       String g =  new NumbersToWords().numberToWords(878564273);
        System.out.println(g);
    }
    public final Map<Integer, String> map = new HashMap<>();
    public final Map<Integer, String> mapH = new HashMap<>();
    public final Map<Integer, String> mapT = new HashMap<>();

    public void init() {
        map.put(0, "");
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");
        map.put(4, "Four");
        map.put(5, "Five");
        map.put(6, "Six");
        map.put(7, "Seven");
        map.put(8, "Eight");
        map.put(9, "Nine");
        map.put(10, "Ten");

        mapH.put(0, "");
        mapH.put(2, "Twenty ");
        mapH.put(3, "Thirty ");
        mapH.put(4, "Forty ");
        mapH.put(5, "Fifty ");
        mapH.put(6, "Sixty ");
        mapH.put(7, "Seventy ");
        mapH.put(8, "Eighty ");
        mapH.put(9, "Ninety ");

        mapT.put(10,"Ten");
        mapT.put(11, "Eleven");
        mapT.put(12, "Twelve");
        mapT.put(13, "Thirteen");
        mapT.put(14, "Fourteen");
        mapT.put(15, "Fifteen");
        mapT.put(16, "Sixteen");
        mapT.put(17, "Seventeen");
        mapT.put(18, "Eighteen");
        mapT.put(19, "Nineteen");


    }

    public String numberToWords(int num) {
        if(num == 0) return "Zero";
        init();
        StringBuilder res = new StringBuilder();
        String number = String.valueOf(num);
        var len = number.length();
        var club = len / 3;
        var non_club = len % 3;
        if (non_club == 0) {
            switch (club) {
                /*case 4 -> res.append(con(number.substring(0, 3))).append(" Billion ").
                        append(con(number.substring(3, 6))).append(" Million ").
                        append(con(number.substring(6, 9))).append(" Thousand ").
                        append(con(number.substring(9)));
                case 3 -> res.append(con(number.substring(0, 3))).append(" Million ").
                        append(con(number.substring(3, 6))).append(" Thousand ").
                        append(con(number.substring(6)));
                case 2 -> res.append(con(number.substring(0, 3))).append(" Thousand ").
                        append(con(number.substring(3)));
                default -> res.append(con(number));*/
            }
        } else {
            switch (club) {
                /*case 3 -> res.append(con(number.substring(0, non_club))).append(" Billion ").
                        append(con(number.substring(non_club, non_club + 3))).append(" Million ").
                        append(con(number.substring(non_club + 3, non_club + 6))).append(" Thousand ").
                        append(con(number.substring(non_club + 6)));
                case 2 -> res.append(con(number.substring(0, non_club))).append(" Million ").
                        append(con(number.substring(non_club, non_club + 3))).append(" Thousand ").
                        append(con(number.substring(non_club + 3)));
                case 1 -> res.append(con(number.substring(0, non_club))).append(" Thousand ").
                        append(con(number.substring(non_club)));
                default -> res.append(con(number));*/
            }
        }
        return res.toString();

    }

    public String con(String num) {
        if(Integer.parseInt(num) == 0) return "";
        StringBuilder res = new StringBuilder();
        int len = num.length();
        switch (len) {
            /*case 1 -> res.append(map.get(Integer.parseInt(num)));
            case 2 -> {
                if(num.charAt(0) != '1'){
                    res.append(mapH.get(Integer.parseInt(num.substring(0, 1)))).
                            append(map.get(Integer.parseInt(num.substring(1))));}
                else {
                    res.append(mapT.get(Integer.parseInt(num)));
                }
            }
            case 3 -> {
                if (num.charAt(1) != '1'){
                    res.append(map.get(Integer.parseInt(num.substring(0, 1)))).append(" Hundred ").
                            append(mapH.get(Integer.parseInt(num.substring(1, 2)))).
                            append(map.get(Integer.parseInt(num.substring(2))));}
                else {
                    res.append(map.get(Integer.parseInt(num.substring(0, 1)))).append(" Hundred ").
                            append(mapT.get(Integer.parseInt(num.substring(1))));
                }
            }*/
        }
        return res.toString().trim();
    }
}