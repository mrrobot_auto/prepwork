package leetcode75;

import java.util.ArrayList;
import java.util.List;

public class MinWindow {
    public static void main(String[] args) {
        minWindow("ADOBECODEBANC", "ABC");
    }

    public static String minWindow(String s, String t) {
        if (s.equals(t)) return t;
        if (s.length() == t.length() && !s.equals(t)) return "";
        if (s.length() < t.length()) return "";
        int n = s.length();
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            if (t.contains(String.valueOf(s.charAt(i)))) {

                list.add(i);
            }
        }

        int min = Integer.MAX_VALUE;
        int minstart = 0;
        int minend = 0;
        for (int i = 0; i <= list.size() - t.length(); i = i + t.length()) {
            int temp = min;
            min = Math.min(min, list.get(i + t.length() - 1) - list.get(i));
            if (temp != min) {
                minstart = list.get(i);
                minend = list.get(i + t.length() - 1);
            }
        }
        return s.substring(minstart, minend + 1);
    }
}

