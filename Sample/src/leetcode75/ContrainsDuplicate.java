package leetcode75;

import java.util.HashMap;
import java.util.stream.IntStream;

public class ContrainsDuplicate {
    public static void main(String[] args) {

    }
    public boolean containsDuplicate(int[] nums) {
        HashMap<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i<nums.length;i++){
            if(map.containsKey(nums[i])){
                int c = map.get(nums[i]);
                map.put(nums[i],c+1);
            }else map.put(nums[i],1);
        }
        return map.entrySet().stream().anyMatch(p -> p.getValue()>1);
    }
}
