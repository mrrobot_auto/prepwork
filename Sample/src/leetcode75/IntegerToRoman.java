package leetcode75;

public class IntegerToRoman{
    public static void main(String[] args) {
        new IntegerToRoman().intToRoman(4);
    }
    public String intToRoman(int num) {
        StringBuilder res = new StringBuilder();
        int p;
        if(num/1000 != 0){
            p = num/1000;
            res.append("M".repeat(Math.max(0, p)));
            num -= (p*1000);
        }
        if(num/900 != 0){
            p = num/900;
            res.append("CM".repeat(Math.max(0, p)));
            num -= (p*900);
        }
        if(num/500 != 0){
            p = num/500;
            res.append("D".repeat(Math.max(0, p)));
            num -= (p*500);
        }
        if(num/400 != 0){
            p = num/400;
            res.append("CD".repeat(Math.max(0, p)));
            num -= (p*400);
        }
        if(num/100 != 0){
            p = num/100;
            res.append("C".repeat(Math.max(0, p)));
            num -= (p*100);
        }
        if(num/90 != 0){
            p = num/90;
            res.append("XC".repeat(Math.max(0, p)));
            num -= (p*90);
        }
        if(num/50 != 0){
            p = num/50;
            res.append("L".repeat(Math.max(0, p)));
            num -= (p*50);
        }
        if(num/40 != 0){
            p = num/40;
            res.append("XL".repeat(Math.max(0, p)));
            num -= (p*40);
        }
        if(num/10 != 0){
            p = num/10;
            res.append("X".repeat(Math.max(0, p)));
            num -= (p*10);
        }
        if(num/9 != 0){
            p = num/9;
            res.append("IX".repeat(Math.max(0, p)));
            num -= (p*9);
        }
        if(num/5 != 0){
            p = num/5;
            res.append("V".repeat(Math.max(0, p)));
            num -= (p*5);
        }
        if(num != 0 && num!=4){
            res.append("I".repeat(Math.max(0, num)));
        }
        if(num == 4){
            res.append("IV");
        }
        return res.toString();
    }
}
