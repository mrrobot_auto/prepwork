package leetcode75;

public class MaxProfit {
    public static void main(String[] args) {
        maxProfit(new int[]{7,2,5,6,1,4});
    }
    public static int maxProfit(int[] prices) {
        int max = 0;
        for(int i = 0; i<prices.length ; i++){
            for(int j = i+1; j< prices.length; j ++){
                if(prices[j]-prices[i] <= 0) break;
                max = Math.max(max,prices[j]-prices[i]);
            }
        }
        return max;
    }
}
