package java8;

import java.util.*;

public class OTest {
    public static void main(String[] args) {
        printOcc("vvaaavvvvdexxxxxx");
        System.out.println();
        printPrimes(200);
    }

    public static void out(){
        System.out.println("I am out");
    }
    public static void printOcc(String s){
        char[] chars = s.toCharArray();
        Queue<Character> q = new LinkedList<>();
        q.offer(chars[0]);
        int index = 1;
        while(index< chars.length){
            if(chars[index] == q.peek()){
                q.offer(chars[index]);
            }
            else {
                System.out.print(q.peek()+""+q.size());
                q.clear();
                q.offer(chars[index]);
            }
            index++;
        }
        System.out.print(q.peek()+""+q.size());
    }
    public static void findOccurances(String s){
        Map<Character,Integer> map = new LinkedHashMap<>();
        char[] chars = s.toCharArray();
        for(char c : chars){
            if(!map.containsKey(c)){
                map.put(c,1);
            }else {
                map.put(c,map.get(c)+1);
            }
        }
        for (Map.Entry e: map.entrySet()) {
            System.out.print(e.getKey()+""+e.getValue());
        }
    }
    public static void printPrimes(int range){
        for(int i=2 ; i<= range ; i++){
            if(isPrime(i)){
                System.out.print(i + ",");
            }
        }
    }
    private static boolean isPrime(int number){
        for(int i = 2; i<= Math.sqrt(number); i++){
            if(number%i == 0){
                return false;
            }
        }
        return true;
    }
}
