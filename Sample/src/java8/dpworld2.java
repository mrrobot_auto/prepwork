package java8;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class dpworld2 {
    /**
     * <li>String a =“a@b%y&*d”</>
     * <li>
     * String result = “d@y%b&*a”
     * </>
     */
    public static void main(String[] args) {
        new dpworld2().reverse("aA@b21%y&2*d");
    }

    public String reverseLetters(String s) {
        String res = "";
        StringBuilder sb = new StringBuilder();
        Deque<Character> stack = new LinkedList<>();
        Queue<Character> q = new LinkedList<>();
        char[] chars = s.toCharArray();
        for (char c : chars) {
            if ((c >= 'A' && c <= 'z') || (c >= '1' && c <= '9')) {
                stack.push(c);
            } else {
                q.offer(c);
            }
        }
        for (char c : chars) {
            if ((c >= 'A' && c <= 'z') || (c >= '1' && c <= '9')) {
                sb.append(stack.pop());
            } else sb.append(q.poll());
        }
        res = sb.toString();
        return res;
    }

    void reverse(String s) {
        int left = 0;
        char[] chars = s.toCharArray();
        int right = s.length() - 1;
        while (left < right) {
            if (!isAllowedCharacter(s.charAt(left))) {
                left++;
                continue;
            }
            if (!isAllowedCharacter(s.charAt(right))) {
                right--;
                continue;
            }
            swap(left, right, chars);
            left++;
            right--;
        }
        System.out.println(String.valueOf(chars));
    }

    boolean isAllowedCharacter(char c) {
        return ((c >= 'A' && c <= 'z') || (c >= '1' && c <= '9'));
    }

    void swap(int i, int j, char[] chars) {
        char temp = chars[i];
        chars[i] = chars[j];
        chars[j] = temp;
    }
}
