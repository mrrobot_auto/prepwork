package java8;

public class dpworld {
    public static void main(String[] args) {
        new dpworld().rotate("SUJAY","AYSUJ");
    }

    /**
     * str1 = "SUJAY"
     * str2 = "AYSUJ"
     */

    boolean rotate(String s1, String s2){
        boolean res = false;
        char[] chars1 = s1.toCharArray();
        char[] chars2 = s2.toCharArray();
        int first = s2.indexOf(chars1[0]);
        int index=0;
        while(index < chars1.length){
            res = chars1[index] == chars2[first];
            if(!res) break;
            first++;
            if(first >= (chars2.length-1)){
                first = 0;
            }
            index++;
        }
        return res;
    }
}
