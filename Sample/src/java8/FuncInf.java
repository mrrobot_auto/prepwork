package java8;

@FunctionalInterface
public interface FuncInf {
    public abstract String m1(String s1, String s2);

    default void test1(String s) {
        System.out.println("length is " + s.length());
    }
}

class test {
    public static void main(String[] args) {
        FuncInf i = (a, b) -> a + " " + b;
        String s = i.m1("pooja", "ghorpade");
        System.out.println(s);
        i.test1(s);
    }

}
