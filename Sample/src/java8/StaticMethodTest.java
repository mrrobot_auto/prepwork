package java8;

public class StaticMethodTest {
    static {
        System.out.println("In Static");
    }
    {
        System.out.println("IN Normal1");
    }
    StaticMethodTest() {
        System.out.println("In COnst");
    }
    {
        System.out.println("IN Normal2");
    }

    public static void main(String[] args) {
        StaticMethodTest t2 = new StaticMethodTest();
        StaticMethodTest t1 = new StaticMethodTest();

        //static
        //constructor
        //constructor
    }
}
