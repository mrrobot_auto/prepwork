package java8;

import java.util.Arrays;
import java.util.Collections;

public class Java8Tester {
    public static void main(String args[]) {
        Integer coins[] = {6,5,2,1};
        //array in desc sort
        Arrays.sort(coins, Collections.reverseOrder());
        int arrSize = coins.length;
        int expectedSum = 11;
//        int noOfIterations = minCoins(coins, arrSize, expectedSum);
        int noOfIterations = findMinIterations(coins, arrSize, expectedSum);
        System.out.println(noOfIterations);

    }

    private static int minCoins(Integer[] coins, int arrSize, int expectedSum) {
        int noOfIterations = 0;

        //if number of coin req is 0
        if(expectedSum==0){
            return noOfIterations;
        }

        //if number of coin req is 1
        for (int i = 0; i < arrSize ; i++) {
            //first iteration on 6
            if(coins[i] == expectedSum){
                return 1;
            }
        }

        //if same coin can sum up the expectedSum
        for (int i = 0; i < arrSize ; i++) {
            //first iteration on 6
            if(coins[i] > expectedSum){
                continue;
            }
            if(coins[i] < expectedSum){
                if(expectedSum%coins[i] == 0){
                    noOfIterations = expectedSum/coins[i];
                    return noOfIterations;
                }
            }
        }
        noOfIterations = findMinIterations(coins, arrSize, expectedSum);
        return noOfIterations;
    }

    static int findMinIterations(Integer[] coins, int arrSize, int expectedSum){
        {
            // base case
            if (expectedSum == 0) return 0;
            // Initialize result
            int res = Integer.MAX_VALUE;
            // Try every coin that has smaller value than V
            for (int i=0; i<arrSize; i++)
            {
                if (coins[i] <= expectedSum)
                {
                    int sub_res = findMinIterations(coins, arrSize, expectedSum-coins[i]);
                    // Check for INT_MAX to avoid overflow and see if
                    // result can minimized
                    if (sub_res != Integer.MAX_VALUE && sub_res + 1 < res)
                        res = sub_res + 1;
                }
            }
            return res;
        }
    }
}
