package java8;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Oracle2 {
    static List<Integer> list = new ArrayList<>();

    public static void main(String[] args) {
        Thread t1 = new Thread(() ->
        {
            try {
                new Oracle2().m1();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                new Oracle2().m2();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        System.out.println("Main thread");
    }

    public void m1() throws InterruptedException {
        IntStream.range(0, 500).forEach(o -> {
            list.add(o);
        });

        list.forEach(System.out::println);
        Thread.currentThread().wait();
    }

    public void m2() throws InterruptedException {
        IntStream.range(0, 500).forEach(o -> {
            list.add(o);
        });
        list.forEach(System.out::println);
        Thread.currentThread().wait();
    }

}
