package java8;

import java.util.*;

public class Oracle {
    public static void main(String[] args) {
        new Oracle().print("a50jdgksg60djfskd70sdf");
        new Oracle().sort(new String[]{"ac","abbde","a","abc"});
        List<Integer> li = new Oracle().test(new String[]{"f","23","26","-1","23h","111"});
        li.forEach(System.out::println);
    }

    public void printSecondHighest() {
        int[] arr = new int[]{20, 40, 60, 10, 30, 50, 60};
        int index = arr.length - 2;
        for (int i = arr.length - 1; i > 1; i--) {
            if (arr[i] == arr[i - 1]) {
                index--;
            }
        }
        System.out.println(arr[index]);
    }

    public List<Integer> test(String[] o) {
        List<Integer> list = new ArrayList<>();
        int temp;
        for (String obj : o) {
            try {
                temp = Integer.parseInt(obj);
                if(temp == -1) list.add(temp);
            } catch (NumberFormatException e){
                temp = -1;
            }
            if(temp != -1) list.add(temp);
        }
        return list;
    }
    public void print(String str){
        char[] chars = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for(char c : chars){
            try{
                int t = Integer.parseInt(String.valueOf(c));
                sb.append(t);
            }catch (NumberFormatException e){
                String temp = sb.toString();
                sb = new StringBuilder();
                if(!temp.equals(""))System.out.println(Integer.parseInt(temp));
            }
        }
    }

    public void sort(String[] arr){
        Arrays.sort(arr, Comparator.comparingInt(String::length));
        Map<Integer,String> map = new HashMap<>();
        List<Integer> arIn = new ArrayList<>();
        for(String s : arr){
            map.put(s.length(),s);
            arIn.add(s.length());
        }
        Collections.sort(arIn,Collections.reverseOrder());
        for(int i : arIn){
            System.out.println(map.get(i));
        }
    }

}
