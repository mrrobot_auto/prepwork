package pattern;

import java.util.Scanner;

public class RightTriangle {
	/*1>>>4321
      432
      43
      4*/
	/*2>> 
	4
	43
	432
	4321*/

    public static void main(String[] args) {

       // first();
        System.out.println("*******************************");
        second();

    }

    private static void second() {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        for (int i = num; i >= 1; i--) {
            for (int j = num; j > i; j--) {
                System.out.print(j);
            }
            System.out.println(i);
        }
        scan.close();
    }

    private static void first() {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        for (int i = 1; i <= num; i++) {
            for (int j = num; j >= i; j--) {
                System.out.print(j);
            }
            System.out.println();
        }
        scan.close();
    }
}
