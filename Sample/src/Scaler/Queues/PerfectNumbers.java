package Scaler.Queues;

import java.util.LinkedList;
import java.util.Queue;

public class PerfectNumbers {
    public static void main(String[] args) {
        new PerfectNumbers().getNumber(4);
    }
    public String getNumber(int A){
        Queue<String> q = new LinkedList<>();
        q.offer("1");
        q.offer("2");
        if(A==1) return "11";
        if(A==2) return "22";
        for(int i = 0; i <= A-2; i++){
            String s = q.poll();
            q.offer(s+"1");
            q.offer(s+"2");
        }
        String res =q.poll();
        return res+new StringBuilder(res).reverse();
    }
}
