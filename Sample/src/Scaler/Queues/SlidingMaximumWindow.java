package Scaler.Queues;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class SlidingMaximumWindow {
    public static void main(String[] args) {
        new SlidingMaximumWindow().slidingMaximum(new int[]{1, 3, -1, -3, 5, 3, 6, 7}, 3);
    }

    public ArrayList<Integer> slidingMaximum(final int[] A, int B) {
        Deque<Integer> deq = new LinkedList<>();
        ArrayList<Integer> res = new ArrayList<>();
        for (int i = 0; i < A.length; i++) {
            while (!deq.isEmpty() && A[deq.peekLast()] < A[i]) {
                deq.removeLast();
            }
            deq.addLast(i);
            if (i-deq.peekFirst() == B) {
                deq.removeFirst();
            }
            if (i >= (B-1)) {
                res.add(A[deq.peekFirst()]);
            }
        }
        return res;
    }
}
