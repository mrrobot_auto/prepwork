package Scaler;

public class Flip {
    public static void main(String[] args) {
        new Flip().flip("10010");
    }

    public int[] flip(String A) {
        int L = -1;
        int R = -1;
        char[] chars = A.toCharArray();
        int sum = 0;
        int localSum = 0;
        boolean isOne = false;
        for (char c : chars) {
            if (c == '1') {
                isOne = true;
            } else {
                isOne = false;
                break;
            }
        }
        if (isOne) return new int[]{};
        int index = -1;
        for (int i = 0; i < A.length(); i++) {
            if(chars[i]-'0' == 0){
                localSum += 1;
            }else {
                localSum -=1;
            }
            if(localSum<0){
                localSum =0;
                index = i;
            }
            if(localSum>sum){
                sum = localSum;
                if(L<0){
                    L=i;
                }else if(index != -1){
                    L=index+1;
                    index = -1;
                }
                R =i;
            }

        }

        return new int[]{L+1,R+1};
    }
}
