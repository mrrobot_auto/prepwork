package Scaler;

import java.util.HashSet;
import java.util.stream.Collectors;

public class Combinations {
    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        combine(1,4, 2,  "", set);
        set.stream().sorted().collect(Collectors.toList());
    }

    public static void combine(int i, int a, int b, String str, HashSet<Integer> set) {
        if(b==0) set.add(Integer.valueOf(str));
        if(i>a) return;
        combine(i + 1, a,b, str,set);
        combine(i+1,a,b-1,str+i,set);
    }
}
