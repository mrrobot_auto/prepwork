package Scaler;

public class NextPermutation {
    public static void main(String[] args) {
        new NextPermutation().nextPermutation(new int[]{5,4,3,2,1});
    }
    public int[] nextPermutation(int[] A) {
        int n = A.length;
        int smallIndex = -1;
        int justLargeIndex;
        for(int i = n-1; i >0 ; i--){
            if(A[i]>A[i-1]){
                smallIndex=i-1;
                break;
            }
        }
        if(smallIndex == -1) return A;
        justLargeIndex = smallIndex+1;
        for(int i = smallIndex+1; i<n; i++){
            if(A[justLargeIndex]>A[i] && A[i]>A[smallIndex]){
                justLargeIndex = i;
            }
        }
        swap(A,smallIndex,justLargeIndex);
        reverse(A,smallIndex+1);
        return A;
    }
    public void swap(int[] A, int i , int j){
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
    public void reverse(int[] A, int i){
        int n = A.length;
        while(i<n){
            swap(A,i,n-1);
            i++;
            n--;
        }
    }
}
