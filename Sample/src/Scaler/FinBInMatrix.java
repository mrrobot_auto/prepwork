package Scaler;

public class FinBInMatrix {
    public static void main(String[] args) {
        new FinBInMatrix().solve(new int[][]{{2,8,8,8},{2,8,8,8},{2,8,8,8}},8);
    }
    public int solve(int[][] A, int B) {
        int N = A.length;
        int M = A[0].length;
        int i = 0;
        int j = M-1;
        int res = Integer.MAX_VALUE;
        while(j>=0 && i<N){
            if(A[i][j] == B){
                res = Math.min((i+1)*1009+(j+1),res);
            }
            if(A[i][j] >= B){
                j--;
            }else{
                i++;
            }
        }
        return res == Integer.MAX_VALUE?-1:res;
    }
}
