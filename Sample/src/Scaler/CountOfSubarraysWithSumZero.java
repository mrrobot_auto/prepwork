package Scaler;

import java.util.HashMap;
import java.util.Map;

public class CountOfSubarraysWithSumZero {
    public static void main(String[] args) {
        coubtOfsubarrays(new int[]{30,-30,30,-30});
    }
    public static int coubtOfsubarrays(int[] A) {
        HashMap<Integer,Integer> map = new HashMap<>();
        int count = 0;
        int mod = 1000000007;
        int N = A.length;
        for(int i = 1; i<N; i++){
            A[i] += A[i-1];
        }
        for(int i : A){
            if(map.containsKey(i)){
                map.put(i,map.get(i)+1);
            }else{
                map.put(i,1);
            }
        }
        for(Map.Entry<Integer,Integer> entry : map.entrySet()){
            int k = entry.getKey();
            int v = entry.getValue();
            if(v>1){
                count = count + ((v*(v-1))/2);
            }
            if(k == 0){
                count += v;
            }

        }
        return count%mod;
    }
}
