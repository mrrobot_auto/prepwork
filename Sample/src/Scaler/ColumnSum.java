package Scaler;

public class ColumnSum {
    public static void main(String[] args) {
        colSum(new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}});
    }
    public static int[] colSum(int[][] A) {
        int[] res = new int[A[0].length];
        int index = 0;
        for(int i = 0; i < A.length ; i++){
            for(int j = 0; j < A[0].length; j++){
                res[index] += A[j][i];
            }
            index++;
        }
        return res;
    }
}
