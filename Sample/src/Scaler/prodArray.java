package Scaler;

import java.util.Arrays;

public class prodArray {
    public static void main(String[] args) {
        proInts(new int[]{1, 2, 3, 4, 5});
    }

    public static int[] proInts(int[] A) {
        int[] prod = new int[A.length];

        Arrays.fill(prod, -1);
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A.length; j++) {
                if (i != j) {
                    if (prod[i] == -1) {
                        prod[i] = A[j];
                    }else {
                        prod[i] = prod[i] * A[j];
                    }
                }
            }

        }
        return prod;
    }
}
