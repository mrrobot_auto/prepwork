package Scaler.LinkedList;

import leetcode.ListNode;


public class MergeTwoLinkedList {
    public static void main(String[] args) {
        ListNode A = new ListNode().addArrayInListNode(new int[]{1,2,3});
        ListNode B = new ListNode().addArrayInListNode(new int[]{4,5,6});
        new MergeTwoLinkedList().mergeTwoLists(B,A);
    }
    public ListNode mergeTwoLists(ListNode A, ListNode B) {
        ListNode dummy = new ListNode(-1);
        ListNode t2;
        if(A.val<=B.val){
            dummy.next = A;
            t2 = B;
        }else{
            dummy.next = B;
            t2 = A;
        }
        ListNode t1 = dummy.next;
        ListNode tail = t1.next;
        while(tail!=null && t2!=null){
            if(t2.val<=tail.val){
                t1.next = t2;
                t2 = t2.next;
                t1=t1.next;
            }else{
                t1.next = tail;
                tail = tail.next;
                t1=t1.next;
            }
        }
        if(t2!=null){
            t1.next = t2;
        }
        if(tail!=null){
            t1.next = tail;
        }
        return dummy.next;
    }
}
