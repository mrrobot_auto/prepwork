package Scaler.LinkedList;

import leetcode.ListNode;

public class MergeSortLL {
    public static void main(String[] args) {
        new MergeSortLL().sortList(new ListNode().addArrayInListNode(new int[]{3,4,2,8}));
    }
    public ListNode sortList(ListNode head) {
        if(head == null || head.next == null) return head;
        ListNode mid = getMiddle(head);
        ListNode head2 = mid.next;
        mid.next = null;
        sortList(head);
        sortList(head2);
        return mergeTwoLists(head,head2);
    }
    public ListNode mergeTwoLists(ListNode A, ListNode B) {
        if(A == null){
            return B;
        }
        if(B == null){
            return A;
        }
        ListNode dummy = new ListNode(-1);
        ListNode t2;
        if(A.val<=B.val){
            dummy.next = A;
            t2 = B;
        }else{
            dummy.next = B;
            t2 = A;
        }
        ListNode t1 = dummy.next;
        ListNode tail = t1.next;
        while(tail!=null && t2!=null){
            if(t2.val<=tail.val){
                t1.next = t2;
                t2 = t2.next;
                t1=t1.next;
            }else{
                t1.next = tail;
                tail = tail.next;
                t1=t1.next;
            }
        }
        if(t2!=null){
            t1.next = t2;
        }
        if(tail!=null){
            t1.next = tail;
        }
        return dummy.next;
    }
    public ListNode getMiddle(ListNode A) {
        ListNode slow = A;
        ListNode fast = A;
        while(fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

}
