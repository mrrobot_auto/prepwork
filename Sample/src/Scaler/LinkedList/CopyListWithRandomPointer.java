package Scaler.LinkedList;

public class CopyListWithRandomPointer {
    public static void main(String[] args) {
        RandomListNode node1 = new RandomListNode(1);
        RandomListNode node2 = new RandomListNode(2);
        RandomListNode node3 = new RandomListNode(3);
        RandomListNode node4 = new RandomListNode(4);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = null;
        node1.random = node3;
        node2.random = node4;
        node4.random = node1;
        node3.random = null;
        new CopyListWithRandomPointer().copyRandomList(node1);
    }

    public RandomListNode copyRandomList(RandomListNode head) {
        RandomListNode temp = head;
        while (temp != null) {
            RandomListNode node = new RandomListNode(temp.label);
            node.next = temp.next;
            temp.next = node;
            temp = temp.next.next;
        }
        temp = head;
        while (temp != null) {
            if (temp.random == null)
                temp.next.random = null;
            else
                temp.next.random = temp.random.next;
            temp = temp.next.next;
        }
        head = head.next;
        temp = head;
        while (temp.next != null) {
            temp.next = temp.next.next;
            temp = temp.next;
        }
        return head;
    }
}
