package Scaler.LinkedList;

import leetcode.ListNode;

import java.util.List;

public class Pallindrome {
    public static void main(String[] args) {
        ListNode l = new ListNode();
        ListNode k = l.addArrayInListNode(new int[]{3,3});
        new Pallindrome().lPalin(k);
    }
    public int lPalin(ListNode head) {
        ListNode temp = head;
        int size = 0;
        while(temp != null){
            size++;
            temp = temp.next;
        }
        int mid = size/2;
        temp = head;
        for(int i = 0; i < mid; i++){
            temp = temp.next;
        }
        ListNode reversed = reverseList(temp);
        while(head != null && reversed!=null){
            if(head.val != reversed.val){
                return 0;
            }
            head = head.next;
            reversed = reversed.next;
        }
        return 1;
    }
    public ListNode reverseList(ListNode head) {
        if(head.next == null) return head;
        ListNode curr = head;
        ListNode prev = null;
        while(curr!=null){
            ListNode temp = curr.next;
            curr.next = prev;
            prev = curr;
            curr = temp;
        }
        return prev;
    }
}
