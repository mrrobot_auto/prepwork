package Scaler.LinkedList;

import java.util.HashMap;

public class LRUCache {
    public static void main(String[] args) {
        var cache = new LRUCache(4);
        cache.set(5, 13);
        cache.set(9, 6);
        cache.set(4, 1);
        System.out.println(cache.get(4));
    }

    private int capacity;
    private HashMap<Integer, DLLNode> map;
    DLLNode head = new DLLNode(-1, -1);
    DLLNode tail = new DLLNode(-2, -2);

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        if (map.containsKey(key)) {
            int val = map.get(key).value;
            this.delete(key);
            DLLNode node = addToTail(key,map.get(key).value);
            map.remove(key);
            map.put(key,node);
            return val;
        } else {
            return -1;
        }
    }

    public void set(int key, int value) {
        if (map.containsKey(key)) {
            hit(key, value);
        } else {
            if (map.size() >= capacity) {
                int tKey = head.next.key;
                delete(tKey);
                map.remove(tKey);
            }
            DLLNode node = addToTail(key,value);
            map.put(key,node);
        }
    }

    private void hit(int key, int value) {
        delete(key);
        map.remove(key);
        DLLNode node = addToTail(key, value);
        map.put(key, node);
    }

    private void delete(int key){
        DLLNode node = map.get(key);
        node.next.prev = node.prev;
        node.prev.next = node.next;
    }
    private DLLNode addToTail(int key, int value){
        DLLNode node = new DLLNode(key,value);
        node.prev = tail.prev;
        tail.prev.next = node;
        tail.prev = node;
        node.next = tail;
        return node;
    }

    static class DLLNode {
        int key;
        int value;
        DLLNode next;
        DLLNode prev;

        public DLLNode(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }
}
