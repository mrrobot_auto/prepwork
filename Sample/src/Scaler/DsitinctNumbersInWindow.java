package Scaler;

import java.util.HashMap;
import java.util.Scanner;

public class DsitinctNumbersInWindow {
    public static void main(String[] args) {


        solve(9);
    }
    public static void solve(int A) {
        if(A<1) {
            return;
        }
        solve(A-1);
        System.out.print(A);
    }
    public static int[] dNums(int[] A, int B) {
        if(A.length<2) return new int[]{A.length};
        HashMap<Integer,Integer> map = new HashMap<>();
        int[] res = new int[A.length-B+1];
        for(int i = 0; i<B; i++){
            addInMap(map,i);
        }
        res[0] = map.size();
        int si = 0;
        int ei = B;
        int index = 1;
        while(ei<A.length){
            removeFromMap(map,A[si]);
            addInMap(map,A[ei]);
            res[index] = map.size();
            si++;
            ei++;
            index++;
        }
        return res;
    }
    static void addInMap(HashMap<Integer,Integer> map, int val){
        map.put(val,map.getOrDefault(val,0)+1);
    }
    static void removeFromMap(HashMap<Integer,Integer> map, int val){
        if(map.containsKey(val)){
            if(map.get(val)>1){
                map.put(val,map.get(val)-1);
            }else{
                map.remove(val);
            }
        }
    }
}
