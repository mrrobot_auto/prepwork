package Scaler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LongestSubarraySumZero {
    public static void main(String[] args) {
        solve(new int[]{9,-20,-11,-8,-4,2,-12,14,1});
        Set<Integer> integers = new HashSet<>();

    }

    public static int solve(int[] A) {
        HashMap<Integer, Integer[]> map = new HashMap<>();
        int res = 0;
        for (int i = 1; i < A.length; i++) {
            A[i] += A[i - 1];
        }
        int index = 0;
        for (int i : A) {
            if (map.containsKey(i)) {
                Integer[] temp = map.get(i);
                temp[1] = index;
            } else {
                map.put(i, new Integer[]{index, 0});
            }
            index++;
        }
        for (Map.Entry<Integer, Integer[]> entry : map.entrySet()) {
            Integer k = entry.getKey();
            Integer[] v = entry.getValue();
            if (v[1] > 0) {
                res = Math.max(res, (A[1] - A[0]));
            }
        }
        return res;
    }
}
