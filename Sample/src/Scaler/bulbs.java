package Scaler;

public class bulbs {
    public static void main(String[] args) {
        bulbs(new int[]{1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1});
    }
    public static int bulbs(int[] A) {
        int count = 0;
        int index = 0;
        boolean wasZero = false;
        while(index < A.length){
            if(A[index] == 0 && !wasZero){
                count++;
                wasZero = true;
            }else{
                wasZero = false;
            }
            index++;
        }
        int res = count*2;
        if(wasZero){
            res-=1;
        }
        return res;
    }
}

