package Scaler;

public class ReverseAllBits {
    public static void main(String[] args) {
        new ReverseAllBits().reverse(3);
    }
    public long reverse(long A) {
        long res = 0;
        for(int i = 31; i>=0 ; i--){
            if(checkBit(A,i)){
                res = toggle(res,31-i);
            }
        }
        return res;
    }
    public boolean checkBit(long A, int B){
        return (A | 1L <<B) == A;
    }
    public long toggle(long A, int B){
        return (A ^ (1L <<B));
    }
}
