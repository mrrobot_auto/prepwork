package Scaler;

public class SubarrayWithLeastAverage {
    public static void main(String[] args) {
        new SubarrayWithLeastAverage().solve(new int[]{15,3,15,6,9,14,8,10,9,17},1);
    }
    public int solve(int[] A, int B) {
        int startIndex = 0;
        int sum = 0;
        int res = startIndex;
        while(startIndex<B){
            sum += A[startIndex];
            startIndex++;
        }
        startIndex = 1;
        int endIndex = B;
        int k = sum;
        while(endIndex<A.length){
            k -= A[startIndex-1];
            k += A[endIndex];
            if(k < sum){
                sum = k;
                res = startIndex;
            }
            endIndex++;
            startIndex++;
        }
        return res;
    }
}
