package Scaler.Trees;

import leetcode.TreeNode;

public class CheckBST {
    public static void main(String[] args) {

    }
    int isBST = 1;
    TreeNode prev = null;
    public int isValidBST(TreeNode A) {
        traverse(A);
        return isBST;
    }
    public void traverse(TreeNode current){
        if(current == null) return;
        traverse(current.left);
        if(prev!=null && current.val<=prev.val){ //value current node will always be higher than value of previous node
            isBST = 0;
        }
        prev = current;
        traverse(current.right);
    }
}
