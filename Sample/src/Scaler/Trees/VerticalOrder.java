package Scaler.Trees;

import geeksforgeeks.TreeNode;

import java.util.*;

public class VerticalOrder {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(6);
        root.left = new TreeNode(3);
        root.left.left = new TreeNode(2);
        root.left.right = new TreeNode(5);
        root.right = new TreeNode(7);
        root.right.right = new TreeNode(9);
        new VerticalOrder().verticalOrderTraversal(root);
    }
    public ArrayList<ArrayList<Integer>> verticalOrderTraversal(TreeNode A) {
        Queue<Pair> q = new LinkedList<>();
        Pair pair = new Pair();
        pair.level = 0;
        pair.node = A;
        HashMap<Integer,LinkedList<Integer>> map = new HashMap<>();
        q.offer(pair);
        int minL=Integer.MAX_VALUE,maxL=Integer.MIN_VALUE;
        while(!q.isEmpty()){
            Pair p = q.poll();
            minL = Math.min(p.level,minL);
            maxL = Math.max(p.level,maxL);
            if(map.containsKey(p.level)){
                LinkedList<Integer> list = map.get(p.level);
                list.add(p.node.val);
            }else{
                LinkedList<Integer> l = new LinkedList<>();
                l.add(p.node.val);
                map.put(p.level,l);
            }
            if(p.node.left != null){
                Pair np = new Pair();
                np.level = p.level+1;
                np.node = p.node.left;
                q.offer(np);
            }
            if(p.node.right != null){
                Pair np = new Pair();
                np.level = p.level-1;
                np.node = p.node.right;
                q.offer(np);
            }
        }
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        for(int i = minL ; i <=maxL; i++){
            res.add(new ArrayList<>(map.get(i)));
        }
        return res;
    }
}
