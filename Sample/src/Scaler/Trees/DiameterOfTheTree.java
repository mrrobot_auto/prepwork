package Scaler.Trees;

import leetcode.TreeNode;

public class DiameterOfTheTree {
    public static void main(String[] args) {

    }
    int ans = 0;
    public int solve(TreeNode A) {
        dia(A);
        return ans;
    }
    int height(TreeNode root){
        if(root == null) return -1;
        return Math.max(height(root.left),height(root.right))+1;
    }
    void dia(TreeNode root){
        if(root == null) return;
        ans = Math.max(ans,(height(root.left)+height(root.right))+2);
        dia(root.left);
        dia(root.right);
    }
}
