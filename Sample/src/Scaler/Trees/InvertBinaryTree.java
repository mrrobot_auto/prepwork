package Scaler.Trees;

import leetcode.TreeNode;

public class InvertBinaryTree {
    public static void main(String[] args) {

    }
    public TreeNode invertTree(TreeNode root) {
        invert(root);
        return root;
    }
    public void invert(TreeNode root) {
        if(root == null) return;
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;
        invert(root.left);
        invert(root.right);
    }
}
