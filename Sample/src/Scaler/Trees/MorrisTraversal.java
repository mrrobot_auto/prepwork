package Scaler.Trees;

import leetcode.TreeNode;

import java.util.ArrayList;

public class MorrisTraversal {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(8, 4, 13);
        root.left.left = new TreeNode(3, 1, null);
        root.left.right = new TreeNode(7);
        root.left.left.left.left = new TreeNode(0);
        root.left.left.left.right = new TreeNode(2);
        root.right.right = new TreeNode(15, null, 1);
        root.right.left = new TreeNode(10);
        root.right.right.right = new TreeNode(17, 16, 20);
        var list = new MorrisTraversal().solve(root);
        list.forEach(System.out::println);
    }

    public ArrayList<Integer> solve(TreeNode A) {
        ArrayList<Integer> list = new ArrayList<>();
        TreeNode curr = A;
        while (curr != null) {
            if (curr.left == null) {
                list.add(curr.val);
                curr = curr.right;
            } else {
                TreeNode temp = curr.left;
                while (temp.right != null && temp.right != curr) {
                    temp = temp.right;
                }
                if (temp.right == null) {
                    temp.right = curr;
                    curr = curr.left;
                } else {
                    temp.right = null;
                    list.add(curr.val);
                    curr = curr.right;
                }
            }
        }
        return list;
    }
}
