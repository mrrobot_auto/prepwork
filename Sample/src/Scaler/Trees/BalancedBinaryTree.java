package Scaler.Trees;

import geeksforgeeks.TreeNode;

public class BalancedBinaryTree {
    public static void main(String[] args) {

    }
    int res = 1;
    public int isBalanced(TreeNode A) {
        getHeight(A);
        return res;
    }
    public int getHeight(TreeNode root){
        if(root == null) return -1;
        int left = getHeight(root.left);
        int right = getHeight(root.right);
        if(Math.abs(left-right)>1){
            res = 0;
        }
        return 1 + Math.max(left,right);
    }
}
