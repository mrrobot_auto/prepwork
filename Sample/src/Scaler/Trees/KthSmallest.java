package Scaler.Trees;

import leetcode.TreeNode;

public class KthSmallest {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(8, 4, 13);
        root.left.left = new TreeNode(3, 1, null);
        root.left.right = new TreeNode(7);
        root.left.left.left.left = new TreeNode(0);
        root.left.left.left.right = new TreeNode(2);
        root.right.right = new TreeNode(15, null, 1);
        root.right.left = new TreeNode(10);
        root.right.right.right = new TreeNode(17, 16, 20);
        new KthSmallest().kthsmallest(root,7);

    }
    public int kthsmallest(TreeNode root, int k) {
        return inorder(root,0,k-1);
    }
    int inorder(TreeNode root, int v, int k){
        if(root == null) return -10;

        int ans = inorder(root.left,v+1,k);
        if(v == k) return root.val;
        if(ans != -10) return ans;
        ans = inorder(root.right,v+1,k);

        return ans;
    }
}
