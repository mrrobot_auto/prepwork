package Scaler.Trees;

import leetcode.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LowestCommonAncestor {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(8, 4, 13);
        root.left.left = new TreeNode(3, 1, null);
        root.left.right = new TreeNode(7);
        root.left.left.left.left = new TreeNode(0);
        root.left.left.left.right = new TreeNode(2);
        root.right.right = new TreeNode(15, null, 1);
        root.right.left = new TreeNode(10);
        root.right.right.right = new TreeNode(17, 16, 20);
        ArrayList<Integer> list = new ArrayList<>();
        new LowestCommonAncestor().lca(root, 20, 17);
        System.out.println(list);
    }

    public int lca(TreeNode A, int B, int C) {
        ArrayList<Integer> Blist = new ArrayList<>();
        registerPath(A, B, Blist);
        ArrayList<Integer> Clist = new ArrayList<>();
        registerPath(A, C, Clist);
        Collections.reverse(Blist);
        Collections.reverse(Clist);
        int i = 0;
        int j = 0;
        int s = Math.min(Blist.size(), Clist.size());
        int res = s;
        while (i < Blist.size() && j < Clist.size()){
                if((int)Blist.get(i) != (int)Clist.get(j)){
                    res = i;
                    break;
                }
            i++;
            j++;
        }
        return Blist.size() > Clist.size() ? Clist.get(res-1) : Blist.get(res-1);
    }

    public boolean registerPath(TreeNode A, int B, ArrayList<Integer> list) {
        if (A == null) return false;
        if (A.val == B) {
            list.add(A.val);
            return true;
        }
        boolean t1 = registerPath(A.left, B, list) || registerPath(A.right, B, list);
        if (t1) {
            list.add(A.val);
            return true;
        }
        return false;
    }
}
