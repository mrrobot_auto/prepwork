package Scaler.Trees;


import leetcode.TreeNode;

public class DeleteNodeFromBST {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(8, 4, 13);
        root.left.left = new TreeNode(3, 1, null);
        root.left.right = new TreeNode(7);
        root.left.left.left.left = new TreeNode(0);
        root.left.left.left.right = new TreeNode(2);
        root.right.right = new TreeNode(15, null, 1);
        root.right.left = new TreeNode(10);
        root.right.right.right = new TreeNode(17, 16, 20);
        var newRoot = new DeleteNodeFromBST().delete(root, 4);

    }

    public TreeNode delete(TreeNode root, int K) {
        TreeNode prev = null;
        TreeNode curr = root;
        while (curr != null) {
            if (curr.val == K) {
                if (curr.left == null && curr.right == null) {
                    if(prev == null){
                        return null;
                    }
                    if (curr.val > prev.val) {
                        prev.right = null;
                    } else {
                        prev.left = null;
                    }
                    break;
                } else if (curr.left == null || curr.right == null) {
                    if (curr.left != null) {
                        if(prev == null){
                            return curr.left;
                        }
                        if (prev.val > curr.left.val) {
                            prev.left = curr.left;
                        } else {
                            prev.right = curr.left;
                        }
                    } else {
                        if(prev == null){
                            return curr.right;
                        }
                        if (prev.val > curr.right.val) {
                            prev.left = curr.right;
                        } else {
                            prev.right = curr.right;
                        }
                    }
                    break;
                }else{
                    TreeNode temp = curr.left;
                    while(temp.right != null){
                        temp = temp.right;
                    }
                    int t = temp.val;
                    temp.val = curr.val;
                    curr.val = t;
                    curr.left = delete(curr.left,K);
                }
            } else if (curr.val > K) {
                prev = curr;
                curr = curr.left;
            } else {
                prev = curr;
                curr = curr.right;
            }

        }
        return root;
    }
}
