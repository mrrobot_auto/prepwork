package Scaler;

public class KthSymbolStringRecursion {
    public static void main(String[] args) {
        new KthSymbolStringRecursion().solve(9, 140);
        //new KthSymbolStringRecursion().append1("L",3);
    }

    public int solve(int A, int B) {
        if(A<=3) return Integer.parseInt(String.valueOf("0110".charAt(B)));
        String L = "0110";
        String R = "1001";
        String s = append1("L",A-3);
        String se = "L";
        for(int i=0,j = 0;j<B;i++,j=j+4){
            se = String.valueOf(s.charAt(i));
        }
        int index = (B%4);

        if(se.equals("L")){
            return Integer.parseInt(String.valueOf(L.charAt(index)));
        }else
            return Integer.parseInt(String.valueOf(R.charAt(index)));
    }

    String append(String S, int A) {
        if (A < 1) return S;
        int n = S.length();
        for (int i = 0; i < n; i++) {
            if (S.charAt(i) == '0') {
                S = S + "1";
            } else {
                S = S + "0";
            }
        }
        return append(S, A - 1);
    }

    String append1(String S, int B) {
        if (B < 1) return S;
        int n = S.length();
        for (int j = 0; j < n; j++) {
            if (S.charAt(j) == 'L') {
                S = S + "R";
            } else {
                S = S + "L";
            }
        }
        return append1(S,B-1);
    }
}
