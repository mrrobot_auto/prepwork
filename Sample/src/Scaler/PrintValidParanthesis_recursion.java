package Scaler;

public class PrintValidParanthesis_recursion {
    public static void main(String[] args) {
        new PrintValidParanthesis_recursion().print(3,0,0,"");
    }
    public void print(int N, int open, int close, String str){
        if(str.length() == 2*N){
            System.out.printf(str);
            return;
        }
        if(open<N)
            print(N,open+1,close,str+"(");
        if(close<open)
            print(N,open,close+1,str+")");
    }
}
