package Scaler;

import java.util.Scanner;

public class ReverseStringRecursion {
    public static void main(String[] args) {
        // YOUR CODE GOES HERE
        // Please take input and print output to standard input/output (stdin/stdout)
        // DO NOT USE ARGUMENTS FOR INPUTS
        // E.g. 'Scanner' for input & 'System.out' for output
        Scanner scan = new Scanner(System.in);
        String input = scan.next();
        System.out.print(reverse(input,input.length()-1));

    }
    static String reverse(String A,int index){
        if(index<0){
            return "";
        }
        return A.charAt(index)+""+reverse(A, index - 1);
    }
}
