package Scaler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MedianOfTwoSortedArrays {
    public static void main(String[] args) {
        new MedianOfTwoSortedArrays().findMedianSortedArrays(List.of(),List.of(20));
    }
    public double findMedianSortedArrays(final List<Integer> a, final List<Integer> b) {
        List<Integer> list = merge(a,b);
        if(list.size()<=1) return list.get(0);
        if(list.size()%2==0){
            double one = list.get(list.size()/2);
            double two = list.get((list.size()/2)-1);
            return (one+two)/2;
        }else{
            return list.get(list.size()/2);
        }
    }
    ArrayList<Integer> merge(final List<Integer> a, final List<Integer> b){
        int l = 0;
        int r = 0;
        ArrayList<Integer> res = new ArrayList<>();
        int m = a.size();
        int n = b.size();
        while(l<m && r<n){
            if(a.get(l)<b.get(r)){
                res.add(a.get(l));
                l++;
            }else{
                res.add(b.get(r));
                r++;
            }
        }
        while(l<m){
            res.add(a.get(l));
            l++;
        }
        while(r<n){
            res.add(b.get(r));
            r++;
        }
        return res;
    }
}
