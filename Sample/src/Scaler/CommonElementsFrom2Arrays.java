package Scaler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommonElementsFrom2Arrays {
    public static void main(String[] args) {

    }
    public int[] solve(int[] A, int[] B) {
        // Just write your code below to complete the function. Required input is available to you as the function arguments.
        // Do not print the result or any output. Just return the result via this function.\
        HashMap<Integer,Integer> map = new HashMap<>();
        List<Integer> list = new ArrayList<>();
        for(int i : A){
            map.put(i,map.getOrDefault(i,0)+1);
        }
        for(int i : B){
            if(map.containsKey(i) && map.get(i)>0){
                map.put(i,map.get(i)-1);
                list.add(i);
            }
        }
        int[] res = new int[list.size()];
        int index = 0;
        for(int i : list){
            res[index] = i;
            index++;
        }
        return res;
    }
}
