package Scaler;

import geeksforgeeks.MaximumSubArray;

public class MaximumSubMatrixSum {
    public static void main(String[] args) {
        new MaximumSubMatrixSum().solve(new int[][]{
                {-83,-73,-70,-61},
                {-56,-48,-13,4},
                {38,48,71,71}});
    }
    public long solve(int[][] A) {
        int N = A.length;
        int M = A[0].length;
        long[][] rowWiseSum = new long[N][M];
        long[][] prefixSum = new long[N][M];
        for (int i = 0; i < N; i++) {
            rowWiseSum[i][M-1] = A[i][M-1];
        }
        for (int i = N-1; i >= 0; i--) {
            for (int j = M-2; j >= 0; j--) {
                rowWiseSum[i][j] = rowWiseSum[i][j + 1] + A[i][j];
            }
        }
        for (int i = M-1; i >=0; i--) {
            prefixSum[N-1][i] = rowWiseSum[N-1][i];
        }
        for (int j = M-1; j >= 0; j--) {
            for (int i = N-2; i >= 0; i--) {
                prefixSum[i][j] = prefixSum[i + 1][j] + rowWiseSum[i][j];
            }
        }
        long max = Long.MIN_VALUE;
        for(long[] i : prefixSum){
            for(long j : i){
                max = (int)Math.max(max,j);
            }
        }
        return max;
    }
}
