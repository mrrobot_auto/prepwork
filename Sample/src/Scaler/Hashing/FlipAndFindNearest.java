package Scaler.Hashing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class FlipAndFindNearest {
    public static void main(String[] args) {
        new FlipAndFindNearest().solve("10001010",
                List.of(
                        List.of(2, 5),
                        List.of(1, 5),
                        List.of(2, 5),
                        List.of(1, 7),
                        List.of(2, 8),
                        List.of(1, 2),
                        List.of(2, 5),
                        List.of(1, 2),
                        List.of(1, 1),
                        List.of(2, 4), List.of(1, 4), List.of(1, 3)
                ));
        //[[2,5],[1,5],[2,5],[1,7],[2,8],[1,2],[2,5],[1,2],[1,1],[2,4],[1,4],[1,3]]
    }

    public ArrayList<Integer> solve(String A, List<List<Integer>> B) {
        char[] a = A.toCharArray();
        ArrayList<Integer> ans = new ArrayList<>();
        TreeSet<Integer> set = new TreeSet<>();
        int idx = 1;
        for (char c : a) {
            if (c == '1') {
                set.add(idx);
            }
            idx++;
        }
        for (int i = 0; i < B.size(); i++) {
            int q = B.get(i).get(0);
            if (q == 1) {
                int v = B.get(i).get(1);
                if (set.contains(v)) {
                    set.remove(v);
                } else set.add(v);
            } else {
                int v = B.get(i).get(1);
                int left = Integer.MAX_VALUE;
                int right = Integer.MIN_VALUE;
                if (set.floor(v) != null) {
                    left = set.floor(v);
                }
                if (set.ceiling(v) != null) {
                    right = set.ceiling(v);
                }
                if (left == Integer.MAX_VALUE && right == Integer.MIN_VALUE) {
                    ans.add(-1);
                    continue;
                }
                if (left != Integer.MAX_VALUE && (Math.abs(v - left) <= Math.abs(right - v))) {
                    ans.add(left);
                } else {
                    ans.add(right);
                }

            }
        }
        return ans;
    }
}
