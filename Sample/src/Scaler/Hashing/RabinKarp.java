package Scaler.Hashing;

public class RabinKarp {
    public static void main(String[] args) {
//        new RabinKarp().fastPower(2,10);

        new RabinKarp().solve("abcabcabccab", "abc");
    }

    public int solve(String A, String B) {
        int count = 0;
        char[] bChar = B.toCharArray();
        char[] aChar = A.toCharArray();
        int m = bChar.length-1;
        int i = 0;
        int j = bChar.length-1;
        long bHash = hash(B);
        long aHash = hash(new String(aChar, i, j+1));
        if (aHash == bHash) count+=1;
        i++;
        j++;
        while(j<aChar.length){
            aHash = ((aHash - (aChar[i-1]*fastPower(31,m)))*31)+aChar[j];
            if(aHash==bHash) count+=1;
            j++;
            i++;
        }
        return count;
    }

    private long fastPower(int a, int n) {
        if(n == 0) return 1;
        long p = fastPower(a,n/2);
        long ans = 0;
        if(n % 2 == 0){
            ans = p*p;
        }else{
            ans = a*p*p;
        }
        return ans;
    }

    long hash(String a) {
        long ans = 0;
        char[] chars = a.toCharArray();
        int power = 1;
        for (int i = chars.length - 1; i >= 0; i--) {
            ans += (long) (chars[i]) * power;
            power *= 31;
        }
        return ans;
    }
}
