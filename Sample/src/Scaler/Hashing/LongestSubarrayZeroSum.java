package Scaler.Hashing;

import java.util.HashMap;

public class LongestSubarrayZeroSum {
    public static void main(String[] args) {
        new LongestSubarrayZeroSum().solve(new int[]{-16,16,3});
    }
    public int solve(int[] A) {
        int n = A.length;
        Long[] psum = new Long[A.length];
        psum[0] = (long) A[0];
        int distance = Integer.MIN_VALUE;
        for(int i = 1; i < n; i++){
            psum[i] =A[i] + psum[i-1];
        }
        HashMap<Long,Integer> firstOcc = new HashMap<>();
        for(int i = 0 ; i<n; i++){
            if(!firstOcc.containsKey(psum[i])){
                firstOcc.put(psum[i],i);
            }
        }
        for(int i = n-1; i>=0;i--){
            if(psum[i] == 0){
                distance = Math.max(distance,i+1);
            }else{
                distance = Math.max(distance,i-firstOcc.get(psum[i]));
            }
        }
        return distance;
    }
}
