package Scaler.Hashing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class PermutationOfAinB {
    public static void main(String[] args) {
        new PermutationOfAinB().solve("abc", "abcbacabc");
    }

    public int solve(String A, String B) {
        int count = 0;
        int[] aOcc = new int[26];
        int[] bOcc = new int[26];
        char[] aChar = A.toCharArray();
        char[] bChar = B.toCharArray();
        int a = aChar.length;
        int b = bChar.length;
        if (a > b) return -1;
        int i = 0;
        int j = a;
        for (char c : aChar) {
            aOcc[c - 'a']++;
        }
        for (int k = 0; k < a; k++) {
            bOcc[bChar[k] - 'a']++;
        }
        while (j <= b) {
            if (check(aOcc, bOcc)) {
                count++;
            }
            bOcc[bChar[i]-'a']--;
            i++;
            if (j == b) break;
            bOcc[bChar[j] - 'a']++;
            j++;
        }
        return count;
    }

    public boolean check(int[] a, int[] b) {
        for (int i = 0; i < 26; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
}
