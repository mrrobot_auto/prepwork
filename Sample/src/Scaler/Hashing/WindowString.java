package Scaler.Hashing;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class WindowString {
    public static void main(String[] args) {
        new WindowString().minWindow("ADOBECODEBANC","ABC");
    }
    public String minWindow(String A, String B) {
        int ans = Integer.MAX_VALUE;
        HashMap<Character,Integer> aOcc = new HashMap<>();
        HashMap<Character,Integer> bOcc = new HashMap<>();
        char[] aChar = A.toCharArray();
        char[] bChar = B.toCharArray();
        int a = aChar.length;
        int b = bChar.length;
        int i = 0;
        int j = b;
        String res = "";
        for(char c : bChar){
            bOcc.put(c,bOcc.getOrDefault(c,0)+1);
        }
        IntStream.range(0, b).forEachOrdered(k -> aOcc.put(aChar[k], aOcc.getOrDefault(aChar[k], 0) + 1));
        while(j<=a){
            if(check(aOcc,bOcc)){
                if(j-i<ans){
                    ans = j-i;
                    res = new String(aChar,i,j-i);
                }
                var val = aOcc.get(aChar[i]);
                aOcc.put(aChar[i],val-1);
                i++;
            }else{
                if(j==a) break;
                aOcc.put(aChar[j],aOcc.getOrDefault(aChar[j],0)+1);
                j++;
            }
        }
        return res;
    }
    public boolean check(HashMap<Character,Integer> a, HashMap<Character,Integer> b){
        for(Map.Entry<Character,Integer> entry : b.entrySet()){
            if(a.containsKey(entry.getKey())){
                if(a.get(entry.getKey())<entry.getValue()){
                    return false;
                }
            }else return false;
        }
        return true;
    }
}
