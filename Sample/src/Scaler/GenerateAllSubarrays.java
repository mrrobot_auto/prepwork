package Scaler;

import java.util.ArrayList;
import java.util.List;

public class GenerateAllSubarrays {
    public static void main(String[] args) {
        solve(new ArrayList<>(List.of(36,63,63,26,87,28,77,93,7)));
    }
    public static ArrayList<ArrayList<Integer>> solve(ArrayList<Integer> A) {
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        for(int i=0; i<A.size();i++){
            for(int j=i+1; j<A.size();j++){
                ArrayList<Integer> list = new ArrayList<>();
                for(int k = i; k<j ;k++){
                    list.add(A.get(k));
                }
                res.add(list);
            }
        }
        return res;
    }
}
