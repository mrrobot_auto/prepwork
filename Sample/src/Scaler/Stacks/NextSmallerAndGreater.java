package Scaler.Stacks;

import java.util.Arrays;
import java.util.Stack;
import java.util.stream.IntStream;

public class NextSmallerAndGreater {
    public static void main(String[] args) {

        Arrays.stream(new NextSmallerAndGreater().prevSmaller(new int[]{4,7,3,8})).forEach(o -> System.out.print(o+", ".trim()));
        System.out.println();
        Arrays.stream(new NextSmallerAndGreater().nextSmaller(new int[]{4,7,3,8})).forEach(o -> System.out.print(o+", ".trim()));
        System.out.println();
        Arrays.stream(new NextSmallerAndGreater().nextGreater(new int[]{4,7,3,8})).forEach(o -> System.out.print(o+", ".trim()));
        System.out.println();
        Arrays.stream(new NextSmallerAndGreater().prevGreater(new int[]{4,7,3,8})).forEach(o -> System.out.print(o+", ".trim()));
    }

    public int[] prevSmaller(int[] A) { //similar to NSL
        int len = A.length;
        int[] res = new int[len];
        Stack<Integer> s = new Stack<>();
        for(int i = 0; i < len; i++) {
            while((!s.isEmpty()) && A[s.peek()] >= A[i]) {
                s.pop();
            }
            if(!s.isEmpty()) {
                res[i] = s.peek();
            }
            else {
                res[i] = -1;
            }
            s.push(i);
        }
        return res;
    }

    public int[] nextSmaller(int[] A) { // similar to NSR
        int n = A.length;
        int[] res = new int[n];

        Stack<Integer> s = new Stack<>();

        for(int i = n - 1; i >= 0; i--) {
            while((!s.isEmpty()) && A[s.peek()] > A[i]) {
                s.pop();
            }

            if(!s.isEmpty()) {
                res[i] = s.peek();
            }
            else {
                res[i] = n;
            }

            s.push(i);
        }

        return res;
    }

    public int[] nextGreater(int[] A) { // similar to NGR
        int n = A.length;
        int[] res = new int[n];

        Stack<Integer> s = new Stack<>();

        for(int i = n - 1; i >= 0; i--) {
            while((!s.isEmpty()) && A[s.peek()] < A[i]) {
                s.pop();
            }

            if(!s.isEmpty()) {
                res[i] = s.peek();
            }
            else {
                res[i] = n;
            }

            s.push(i);
        }

        return res;
    }
    public int[] prevGreater(int[] A) { //similar to NGL
        int n = A.length;
        int[] res = new int[n];
        Stack<Integer> s = new Stack<>();

        for(int i = 0; i < n; i++) {
            while((!s.isEmpty()) && A[s.peek()] <= A[i]) {
                s.pop();
            }
            if(!s.isEmpty()) {
                res[i] = s.peek();
            }
            else {
                res[i] = -1;
            }
            s.push(i);
        }
        return res;
    }
}
