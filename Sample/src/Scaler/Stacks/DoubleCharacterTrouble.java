package Scaler.Stacks;

import java.util.Stack;

public class DoubleCharacterTrouble {
    public static void main(String[] args) {
        new DoubleCharacterTrouble().dct("abccbc");
    }
    public String dct(String A) {
        char[] chars = A.toCharArray();
        Stack<Character>  stack  = new Stack<>();
        for(char c : chars){
            if(stack.empty()){
                stack.push(c);
            }else{
                if(stack.peek() == c){
                    stack.pop();
                }else{
                    stack.push(c);
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        for(char c : stack){
            sb.append(c);
        }
        return sb.toString();
    }
}
