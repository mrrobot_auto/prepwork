package Scaler.Stacks;

import java.util.Stack;

public class PostfixExpression {
    //Also known as Reverse Polish Notation
    public static void main(String[] args) {

    }
    public int evalRPN(String[] A) {
        int x = 0;
        int y = 0;
        Stack<Integer> stack = new Stack<>();
        for(String s : A){
            switch (s) {
                case "+" -> {
                    y = stack.pop();
                    x = stack.pop();
                    stack.push(x + y);
                }
                case "-" -> {
                    y = stack.pop();
                    x = stack.pop();
                    stack.push(x - y);
                }
                case "*" -> {
                    y = stack.pop();
                    x = stack.pop();
                    stack.push(x * y);
                }
                case "/" -> {
                    y = stack.pop();
                    x = stack.pop();
                    stack.push(x / y);
                }
                default -> {
                    int t = Integer.parseInt(s);
                    stack.push(t);
                }
            }
        }
        return stack.pop();
    }
}
