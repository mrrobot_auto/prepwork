package Scaler;

public class AthMagicalNumber {
    public static void main(String[] args) {
        new AthMagicalNumber().solve(807414236,3788,38141);
        //426344489,36067,29025
    }
    public int solve(int A, int B, int C) {
        int mod = 1000000007;
        long ans = 1;
        long l = 0;
        long r = (long) Math.max(B, C) *A;
        while(l<=r){
            long mid = l+(r-l)/2;
            long rank = getRank(mid,B,C);
            if(rank==A){
                ans = mid;
                r=mid-1;
            }else if(rank>A){
                r = mid-1;
            }else{
                l = mid+1;
            }
        }
        ans%=mod;
        return (int)ans;
    }
    public long getRank(long num,int B,int C){
        int prod = (B*C);
        int gcd = gcd(B,C);
        int lcm = prod/gcd;
        return (num/B)+(num/C)-(num/lcm);
    }
    public int gcd(int A, int B){
        if(B==0) return A;
        return gcd(B,A%B);
    }
}
