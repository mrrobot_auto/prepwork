package Scaler;

public class SumOfSubarray_Sliding_Window {
    public static void main(String[] args) {

    }

    public static int sumOfSubArray(int[] A, int B, int C) {
        int N = A.length;

        int startIndex = 0;
        int endIndex = B;
        int sum = 0;
        while(startIndex<endIndex){
            sum+=A[startIndex];
            startIndex++;
        }
        if(sum == C){
            return 1;
        }
        startIndex = 1;
        endIndex++;
        while(endIndex<N){
            sum -= A[startIndex];
            sum += A[endIndex];
            if(sum == C){
                return 1;
            }
            startIndex++;
            endIndex++;
        }
        return 0;
    }
}
