package Scaler.TwoPointers;

public class MaxArea {
    public static void main(String[] args) {

    }
    public int maxArea(int[] A) {
        int n = A.length;
        int i = 0;
        int j = n-1;
        int res = 0;
        while(i<j){
            int water = Math.min(A[i],A[j]) * (j-i);
            res = Math.max(res,water);
            if(A[i]<A[j]){
                i++;
            }else{
                j--;
            }
        }
        return res;
    }
}
