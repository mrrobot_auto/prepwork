package Scaler.TwoPointers;

public class TwoSum {
    public static void main(String[] args) {
        new TwoSum().solve(new int[]{2,2,3,4,4,5,6,7,10},8);
    }
    public int solve(int[] A, int B) {
        int i = 0;
        int n = A.length;
        int mod = 1000000007;
        int j = n-1;
        long ans = 0L;
        while(i<j){
            int sum = A[i]+A[j];
            if(sum == B){
                if(A[i]==A[j] && A[i]+A[j]==B)
                {
                    long m=(j-i);
                    long o=(j-i+1);
                    ans+=(m*o)/2;
                    break;
                }
                else {
                    int k = 1;
                    while (A[i] == A[i + 1]) {
                        k++;
                        i++;
                    }
                    if (i >= j) break;
                    int l = 1;
                    while (A[j] == A[j - 1]) {
                        l++;
                        j--;
                    }
                    ans += ((long) k * l);
                    i++;
                    j--;
                }
            }else if(sum<B){
                i++;
            }else{
                j--;
            }
        }
        return (int)(ans%mod);
    }
}
