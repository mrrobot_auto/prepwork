package Scaler.TwoPointers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SubarrayWithGivenSum {
    public static void main(String[] args) {
        //new SubarrayWithGivenSum().solve(List.of(1,1000000000),1000000000);
    }
    public ArrayList<Integer> solve(ArrayList<Integer> A, int B) {
        int i = 0;
        int j = 0;
        int n = A.size();
        int sum = 0;
        if(A.stream().anyMatch(l -> l==B)) return (ArrayList<Integer>) A.stream().filter(o -> o==B).collect(Collectors.toList());
        ArrayList<Integer> res = new ArrayList<>();
        while(j < n){
            if(sum==B){
                for(int k = i; k < j; k++){
                    res.add(A.get(k));
                }
                break;
            }else if(sum>B){
                sum -= A.get(i);
                i++;
            }else{
                sum += A.get(j);
                j++;
            }
        }
        if(res.size()==0){
            res.add(-1);
        }
        return res;
    }
}
