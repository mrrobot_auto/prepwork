package Scaler;

public class SumOfDigitsRecursion {
    public static void main(String[] args) {
        solve(2354);
    }
    public static int solve(int A) {
        if(A <= 0) return 0;
        return A%10 + solve(A/10);
    }
}
