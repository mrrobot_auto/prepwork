package Scaler;

public class ncrcombinometrics {
    public static void main(String[] args) {
        new ncrcombinometrics().solve(5,2,11);
    }
    public int solve(int A, int B, int C) {
        int n = factorial(A,C);
        int r = factorial(A-B,C);
        return n/(B*r);
    }
    public int factorial(int N,int M){
        if(N < 1) return 1;
        return (((factorial(N - 1, M) % M) * N) % M) % M;
    }
}
