package Scaler;

import java.util.LinkedList;

public class AddOne {
    public static void main(String[] args) {
        new AddOne().plusOne(new int[]{9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9});
    }
    public int[] plusOne(int[] A) {
        long num = convert(A);
        num += 1;
        LinkedList<Integer> list = new LinkedList<>();
        while(num>0){
            int rem = (int) (num%10);
            num /= 10;
            list.add(rem);
        }
        int[] res = new int[list.size()];
        int index = res.length-1;
        for(int i : list){
            res[index] = i;
            index--;
        }
        return res;
    }
    long convert(int[] A){
        long power = 1;
        long res = 0;
        for(int i = A.length-1 ; i>=0; i--){
            res += (power*A[i]);
            power *= 10;
        }
        return res;
    }
}
