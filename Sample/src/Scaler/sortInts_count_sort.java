package Scaler;

public class sortInts_count_sort {

    public static void main(String[] args) {
        countSortInts(new int[]{1,1,1,1,1,1});
    }
    static int[] countSortInts(int[] A){
        int[] res = new int[100001];
        int[] ans = new int[A.length];
        for(int i : A){
            res[i] += 1;
        }
        int index = 0;
        for(int i = 0; i<res.length; i++){
            if(index>(A.length-1)) break;
            if(res[i]>0){
                for(int j=0; j<res[i]; j++){
                    ans[index] = i;
                    index++;
                }
            }
        }
        return ans;
    }
}
