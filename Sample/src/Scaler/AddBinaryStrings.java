package Scaler;

import java.math.BigInteger;

public class AddBinaryStrings {
    public static void main(String[] args) {
        addBinary("10001100010111000101100010100110001001101010000010011010", "101111000100100100111110010010101110101001100100101001111010011000000110");
    }

    public static String addBinary(String A, String B) {
        char[] As = A.toCharArray();
        char[] Bs = B.toCharArray();
        return dtob(btod(As).add(btod(Bs)));
    }


    static BigInteger btod(char[] chars) {
        BigInteger ans = new BigInteger("0");
        BigInteger power = new BigInteger("1"); //2^0
        for (int i = chars.length - 1; i >= 0; i--) {
            int t = chars[i] - '0';
            ans = ans.add(power.multiply(BigInteger.valueOf(t)));
            power = power.multiply(BigInteger.valueOf(2));
        }
        return ans;
    }

    static String dtob(BigInteger n) {
        String ans = "";
        while (n.compareTo(BigInteger.valueOf(0)) > 0) {
            BigInteger r = n.mod(BigInteger.valueOf(2));
            n = n.divide(BigInteger.valueOf(2));
            ans = r+ans;
        }
        return ans;
    }
}
