package Scaler.LabSession10;

public class MaxXor {
    public class Solution {

        class Node{
            int val;
            Node[] children;

            public Node(int v){
                this.val = v;
                children = new Node[2];
            }
        }

        public void insert(Node root, int val, int x){
            Node curr = root;
            for(int i = x - 1; i >= 0; i--){
                if((val & (1<<i)) != 0){ //bit set
                    if(curr.children[1] != null){
                        curr = curr.children[1];
                    }else{
                        Node n = new Node(1);
                        curr.children[1] = n;
                        curr = curr.children[1];
                    }
                }else{ //bit unset
                    if(curr.children[0] != null){
                        curr = curr.children[0];
                    }else{
                        Node n = new Node(0);
                        curr.children[0] = n;
                        curr = curr.children[0];
                    }
                }
            }
        }

        public int solve(int[] arr) {
            Node root = new Node(-1);
            int max = arr[0];
            for(int i = 1; i < arr.length; i++){
                max = Math.max(max, arr[i]);
            }

            int x = 0;
            while(max > 0){
                max = (max >> 1);
                x++;
            }

            insert(root, arr[0], x);
            int ans = Integer.MIN_VALUE;
            for(int i = 1; i < arr.length; i++){
                int xor = 0;
                Node curr = root;
                for(int j = x - 1; j >= 0; j--){
                    if((arr[i] & (1<<j)) != 0){
                        if(curr.children[0] != null){
                            xor = (xor | (1<<j));
                            curr = curr.children[0];
                        }else{
                            curr = curr.children[1];
                        }
                    }else{
                        if(curr.children[1] != null){
                            xor = (xor | (1<<j));
                            curr = curr.children[1];
                        }else{
                            curr = curr.children[0];
                        }
                    }
                }
                ans = Math.max(xor, ans);
                insert(root, arr[i], x);
            }
            return ans;
        }
    }

}
