package Scaler.LabSession10;

public class MaxXorSubarray {
    public class Solution {
        public int[] solve(int[] arr) {
            Node root = new Node(-1);
            int max = arr[0];
            int start = 0, end = 0;
            for(int i = 1; i < arr.length; i++){
                if(arr[i] > max){
                    max = arr[i];
                    start = i;
                    end = i;
                }
            }
            int ans = max;

            max = arr[0];
            for(int i = 1; i < arr.length; i++){
                arr[i] = (arr[i] ^ arr[i - 1]);
                max = Math.max(max, arr[i]);
                if(arr[i] > ans){
                    ans = arr[i];
                    start = 0;
                    end = i;
                }
            }

            int x = 0;
            while(max > 0){
                max = (max >> 1);
                x++;
            }

            insert(root, arr[0], x, 0);
            for(int i = 1; i < arr.length; i++){
                int xor = 0;
                Node curr = root;
                for(int j = x - 1; j >= 0; j--){
                    if((arr[i] & (1<<j)) != 0){
                        if(curr.children[0] != null){
                            xor = (xor | (1<<j));
                            curr = curr.children[0];
                        }else{
                            curr = curr.children[1];
                        }
                    }else{
                        if(curr.children[1] != null){
                            xor = (xor | (1<<j));
                            curr = curr.children[1];
                        }else{
                            curr = curr.children[0];
                        }
                    }
                }
                if(xor > ans){
                    ans = xor;
                    start = curr.idx + 1;
                    end = i;
                }else if(ans == xor && end - start + 1 > i - curr.idx){
                    start = curr.idx + 1;
                    end = i;
                }
                insert(root, arr[i], x, i);
            }

            int[] res = new int[2];
            res[0] = start + 1;
            res[1] = end + 1;
            return res;

        }

        class Node{
            int val;
            int idx;
            Node[] children;

            public Node(int v){
                this.val = v;
                children = new Node[2];
            }
        }

        public void insert(Node root, int val, int x, int idx){
            Node curr = root;
            for(int i = x - 1; i >= 0; i--){
                if((val & (1<<i)) != 0){ //bit set
                    if(curr.children[1] != null){
                        curr = curr.children[1];
                    }else{
                        Node n = new Node(1);
                        curr.children[1] = n;
                        curr = curr.children[1];
                    }
                }else{ //bit unset
                    if(curr.children[0] != null){
                        curr = curr.children[0];
                    }else{
                        Node n = new Node(0);
                        curr.children[0] = n;
                        curr = curr.children[0];
                    }
                }
            }
            curr.idx = idx;
        }

    }

}
