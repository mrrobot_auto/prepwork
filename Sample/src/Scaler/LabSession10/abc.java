package Scaler.LabSession10;

import java.util.ArrayList;

public class abc {
    public class Solution {
        public ArrayList<ArrayList<String>> partition(String a) {
            ArrayList<ArrayList<String>> ans = new ArrayList<>();
            helper(a, new ArrayList<String>(), ans);
            return ans;
        }

        public void helper(String str, ArrayList<String> a, ArrayList<ArrayList<String>> ans){
            if(str.length() == 0){
                ArrayList<String> one = new ArrayList<>();
                for(int i = 0 ; i < a.size(); i++){
                    one.add(a.get(i));
                }
                ans.add(one);
                return;
            }

            for(int i = 0 ; i < str.length(); i++){
                if(isPalindrome(str, 0, i) == true){
                    String ros = str.substring(i + 1); // rest of the string from idx i+1 to last index
                    a.add(str.substring(0, i + 1)); // [0, i + 1)
                    helper(ros, a, ans);
                    a.remove(a.size() - 1);
                }
            }
        }

        public boolean isPalindrome(String str, int si, int ei){
            while(si < ei){
                if(str.charAt(si) != str.charAt(ei)){
                    return false;
                }
                si++;
                ei--;
            }
            return true;
        }
    }

}
