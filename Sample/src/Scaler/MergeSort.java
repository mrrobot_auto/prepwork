package Scaler;

public class MergeSort {
    public static void main(String[] args) {
        int[] a = new int[]{45, 10, 15, 25, 50};
        new MergeSort().mergeSort(a, 0, a.length-1);
    }

    public void mergeSort(int[] A, int l, int r) {
        if (l == r) return;
        int mid = (l + r) / 2;
        mergeSort(A, l, mid);
        mergeSort(A, mid + 1, r);
        merge(A, l, mid + 1, r);
    }

    private void merge(int[] a, int l, int y, int r) {
        int i = l;
        int j = y;
        int k = 0;
        int[] c = new int[r - l + 1];
        while (i < y && j <= r) {
            if (a[i] <= a[j]) {
                c[k] = a[i];
                i++;
            } else {
                c[k] = a[j];
                j++;
            }
            k++;
        }
        while (i < y) {
            c[k] = a[i];
            i++;
            k++;
        }
        while (j <= r) {
            c[k] = a[j];
            j++;
            k++;
        }
        k = 0;
        for (int m = l; m <= r; m++) {
            a[m] = c[k];
            k++;
        }
    }
}
