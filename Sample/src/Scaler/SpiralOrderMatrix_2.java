package Scaler;

public class SpiralOrderMatrix_2 {
    public static void main(String[] args) {
        new SpiralOrderMatrix_2().generateMatrix(5);
    }

    public int[][] generateMatrix(int A) {
        int[][] res = new int[A][A];
        int N = A - 1;
        int num = 1;
        int k = 0, i = k, j = k;
        while (N >= 1) {
            while (j <= N) {
                res[i][j] = num;
                num++;
                j++;
            }
            j = N;
            i++;
            while (i <= N) {
                res[i][j] = num;
                num++;
                i++;
            }
            i = N;
            j = N-1;
            while (j >= k) {
                res[i][j] = num;
                num++;
                j--;
            }
            i = N-1;
            j = k;
            while (i >= k) {
                if(i!=j) {
                    res[i][j] = num;
                    num++;
                }
                i--;
            }
            k++;
            i=k;
            j=k;
            N--;
        }
        return res;
    }
}
