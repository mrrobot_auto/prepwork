package Scaler;

import java.util.Arrays;

public class NumberOfOpenDoors {
    public static void main(String[] args) {
        new NumberOfOpenDoors().solve(36);
    }
    public int solve(int A) {
        A += 1;
        boolean[] doors = new boolean[A];
        Arrays.fill(doors,true);
        int i = 2;
        int count = 0;
        while(i < A){
            for(int j = i; j < A; j+=i){
                doors[j] = !doors[j];
            }
            i++;
        }
        for(int k = 1; k < A; k++){
            if(doors[k]){
                count++;
            }
        }
        return count;
    }
}
