package Scaler;

public class PickFromBothSides {
    public static void main(String[] args) {
        piAnInt(new int[]{-533, -666, -500, 169, 724, 478, 358, -38, -536, 705, -855, 281, -173, 961, -509, -5, 942, -173, 436, -609, -396, 902, -847, -708, -618, 421, -284, 718, 895, 447, 726, -229, 538, 869, 912, 667, -701, 35, 894, -297, 811, 322, -667, 673, -336, 141, 711, -747, -132, 547, 644, -338, -243, -963, -141, -277, 741, 529, -222, -684, 35},48);
    }
    public static int piAnInt(int[] A, int B) {
        // B = B%A.length;
        int[] prefix = new int[A.length];
        prefix[A.length-1] = A[A.length-1];
        int[] pudhun = new int[A.length];
        pudhun[0] = A[0];
        for(int i=1; i<A.length; i++){
            prefix[(A.length-1)-i] = prefix[(A.length)-i]+A[(A.length-1)-i];
            pudhun[i] = pudhun[i-1] + A[i];
        }
        int max = -10000;
        int min = 10000;
        int pSum = 0;
        int lSum = 0;
        for(int k=0; k<B ; k++){
            pSum += A[k];
            lSum += A[(A.length-1)-k];
            max = Math.max(max,pSum);
            min = Math.min(min,lSum);
        }
        return max-min;
    }
}
