package Scaler;

import java.util.ArrayList;
import java.util.List;

public class AntiDiagonals {
    public static void main(String[] args) {
        diagonal(new ArrayList<>(List.of(
                new ArrayList<>(List.of(1, 2, 3)),
                new ArrayList<>(List.of(4, 5, 6)),
                new ArrayList<>(List.of(7, 8, 9))
        )));
    }

    public static ArrayList<ArrayList<Integer>> diagonal(ArrayList<ArrayList<Integer>> A) {
        int N = A.size();
        int[][] re = new int[(2*N)-1][N];
        ArrayList<ArrayList<Integer>> res = new ArrayList<>(2*(N-1));
        res.stream().forEach(o -> o.add(0));
        int startIndex = 0;
        int endIndex = N - 1;
        int index = 0;
        int refIndex = 0;
        while (startIndex < N) {
            int i = startIndex;
            int j = 0;
            ArrayList<Integer> list = null;
            while (i < N) {
                if (refIndex != 0) {
                    re[index][refIndex] = A.get(i).get(j);
                    ArrayList<Integer> tempList = res.get(index);
                    tempList.add(A.get(i).get(j));
                } else {
                    re[index][refIndex] = A.get(i).get(j);
                    list = new ArrayList<>();
                    list.add(A.get(i).get(j));
                    res.add(index, list);
                }
                if (j != endIndex) {
                    j++;
                } else {
                    i++;
                }
                index++;
            }
            startIndex++;
            endIndex--;
            refIndex++;
            index = refIndex;
        }
        return res;
    }
}
