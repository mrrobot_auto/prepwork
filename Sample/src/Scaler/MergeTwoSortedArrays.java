package Scaler;

public class MergeTwoSortedArrays {
    public static void main(String[] args) {
        new MergeTwoSortedArrays().solve(new int[]{-4,3},new int[]{-2,-2});
    }
    public int[] solve(final int[] A, final int[] B) {
        int N = A.length;
        int M = B.length;
        int[] res = new int[N+M];
        int i = 0;
        int j = 0;
        int idx = 0;
        while(i<N && j<M){
            if(A[i]<=A[j]){
                res[idx] = A[i];
                i++;
            }else{
                res[idx] = B[j];
                j++;
            }
            idx++;
        }
        while(i<N){
            res[idx] = A[i];
            i++;
            idx++;
        }
        while(j<M){
            res[idx] = B[j];
            j++;
            idx++;
        }
        return res;
    }
}
