package Scaler;

public class MaxChunksToSort {
    public static void main(String[] args) {
        new MaxChunksToSort().solve(new int[]{1,2,3,4,0});
    }
    public int solve(int[] A) {
        int count = A.length;
        int n = A.length;
        for(int i = 1; i<n;i++){
            for(int j = i-1; j >=0; j--){
                if(A[j]>A[j+1]){
                    swap(A,j,j+1);
                }else{
                    count--;
                    break;
                }
            }
        }
        return count;
    }
    void swap(int[] A, int i, int j){
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
}
