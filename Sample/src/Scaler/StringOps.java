package Scaler;

import java.util.Arrays;

public class StringOps {
    public static void main(String[] args) {
        stringOps("lLdfRVCgbkND",new int[]{3,2,4,5,6});

    }

    public static String stringOps(String A,int[] s) {
        StringBuilder res = new StringBuilder();
        Arrays.sort(s);

        for (int i = 0; i < A.length(); i++) {
            if (A.charAt(i) < 'A' || A.charAt(i) > 'Z') {
                res.append(A.charAt(i));
            }
        }
        String ans = res.toString();
        for (int i = 0; i < ans.length(); i++){
            if (ans.charAt(i) == 'a' || ans.charAt(i) == 'e'
                    || ans.charAt(i) == 'i' || ans.charAt(i) == 'o' || ans.charAt(i) == 'u') {
                ans = ans.replace(ans.charAt(i),'#');
            }
        }
        return ans+ans;
    }
}
