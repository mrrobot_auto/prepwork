package Scaler;

public class minimumSwaps_Sliding_Window {
    public static void main(String[] args) {
        minimumSwaps_Sliding_Window.solved(
                new int[]{-750, 6890, 1986, 1066, 695, 45, 5734, 4795, -9199, -7268, 20, -2898, 15, 8918, -1243, 5294, 3575, -5819, 8767},
                45
        );
    }

    public static int solved(int[] A, int B) {
        int swaps = 0;
        int N = A.length;
        int windowSize = 0;
        for (int i : A) {
            if (i <= B) {
                windowSize++;
            }
        }
        if (windowSize == N) return 0;
        int index = 0;
        while (index < windowSize) {
            if (A[index] > B) {
                swaps++;
            }
            index++;
        }
        int ans = swaps;
        int preIndex = 0;
        index = 1;
        windowSize++;
        while (windowSize < N) {
            //removed index < B ? swaps++
            //added index > B ? swaps++
            //removed index >B? swaps--;
            //added index < B? swaps--;

            if (A[preIndex] <= B && A[windowSize - 1] >= B) {
                swaps++;
            } else if (A[preIndex] > B && A[windowSize - 1] <= B) {
                swaps--;
            }
            preIndex = index;
            index++;
            windowSize++;
            ans = Math.min(ans, swaps);
        }
        return ans;
    }

    public int solve(int[] A, int B) {
        int swaps = Integer.MAX_VALUE;
        int N = A.length;
        int windowSize = 0;
        for (int i : A) {
            if (i <= B) {
                windowSize++;
            }
        }
        if (windowSize == N) return 0;
        int index = 0;
        while (windowSize < N) {
            int localSwaps = 0;
            for (int i = index; i < windowSize; i++) {
                if (A[i] > B) {
                    localSwaps++;
                }
            }
            index++;
            windowSize++;
            swaps = Math.min(localSwaps, swaps);
        }
        return swaps;
    }

    public int solves(int[] A, int B) {
        // Get the window size
        int window = 0;
        for (int k : A) {
            if (k <= B) {
                window++;
            }
        }

        // Count numbers less than equal to B in first window
        int count = 0;
        for (int i = 0; i < window; i++) {
            if (A[i] <= B) {
                count++;
            }
        }
        // find the window with max numbers with less than equal to B
        int max = count;
        for (int i = 1, j = window; i < A.length - window + 1 && j < A.length; i++, j++) {
            if (A[i - 1] <= B && A[j] > B) {
                count--;
            } else if (A[i - 1] > B && A[j] <= B) {
                count++;
            }
            if (max < count) {
                max = count;
            }
        }
        return window - max;
    }
}
