package Scaler;

import java.util.Arrays;

public class PaintersProblem {
    public static void main(String[] args) {
        new PaintersProblem().paint(1, 1000000, new int[]{1000000,1000000});
    }

    public int paint(int painters, int timeToPaintOneUnit, int[] boards) {
        long l = 0;
        long r = (long) (Arrays.stream(boards).sum()) * timeToPaintOneUnit;
        long res = r;
        while (l <= r) {
            long mid = (l + r) / 2;
            boolean p = getMinPainters(timeToPaintOneUnit, mid, boards, painters);
            if (p) {
                res = mid;
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return (int)(res%10000003);
    }

    public boolean getMinPainters(int timeToPaintOneUnit, long totalTime, int[] boards, int totalPainters) {
        int painters = 1;
        long timeLeft = totalTime;
        for (int boardLength : boards) {
            if ((long) boardLength * timeToPaintOneUnit > totalTime) return false;
            if ((long) boardLength * timeToPaintOneUnit <= timeLeft) {
                timeLeft -= (long) boardLength * timeToPaintOneUnit;
            } else {
                painters++;
                timeLeft = totalTime - ((long) boardLength * timeToPaintOneUnit);
            }
        }
        return painters <= totalPainters;
    }
}
