package Scaler;

public class A_RaisedTo_B_Mod_M {
    public static void main(String[] args) {
        new A_RaisedTo_B_Mod_M().fastPower(2,3,10);
    }
    public int fastPower(int A,int B,int M){
        if(B == 0) return 1;
        int power = fastPower(A,B/2,M);
        long ans = 0;
        if(B%2==0){
            ans = ((long) power *power)%M;
        }else {
            ans = (((long) power *power)%M * A%M)%M;
        }
        return (int) ans;
    }
}
