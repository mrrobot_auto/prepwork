package Scaler;

public class powRecursive {
    public static void main(String[] args) {
        pow3(2,3,5);
    }
    public static int pow(int x, int n){
        if(n<1) return 1;
        return x*pow(x,n-1);
    }
    public static int pow1(int x, int n){
        if(n<1) return 1;
        if(n%2==0){
            return pow1(x,n/2)*pow1(x,n/2);
        }else
            return x*pow1(x, n/2)*pow1(x, n/2);
    }
    public static int pow3(int A, int B, int C) {
        // Just write your code below to complete the function. Required input is available to you as the function arguments.
        // Do not print the result or any output. Just return the result via this function.
        if(B<1) return 1;
        if(B%2==0){
            return (pow3(A,B/2,C))*(pow3(A,B/2,C))%C;
        }else{
            return ((A%C)*(pow3(A,B/2,C))*(pow3(A,B/2,C))%C)%C;
        }
    }
}
