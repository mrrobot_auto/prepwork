package Scaler;

public class XORSum {
    public static void main(String[] args) {
        new XORSum().solve(10, 11);
    }

    public int solve(int A, int B) {
        if (A == B) return 0;
        int sum = (A) + (B);
        for (int i = 1; i <= Math.max(A,B); i++) {
            int local = (A ^ i) + (B ^ i);
            sum = Math.min(sum, local);
        }
        return sum;
    }
}
