package Scaler;

public class MaxSubarray {
    public static void main(String[] args) {
        maxSubarray(4,11,new int[]{7,10,3,1});
    }
    public static int maxSubarray(int A, int B, int[] C) {
        int pt1 = 0;
        int pt2 = 0;
        int sum = 0;
        int res = 0;
        while(pt1<C.length && pt2<=pt1){
            sum += C[pt1];
            while(sum > B && pt2<=pt1){
                sum -= C[pt2];
                pt2++;
            }
            if(C[pt1] == B){
                return B;
            }
            if(sum == B){
                return B;
            }
            
            pt1++;
            res = Math.max(res,sum);
        }
        return res;
    }
}
