package Scaler;

import java.util.Arrays;

public class reverse_the_statement {
    public static void main(String[] args) {
        reverse(" bwroafq rfmy eimspekey w wnzjh qisjiabv ya hncn mazvb pfwlcsnkqz muiapt nnvwwx rp bsypbqu ymg bjwapykfil");
    }
    static String reverse(String A){
        String[] chars = A.split(" ");
        StringBuilder sb = new StringBuilder();
        
        for(int i=chars.length-1; i>=0 ;i--){
            if(!chars[i].equals("")){
                sb.append(chars[i]);
                if(i>0) sb.append(" ");
            }
        }
        return sb.toString();
    }
}
