package Scaler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparatorImpl {
    public static void main(String[] args) {
        var res = new ComparatorImpl().solve(new ArrayList<>(List.of(6, 8, 9)));
    }

    int getFactors(Integer integer) {
        int count = 2;
        for (int i = 2; i <= integer / 2; i++) {
            if (integer % i == 0) {
                if (integer / 2 == i) {
                    count += 1;
                } else {
                    count += 2;
                }
            }
        }
        return count;
    }

    public ArrayList<Integer> solve(ArrayList<Integer> A) {


        A.sort((a, b) -> getFactors(a) - getFactors(b)==0?
                a-b:
                getFactors(a) - getFactors(b));
        return A;
    }
}
