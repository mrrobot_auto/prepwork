package Scaler;

import java.util.Arrays;

public class RangeSumQuery {
    public static void main(String[] args) {
        rangeSumQuery(new int[]{1,2,3,4,5},new int[][]{{0,3},{1,2}});
    }
    public static int[] rangeSumQuery(int[] A, int[][] B) {
        int[] res = new int[B.length];
        for(int i = 1; i<A.length ; i++){
            A[i] = A[i]+A[i-1];
        }
        int index = 0;
        for (int k = 0; k < B.length; k++) {
            if(B[k][0] == 0){
                res[index] = A[B[k][1]];
            }else {
                res[index] = A[B[k][1]] - A[B[k][0] -1];
            }
            index++;
        }

        return res;
    }
}
