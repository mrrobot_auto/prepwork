package Scaler.Trie;

public class SpellChecker {
    public static void main(String[] args) {
        var sp = new SpellChecker();
        sp.insert("camel");
        sp.insert("cat");
        sp.search("cat");

    }

    TrieNode trieNode = new TrieNode('\0');

    void insert(String word) {
        TrieNode head = trieNode;
        char[] chars = word.toCharArray();
        for (char c : chars) {
            if (head.children[c - 'a'] == null) {
                head.children[c - 'a'] = new TrieNode(c);
            }
            head = head.children[c - 'a'];
        }
        head.isWord = true;
    }

    boolean search(String word) {
        TrieNode head = trieNode;
        char[] chars = word.toCharArray();
        for (char c : chars) {
            if (head.children[c - 'a'] == null) {
                return false;
            }
            head = head.children[c - 'a'];
        }
        return head.isWord;
    }

    public String getPrefix(String word){
        char[] chars = word.toCharArray();
        TrieNode head = trieNode;
        head = head.children[chars[0] -'a'];
        for(int i = 1 ; i < chars.length; i++){
            for(int j = 0; j < 26 ; j++){
                if(j == chars[i]-'a') continue;
                if(head.children[chars[i]-'a'] != null){
                    break;
                }

            }
        }
    }

    static class TrieNode {
        char c;
        TrieNode[] children;
        boolean isWord;

        TrieNode(char c) {
            this.c = c;
            children = new TrieNode[26];
            isWord = false;
        }

    }

}
