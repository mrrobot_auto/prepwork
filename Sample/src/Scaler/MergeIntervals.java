package Scaler;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MergeIntervals {
    public static void main(String[] args) {
        var mi = new MergeIntervals();
        var list = new ArrayList<>(List.of(
                mi.createInterval(11,20),
                mi.createInterval(15,18),
                mi.createInterval(19,26),
                mi.createInterval(25,40),
                mi.createInterval(1,5),
                mi.createInterval(6,8),
                mi.createInterval(9,11),
                mi.createInterval(45,54),
                mi.createInterval(56,70),
                mi.createInterval(60,65),
                mi.createInterval(62,68)
        ));
        mi.merge(list);
    }
    public Interval createInterval(int start,int end){
        return new Interval(start,end);
    }
    public ArrayList<Interval> insert(ArrayList<Interval> intervals, Interval newInterval) {
        ArrayList<Interval> res = new ArrayList<>();
        int end = newInterval.end;
        for(int i =0; i < intervals.size();i++){
            if(intervals.get(i).start<=newInterval.end && intervals.get(i).end>= newInterval.start){
                newInterval.start = Math.min(intervals.get(i).start,newInterval.start);
                end = Math.max(intervals.get(i).end,newInterval.end);
            }else if(newInterval.end<intervals.get(i).start){
                res.add(new Interval(newInterval.start,end));
                for(int j=i; j<intervals.size();j++){
                    res.add(intervals.get(j));
                }
                return res;
            }else{
                res.add(intervals.get(i));
            }
        }
        res.add(new Interval(newInterval.start,end));
        return res;
    }
    public ArrayList<Interval> merge(ArrayList<Interval> intervals) {
        intervals.sort(Comparator.comparingInt(a -> a.start));
        ArrayList<Interval> res= new ArrayList<>();
        int n = intervals.size();
        int s = intervals.get(0).start;
        int e = intervals.get(0).end;
        for(int i=1; i<n; i++){
            if(e>=intervals.get(i).start){
                s = Math.min(s,intervals.get(i).start);
                e = Math.max(e,intervals.get(i).end);
            }else{
                res.add(new Interval(s,e));
                s = intervals.get(i).start;
                e = intervals.get(i).end;
            }
        }
        res.add(new Interval(s,e));
        return res;
    }
}
