package Scaler;

public class InversionCount {
    public static void main(String[] args) {
        new InversionCount().solve(new int[]{3, 4, 1, 2});
    }

    public int solve(int[] A) {
        return inversion(A, 0, A.length - 1);
    }

    int inversion(int[] A, int l, int r) {
        if (l == r) return 0;
        int mid = (l + r) / 2;
        int left = inversion(A, l, mid);
        int right = inversion(A, mid + 1, r);
        int all = merge(A, l, mid + 1, r);
        return left + right + all;
    }

    int merge(int[] A, int l, int y, int r) {
        int i = l;
        int j = y;
        int k = 0;
        int[] c = new int[r - l + 1];
        int count = 0;
        while (i < y && j <= r) {
            if (A[i] <= A[j]) {
                c[k] = A[i];
                i++;
            } else {
                count += (y - i);
                c[k] = A[j];
                j++;
            }
            k++;
        }
        while (i < y) {
            c[k] = A[i];
            i++;
            k++;
        }
        while (j <= r) {
            c[k] = A[j];
            j++;
            k++;
        }
        k = 0;
        for (int m = l; m <= r; m++, k++) {
            A[m] = c[k];
        }
        return count;
    }
}
