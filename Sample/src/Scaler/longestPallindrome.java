package Scaler;

public class longestPallindrome {
    public static void main(String[] args) {
        longestCommonPrefix(new String[]{"a","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"});
        StringBuilder sb = new StringBuilder("ad".concat("dc"));

    }
    private static int getLen(char[] c, int si, int ei){
        while(ei<c.length && si>=0 && (c[si] == c[ei])){
            si--;
            ei++;
        }
        return ei-si-1;
    }
    public static String longestPalindrome(String A) {
        int ans = 0;
        int si = 0;
        int ei = 0;
        char[] chars = A.toCharArray();
        int N = chars.length;
        for(int i=0; i<N; i++){
            int temp = ans;
            ans = Math.max(getLen(chars,i,i),ans);
            if(temp != ans){
                si = i;
                ei = i;
            }
            temp = ans;
            ans = Math.max(getLen(chars,i,i+1),ans);
            if(temp != ans){
                si = i;
                ei = i+1;
            }
        }
        int jumps = si == ei ? (ans-1)/2 : (ans/2)-1;
        si-=jumps;
        ei+=jumps;
        return A.substring(si,ei+1);
    }
    public static String longestCommonPrefix(String[] A) {
        int index = -1;
        if(A.length == 1) return A[0];
        boolean gone = false;
        for(int i=0; i<A[0].length(); i++){
            char c  = A[0].charAt(i);
            for(int j = 1; j<A.length;j++){
                if(i>=A[j].length()){
                    gone = true;
                    break;
                }
                if(A[j].charAt(i)!=c){
                    gone = true;
                    break;
                }
            }
            if(!gone) index = i;
            if(gone)break;
        }
        return index == -1? "" : A[0].substring(0,index+1);
    }
}
