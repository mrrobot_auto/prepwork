package Scaler;

import leetcode.ListNode;

public class LinkedList_InsertInKthIndex {

    public static void main(String[] args) {
        new LinkedList_InsertInKthIndex().solve(
                new ListNode().addArrayInListNode(new int[]{6,3,3,6,7,8,7,3,7}),3,5
        );
    }
    public ListNode solve(ListNode A, int B, int C) {
        ListNode nn = new ListNode(B);
        if(C == 0){
            nn.next = A;
            return nn;
        }
        int count = 1;
        ListNode temp = A;
        while(temp.next != null){
            count++;
            temp = temp.next;
            if(count == C)
                break;
        }
        if(temp.next == null){
            temp.next = nn;
            return A;
        }
        ListNode rest = temp.next;
        temp.next = nn;
        nn.next = rest;
        return A;
    }
}
