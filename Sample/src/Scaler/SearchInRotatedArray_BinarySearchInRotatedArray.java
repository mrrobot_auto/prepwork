package Scaler;

public class SearchInRotatedArray_BinarySearchInRotatedArray {
    public static void main(String[] args) {
        int[] ins = new int[]{101, 103, 106, 109, 158, 164, 182, 187, 202, 205, 2, 3, 32, 57, 69, 74, 81, 99, 100};
        new SearchInRotatedArray_BinarySearchInRotatedArray().search(ins,202);
    }
    public int search(final int[] A, int B) {
        int n = A.length;
        int l = 0;
        int r = n-1;
        while(l<=r){
            int mid = l + (r-l)/2;
            if(A[mid]==B) return mid;
            if(B<A[0]){ //target in part2
                if(A[mid]>=A[0]){ //mid in part1
                    l=mid+1;
                }else{ // mid in part2
                    if(A[mid]>B){
                        r = mid-1;
                    }else{
                        l = mid+1;
                    }
                }
            }else{ //target in part1
                if(A[mid]>=A[0]){ //mid in part1
                    if(A[mid]>B){
                        r = mid-1;
                    }else{
                        l = mid+1;
                    }
                }else //mid in part2
                {
                    r = mid-1;
                }
            }
        }
        return -1;
    }
}
