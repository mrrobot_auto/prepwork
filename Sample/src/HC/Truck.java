package HC;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Truck {
    int id;
    String ownerName;
    int capacity;
    int rate;

    public Truck(int id, String ownerName, int capacity, int rate) {
        this.id = id;
        this.ownerName = ownerName;
        this.capacity = capacity;
        this.rate = rate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return this.getId()+"\n"+this.getOwnerName()+"\n"+this.getCapacity()+"\n"+this.getRate();
    }
}

class Solution1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfTrucks = scanner.nextInt();
        Truck[] trucks = new Truck[numberOfTrucks];
        for(int i = 0; i<numberOfTrucks ; i ++){
            int id = scanner.nextInt();
            String ownerName = scanner.next();
            int capacity = scanner.nextInt();
            int rate = scanner.nextInt();
            trucks[i] = new Truck(id,ownerName,capacity,rate);
        }
        int capacity = scanner.nextInt();
        int rate = scanner.nextInt();
        String owner = scanner.next();
        int countOfTrucks = getCountOfTruckByCapacityAndRate(trucks,capacity,rate);
        if(countOfTrucks == 0){
            System.out.println("No Truck found with mentioned attribute");
        }else {
            System.out.println(countOfTrucks);
        }
        Truck wantedTruck = findTruckWithSecondMinimumCapacityByOwner(trucks,owner);
        if(wantedTruck == null){
            System.out.println("No Truck found with mentioned attribute");
        }else {
            System.out.println(wantedTruck);
        }
    }

    public static int getCountOfTruckByCapacityAndRate(Truck[] trucks, int capacity, int rate) {
        int res = 0;
        for (Truck truck : trucks) {
            if (truck.getCapacity() >= capacity && truck.getRate() <= rate) {
                res++;
            }
        }
        return res;
    }

    public static Truck findTruckWithSecondMinimumCapacityByOwner(Truck[] trucks, String ownerName) {
        Truck result = null;
        List<Truck> truckList = new ArrayList<>();
        for (Truck truck : trucks) {
            if (truck.getOwnerName().equalsIgnoreCase(ownerName)) {
                truckList.add(truck);
            }
        }
        if(truckList.size()<=1) return null;
        truckList.sort(Comparator.comparingInt(Truck::getCapacity));
        if (truckList.size() > 1) {
            result = truckList.get(truckList.size() - 2);
        }
        return result;
    }
}