package DataStructure.BinaryTree;

public class BinarySearchTree {
    public static void main(String[] args) {
        var head = new Node<>(8);
        head.setLeftChild(new Node<>(7));
        head.setRightChild(new Node<>(14));

        insert(head, new Node<>(2));
        insert(head, new Node<>(5));
        insert(head, new Node<>(12));
        insert(head, new Node<>(18));
        insert(head, new Node<>(13));
        insert(head, new Node<>(16));
        insert(head, new Node<>(30));
        insert(head, new Node<>(100));
        insert(head, new Node<>(-103));
        insert(head, new Node<>(10000));
        var found = lookup(head, 0);
        System.out.println();
    }

    public static Node<Integer> insert(Node<Integer> head, Node<Integer> node) {
        if (head == null) return node;
        if (node.getData() <= head.getData()) {
            head.setLeftChild(insert(head.getLeftChild(), node));
        } else {
            head.setRightChild(insert(head.getRightChild(), node));
        }
        return head;
    }

    public static Node<Integer> lookup(Node<Integer> head, int value) {
        if (head == null) return null;
        if (head.getData() == value) return head;
        return value <= head.getData()
                ? lookup(head.getLeftChild(), value)
                : lookup(head.getRightChild(), value);
    }
    public static void printTree(Node<Integer> head,int value){
        if(head == null) System.out.println();
        if(head.getData()>=value){
            System.out.println("""
                        /
                       /
                      %d
                    """+value);
        }else {
            System.out.println("""
                        \\
                         \\
                          %d
                    """+value);
        }
    }
}
