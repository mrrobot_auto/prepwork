package DataStructure.BinaryTree;

public class DepthFirstTraversal {
    public static void main(String[] args) {
        BinaryTreeImplementation bt = new BinaryTreeImplementation();
        Node<Character> root = bt.getSampleBinaryTree();
        depthPreOrderTraversal(root);
        System.out.println();
        depthPInOrderTraversal(root);
        System.out.println();
        depthPostOrderTraversal(root);
        System.out.println();
        dfs(root, 'K');
    }

    public static void depthPreOrderTraversal(Node<Character> root) {
        if (root == null) return;
        System.out.print(root.getData() + " ");
        depthPreOrderTraversal(root.getLeftChild());
        depthPreOrderTraversal(root.getRightChild());
    }

    public static void depthPInOrderTraversal(Node<Character> root) {
        if (root == null) return;
        depthPInOrderTraversal(root.getLeftChild());
        System.out.print(root.getData() + " ");
        depthPInOrderTraversal(root.getRightChild());
    }

    public static void depthPostOrderTraversal(Node<Character> root) {
        if (root == null) return;
        depthPostOrderTraversal(root.getLeftChild());
        depthPostOrderTraversal(root.getRightChild());
        System.out.print(root.getData() + " ");
    }

    public static void dfs(Node<Character> root, char c) {
        if (root == null) return;
        System.out.print(root.getData() + " ");
        if (root.getData().equals(c)) {
            System.out.println("Found " + c);
            return;
        }
        dfs(root.getLeftChild(), c);
        dfs(root.getRightChild(), c);
    }
}
