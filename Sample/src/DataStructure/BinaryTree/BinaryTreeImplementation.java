package DataStructure.BinaryTree;


import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

public class BinaryTreeImplementation<T> {

    public static void main(String[] args) {
      /* Node<Integer> classicNode = new Node<>(300);
        for (int i = 10; i < 500; i+=10) {
            classicNode = addInTree(classicNode,i);
        }
        BreadthFirstTraversal.breadthFirstTraversal(classicNode);*/
        Node<Character> head = getSampleBinaryTree();
        mirrorTree(head);
        BreadthFirstTraversal.breadthFirstTraversal(head);
        System.out.println();
        BreadthFirstTraversal.breadthFirstTraversal(getSampleBinaryTree());
    }

    public static @NotNull Node<Character> getSampleBinaryTree() {
        Node<Character> node = new Node<>('A');
        node.setLeftChild(new Node<>('B'));
        node.setRightChild(new Node<>('C'));
        Node<Character> nodeC = node.getRightChild();
        nodeC.setLeftChild(new Node<>('D'));
        nodeC.setRightChild(new Node<>('E'));
        Node<Character> nodeD = nodeC.getLeftChild();
        nodeD.setLeftChild(new Node<>('F'));
        nodeD.setRightChild(new Node<>('G'));
        Node<Character> nodeE = nodeC.getRightChild();
        nodeE.setLeftChild(new Node<>('H'));
        nodeE.setRightChild(new Node<>('I'));
        Node<Character> nodeF = nodeD.getLeftChild();
        nodeF.setLeftChild(new Node<>('J'));
        nodeF.setRightChild(new Node<>('K'));
        Node<Character> nodeG = nodeD.getRightChild();
        nodeG.setLeftChild(new Node<>('L'));
        nodeG.setRightChild(new Node<>('M'));

        return node;
    }

    public static Node<Integer> addInTree(Node<Integer> node, int data) {
        if (node == null) return new Node<>(data);
        if (node.getData() <= data) {
            node.setRightChild(addInTree(node.getRightChild(), data));
        } else {
            node.setLeftChild(addInTree(node.getLeftChild(), data));
        }
        return node;
    }

    public static int depthOfBT(Node<?> head) {
        if (head == null)
            return 0;
        if (head.getRightChild() == null && head.getLeftChild() == null)
            return 0;

        int left = 1 + depthOfBT(head.getLeftChild());
        int right = 1 + depthOfBT(head.getRightChild());

        return Math.max(left, right);
    }

    public static void mirrorTree(Node<Character> head) {
        if (head == null) return;
        Node<Character> rightTemp = null;
        Node<Character> leftTemp = null;
        if (head.getLeftChild() != null) {
            leftTemp = head.getLeftChild();
        }
        if (head.getRightChild() != null) {
            rightTemp= head.getRightChild();
        }
        head.setLeftChild(rightTemp);
        head.setRightChild(leftTemp);
        mirrorTree(head.getLeftChild());
        mirrorTree(head.getRightChild());
    }

    public static void printRange(Node<?> head, int min, int max){
        if(head == null){

        }
    }
}
