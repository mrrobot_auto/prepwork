package DataStructure.BinaryTree;


import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstTraversal {
    public static void main(String[] args) {
        BinaryTreeImplementation bt = new BinaryTreeImplementation();
        breadthFirstTraversal(bt.getSampleBinaryTree());

    }

    public static void breadthFirstTraversal(Node<?> root) {
        Queue<Node<?>> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()) {
            Node<?> node = q.poll();
            System.out.print(node.getData() +", ");
            if (node.getLeftChild() != null) q.add(node.getLeftChild());
            if (node.getRightChild() != null) q.add(node.getRightChild());
        }
    }

}
