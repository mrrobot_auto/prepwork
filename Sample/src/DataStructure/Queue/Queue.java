package DataStructure.Queue;

public class Queue<T> {
    public static void main(String[] args) {
        Queue<String> q = new Queue<>();
        q.enqueue("sujay");
        q.enqueue("pooja");
        q.enqueue("MS");
        q.enqueue("AMEX");
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
    }
    Element<T> head = null;
    Element<T> tail = null;
    int size = 0;

    public void enqueue(T data) {
        if (size == 0) {
            head = new Element<>(data, null);
            tail = head;
        } else {
            tail.next = new Element<>(data, null);
            tail = tail.getNext();
        }
        size++;
    }

    public T dequeue() {
        if (size == 0) {
            System.out.println("Size is 0");
            return null;
        }
        T data = head.getData();
        size--;
        if (size != 0) {
            head = head.getNext();
        }else {
            head = null;
            tail = null;
        }
        return data;
    }


    public class Element<T> {
        T data;
        Element<T> next;

        public Element(T data, Element<T> next) {
            this.data = data;
            this.next = next;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Element<T> getNext() {
            return next;
        }

        public void setNext(Element<T> next) {
            this.next = next;
        }
    }
}
