package DataStructure.Queue;

import java.lang.reflect.Array;
import java.util.Arrays;

public class QueueArrayImpl<T> {

    private static final int SPECIAL_EMPTY_VALUE = -1;
    private static int MAX_SIZE = 40;
    private T[] elements;

    // The head index is initialized to a special value which
    // indicate that the queue is empty.
    private int headIndex = SPECIAL_EMPTY_VALUE;
    private int tailIndex = SPECIAL_EMPTY_VALUE;

    public static void main(String[] args) throws QueueOverflowException, QueueUnderflowException{
        MAX_SIZE = 4;
        QueueArrayImpl<Integer> queueArrayImpl = new QueueArrayImpl<>(Integer.class);

        System.out.println("Queue full?: " + queueArrayImpl.isFull());
        System.out.println("Queue empty?: " + queueArrayImpl.isEmpty());
        queueArrayImpl.print();
        queueArrayImpl.enqueue(1);
        queueArrayImpl.enqueue(2);
        queueArrayImpl.enqueue(3);
        queueArrayImpl.print();
        System.out.println("Queue full?: " + queueArrayImpl.isFull());
        System.out.println("Queue empty?: " + queueArrayImpl.isEmpty());

        queueArrayImpl.enqueue(4);
        queueArrayImpl.print();
        System.out.println("Queue full?: " + queueArrayImpl.isFull());
        System.out.println("Queue empty?: " + queueArrayImpl.isEmpty());

        System.out.println("Queue peek: " + queueArrayImpl.peek());

        int data = queueArrayImpl.dequeue();
        queueArrayImpl.enqueue(25);
        System.out.println("Dequeued element: " + data);
        queueArrayImpl.print();
        System.out.println("Peek : " + queueArrayImpl.peek());

        data = queueArrayImpl.dequeue();
        System.out.println("Dequeued element: " + data);
        System.out.println("Peek : " + queueArrayImpl.peek());
        queueArrayImpl.print();
        try {
            queueArrayImpl.enqueue(4);
            queueArrayImpl.enqueue(5);
            queueArrayImpl.enqueue(6);
        } catch (QueueOverflowException soe) {
            System.out.println("Queue is full! No room available.");
        }

        try {
            queueArrayImpl.dequeue();
            queueArrayImpl.dequeue();
            queueArrayImpl.dequeue();
            queueArrayImpl.dequeue();
            queueArrayImpl.dequeue();
            queueArrayImpl.dequeue();
        } catch (QueueUnderflowException sue) {
            System.out.println("Queue is empty! No elements available.");
        }

    }

    public QueueArrayImpl(Class<T> clazz) {
        this(clazz, MAX_SIZE);
    }

    public QueueArrayImpl(Class<T> clazz, int size) {
        elements = (T[]) Array.newInstance(clazz, MAX_SIZE);
    }

    public void enqueue(T data) throws QueueOverflowException {
        if (isFull()) {
            throw new QueueOverflowException();
        }
        tailIndex = (tailIndex + 1) % elements.length;
        elements[tailIndex] = data;

        // This is the first element enqueued, set the head index
        // to the tail index.
        if (headIndex == SPECIAL_EMPTY_VALUE) {
            headIndex = tailIndex;
        }
    }

    public boolean offer(T data) {
        if (isFull()) {
            return false;
        }
        try {
            enqueue(data);
        } catch (QueueOverflowException qoe) {
            // Ignore, this should never happen, we've checked
            // for a full queue already.
        }

        return true;
    }


    public T dequeue() throws QueueUnderflowException {
        if (isEmpty()) {
            throw new QueueUnderflowException();
        }

        T data = elements[headIndex];

        // This was the last element in the queue.
        if (headIndex == tailIndex) {
            headIndex = SPECIAL_EMPTY_VALUE;
        } else {
            headIndex = (headIndex + 1) % elements.length;
        }

        return data;
    }
    public void print(){
        System.out.println("----------------");
        Arrays.stream(elements).forEach(System.out::println);
        System.out.println("*****************");
    }
    public T peek() throws QueueUnderflowException {
        if (isEmpty()) {
            throw new QueueUnderflowException();
        }

        return elements[headIndex];
    }

    public boolean isEmpty()  {
        return headIndex == SPECIAL_EMPTY_VALUE;
    }

    public boolean isFull()  {
        int nextIndex = (tailIndex + 1) % elements.length;

        return nextIndex == headIndex;
    }

    public static class QueueOverflowException extends Exception {
    }

    public static class QueueUnderflowException extends Exception {
    }
}
