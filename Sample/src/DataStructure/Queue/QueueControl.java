package DataStructure.Queue;

import java.lang.reflect.Array;
import java.util.Arrays;

public class QueueControl<T> {
    T[] queue;
    private int head = 0;
    private int tail = 0;

    QueueControl(Class<T> cls, int size) {
        queue = (T[]) Array.newInstance(cls, size);
    }

    public void enqueue(T ele) {
        if (tail == 0) {
            queue[head] = ele;
        }
        queue[tail] = ele;
        tail++;
    }

    public T dequeue() {
        T res = queue[head];
        moveUp();
        tail --;
        return res;
    }
    public boolean isEmpty(){return tail == 0;}
    private void moveUp() {
        for (int i = 0; i < tail; i++) {
            queue[i] = queue[i + 1];
        }
    }

    public void printDesign() {
        System.out.println("------------------------");
        System.out.println("|                      |");
        System.out.println("|                      |");
        System.out.println("------------------------");
    }

    public void print() {
        Arrays.stream(queue).forEach(System.out::println);
    }

    public static void main(String[] args) {
        QueueControl<Integer> q = new QueueControl<>(Integer.class, 10);
        q.enqueue(10);
        q.enqueue(20);
        q.enqueue(5);
        //q.enqueue(52);
        //q.enqueue(23);
        q.print();
        q.printDesign();
        System.out.println("deq : " + q.dequeue());
        System.out.println("deq : " + q.dequeue());
        System.out.println("deq : " + q.dequeue());
        System.out.println("isEmpty : " + q.isEmpty());
        q.print();
    }
}
