package DataStructure.Stack;

import leetcode.LinkedListCycle;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class CallerOfStack {
    public static void main(String[] args) throws Exception{
        Stack<String> stringStack = new Stack<>();
        Deque<String> q = new LinkedList<>();
        System.out.println(stringStack.isEmpty());
        stringStack.push("Pooja");
        stringStack.push("Sujay");
        stringStack.push("AMEX");
        stringStack.push("MS");
        System.out.println(stringStack.isEmpty());
        System.out.println(stringStack.getSize());
        System.out.println(stringStack.pop());
        System.out.println(stringStack.getSize());
        System.out.println(stringStack.peek());
        System.out.println(stringStack.getSize());
    }
}
