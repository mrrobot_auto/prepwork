package DataStructure.Stack;

import java.time.Instant;
import java.util.Arrays;

public class StackImplWithArray {
    int size;
    int top = -1;
    int[] arr;

    public StackImplWithArray(int size) {
        System.out.println("Starting at : " + Instant.now());
        if (size <= 0) {
            System.out.println("Invalid size parameter");
            System.out.println("Terminating at : " + Instant.now());
            System.exit(-1);
        }
        this.size = size;
        this.arr = new int[size];
    }

    boolean isEmpty() {
        return size - 1 == top;
    }

    boolean isFull() {
        return size - 1 == top;
    }

    int peek() {
        if (!isEmpty()) return arr[this.top];
        else {
            System.out.println("Stack is empty.");
            return -1;
        }
    }

    void push(int a) {
        if (!isFull()) {
            top++;
            arr[top] = a;
            System.out.println("Pushed : " + a);
        } else {
            System.out.println("Stack is full.");
        }
    }

    int pop() {
        if (!isEmpty()) {
            int temp = peek();
            top--;
            System.out.println("popped : " + temp);
            return temp;
        } else {
            return -1;
        }
    }

    int[] printStack() {
        if (!isEmpty()) return this.arr;
        else return new int[1];
    }


    public static void main(String[] args) {
        StackImplWithArray stackImplWithArray = new StackImplWithArray(20);
        stackImplWithArray.push(5);
        stackImplWithArray.push(2);
        stackImplWithArray.push(6);
        stackImplWithArray.push(4);
        System.out.println("stack is full : " + stackImplWithArray.isFull());
        System.out.println("stack is empty : " + stackImplWithArray.isEmpty());
        System.out.println("peek : " + stackImplWithArray.peek());
        System.out.println("pop : " + stackImplWithArray.pop());
        System.out.println();
        Arrays.stream(stackImplWithArray.printStack()).filter(p -> p != 0).forEach(System.out::println);
        System.out.println();
        stackImplWithArray.push(14);
        stackImplWithArray.push(655);
        stackImplWithArray.pop();
        stackImplWithArray.push(546);
        System.out.println();
        Arrays.stream(stackImplWithArray.printStack()).filter(p -> p != 0).forEach(System.out::println);
        System.out.println();
        stackImplWithArray.push(54654);
        stackImplWithArray.pop();
        stackImplWithArray.push(25);
        stackImplWithArray.push(205);
        stackImplWithArray.push(2500);
        stackImplWithArray.push(20005);
        stackImplWithArray.push(2500000);
        stackImplWithArray.push(2000000005);
        stackImplWithArray.push(215);
        stackImplWithArray.push(251);
        stackImplWithArray.push(2115);
        System.out.println();
        Arrays.stream(stackImplWithArray.printStack()).filter(p -> p != 0).forEach(System.out::println);
        System.out.println();
        System.out.println("peek : " + stackImplWithArray.peek());
        System.out.println();
        Arrays.stream(stackImplWithArray.printStack()).filter(p -> p != 0).forEach(System.out::println);
        System.out.println();
    }
}
