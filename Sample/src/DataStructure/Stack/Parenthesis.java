package DataStructure.Stack;

import java.util.*;
import java.util.Stack;

public class Parenthesis {
    public static void main(String[] args) throws Exception {
        isValid("{sujay,(pooja)}");
    }

    static boolean isValid(String data) throws Exception {
        char[] chars = data.toCharArray();
        Map<Character, Character> map = new HashMap<>();
        map.put('{', '}');
        map.put('(', ')');
        map.put('[', ']');
        Deque<Character> stack = new LinkedList<>();
        Deque<Character> revStack = new LinkedList<>();
        for (char c : chars) {
            if (map.containsKey(c) || map.containsValue(c)) {
                stack.push(c);
            }
        }
        while (!stack.isEmpty()) {
            char s = stack.pop();
            if (!revStack.isEmpty()) {
                if (revStack.peek() == map.get(s)) {
                    revStack.pop();
                } else revStack.push(s);
            } else revStack.push(s);
        }
        return revStack.isEmpty();
    }
}
