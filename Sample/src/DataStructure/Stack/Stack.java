package DataStructure.Stack;

public class Stack<T> {
    private static final int MAX_SIZE = 400;

    public int getSize() {
        return size;
    }
    public boolean isEmpty(){
        return size==0;
    }
    public boolean isFull(){
        return size==MAX_SIZE;
    }
    private int size = 0;
    private Element<T> TOP = null;

    public void push(T data) throws Exception {
        if (this.isFull()) {
            throw new Exception("StackOverflowException");
        }
        TOP = new Element<>(data, TOP);
        size++;
    }

    public T pop() throws Exception {
        if (this.isEmpty()) {
            throw new Exception("StackUnderflowException");
        }
        T res = TOP.getData();
        TOP = TOP.getNext();
        size--;
        return res;
    }

    public T peek() throws Exception {
        if (this.isEmpty()) {
            throw new Exception("StackUnderflowException");
        }
        return TOP.getData();
    }
    public static class MinimumStack<T>{
        private Stack<T> stack = new Stack<>();
        private Stack<T> minimumStack = new Stack<>();
        public void push(T data){

        }

    }
}
