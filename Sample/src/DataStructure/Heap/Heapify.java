package DataStructure.Heap;

import java.util.Arrays;

public class Heapify {
    public static void main(String[] args) {
        int[] arr = {50};
        Arrays.stream(arr).forEach(o -> System.out.print(o + ","));
        for(int i = 10; i < 1000 ; i=i+10){
            arr = insertInHeap(arr, i, arr.length);
        }
        System.out.println();
        for (int i :
                arr) {
            System.out.print(i + " ,");
        }
        //System.out.println(getIndex(arr,20,0,1));
    }

    public static void dft(int[] root, int index) {

    }

    /**
     * <li>Parent = n/2</li>
     * <li>left = 2*i + 1</li>
     * <li>right = 2*i + 2</li>
     * @param arr  Heap Array
     * @param value  Integer value to add in heap
     * @param size  size of the Array
     * @return : Integer Array as Heap
     */
    public static int[] insertInHeap(int[] arr, int value, int size) {
        arr = Arrays.copyOf(arr, size + 1);
        arr[size] = value;
        int temp = arr[(size) / 2]; //size/2 returns parent
        while (temp < value) {
            swap(arr, size, size / 2);
            if (hasLeft(arr, size / 2) && hasRight(arr, size / 2) && arr[getLeft(size / 2)] > arr[getRight(size / 2)]) {
                swap(arr, getRight(size / 2), getLeft(size / 2));
            }
            size /= 2;
            if (size < 1) break;
            temp = arr[size / 2];
        }
        return arr;
    }

    public static int getIndex(int[] arr, int value, int L, int H) {
        if (value == arr[L]) return L;
        if (value == arr[H]) return H;
        if (hasRight(arr, H) && value > arr[getRight(H)]) {
            H = getRight(H);
        } else if (hasRight(arr, H) && value < arr[getRight(H)]) {
            H++;
        }
        if (hasLeft(arr, L) && value < arr[getLeft(L)]) {
            L = getLeft(L);
        } else if (hasLeft(arr, L) && value > arr[getLeft(L)]) {
            L++;
        }
        return getIndex(arr, value, L, H);
    }

    public static int getLeft(int index) {
        return (2 * index) + 1;
    }

    public static int getRight(int index) {
        return (2 * index) + 2;
    }

    public static boolean hasRight(int[] arr, int index) {
        try {
            int temp = arr[getRight(index)];
        } catch (ArrayIndexOutOfBoundsException aib) {
            return false;
        }
        return true;
    }

    public static boolean hasLeft(int[] arr, int index) {

        try {
            int temp = arr[getLeft(index)];
        } catch (ArrayIndexOutOfBoundsException aib) {
            return false;
        }
        return true;
    }

    public static void swap(int[] arr, int index1, int index2) {
        arr[index1] = arr[index1] ^ arr[index2];
        arr[index2] = arr[index1] ^ arr[index2];
        arr[index1] = arr[index1] ^ arr[index2];
    }
}
