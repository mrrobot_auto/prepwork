package DataStructure.Graph;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.IntStream;

public class GraphImp {
    private LinkedList<Integer>[] adj;

    public GraphImp(int v) {
        adj = new LinkedList[v];
        for (int i = 0; i < v; i++) {
            adj[i] = new LinkedList<>();
        }
    }

    public void addEdge(int source, int destination) {
        adj[source].add(destination);
        adj[destination].add(source);
    }

    public void print() {
        for (int i = 1; i < adj.length; i++) {
            while (!adj[i].isEmpty()) {
                System.out.print(adj[i].poll() + " --> ");
            }
        }
    }

    public void bfs(int source, int destination) {
        boolean[] visited = new boolean[adj.length];
        int parent[] = new int[adj.length];
        Queue<Integer> q = new LinkedList<>();
        q.add(source);
        parent[source] = -1;

        while (!q.isEmpty()) {
            int curr = q.poll();
            if (curr == destination) {
                return;
            }
            for (int n : adj[curr]) {
                if (!visited[n]) {
                    q.add(n);
                    parent[n] = curr;
                }
            }
        }
        int cure = destination;
        while (parent[cure] != -1) {
            System.out.println(cure + " --> ");
            cure = parent[cure];
        }


    }

    public static void main(String[] args) {
        Graph g = new AdjacencySetGraph(Graph.GraphType.UNDIRECTED,5);
        g.addEdge(0,1);
        g.getAdjacentVertices(0);
        g.addEdge(1,0);
        g.addEdge(0,2);
        g.addEdge(2,4);
        g.addEdge(1,3);
        g.addEdge(3,4);
        g.addEdge(1,2);
        g.addEdge(2,3);
        g.addEdge(1,4);
        int[] visited = new int[40];
        Arrays.fill(visited,0);
        IntStream.range(0,5).forEach(o -> DepthFirstTraversalInGraph.depthFirstTraversal(g,visited,o));

    }

}
