package DataStructure.Graph;


import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstTraversalInGraph {
    public static void breadthFirstTraversal(Graph graph, int[] visited, int currentVertex) {
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(currentVertex);
        while(!queue.isEmpty()){
            int vertex = queue.poll();
            if(visited[vertex] == 1){
                continue;
            }
            System.out.print(vertex + "-->");
            visited[vertex] = 1;
            graph.getAdjacentVertices(vertex).stream().filter(i -> visited[i]!=1).forEach(queue::offer);
        }
    }
}
