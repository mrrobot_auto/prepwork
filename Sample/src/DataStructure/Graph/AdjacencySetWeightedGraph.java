package DataStructure.Graph;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class AdjacencySetWeightedGraph implements WeightedGraph{
    List<List<Node>> adjacencyList = new ArrayList<>();

    public AdjacencySetWeightedGraph(WeightedGraph.GraphType type, int numberOfVertex){
        IntStream.rangeClosed(0,numberOfVertex).forEach(p -> adjacencyList.add(new ArrayList<>()));
    }

    @Override
    public void addEdge(int source, int destination, int weight) {
        this.adjacencyList.get(source).add(new Node(destination,weight));

    }

    @Override
    public List<Node> getAdjacentVertices(int v) {
       return this.adjacencyList.get(v).stream().toList();
    }
}
