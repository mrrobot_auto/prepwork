package DataStructure.Graph;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class AdjacencySetGraph implements Graph {
    private List<Node> vertexList = new ArrayList<>();
    private GraphType graphType;
    private int numVertices;

    public AdjacencySetGraph(GraphType graphType, int numVertices) {
        this.graphType = graphType;
        this.numVertices = numVertices;
        IntStream.range(0, numVertices).forEach(i -> vertexList.add(new Node(i)));
    }

    @Override
    public void addEdge(int v1, int v2) {
        vertexList.get(v1).addEdge(v2);
        if(graphType == GraphType.UNDIRECTED){
            vertexList.get(v2).addEdge(v1);
        }
    }

    @Override
    public List<Integer> getAdjacentVertices(int v) {
        return vertexList.get(v).getAdjacentVertices();
    }
}
