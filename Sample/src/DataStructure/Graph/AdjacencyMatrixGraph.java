package DataStructure.Graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AdjacencyMatrixGraph implements Graph {
    private final int[][] adjacencyMatrix;
    private GraphType graphType;
    private int numVertices;

    public AdjacencyMatrixGraph(GraphType graphType, int numVertices) {
        this.graphType = graphType;
        this.numVertices = numVertices;

        adjacencyMatrix = new int[numVertices][numVertices];
        for (int i = 0; i < numVertices; i++) {
            Arrays.fill(adjacencyMatrix[i], 0);
        }
    }

    @Override
    public void addEdge(int v1, int v2) {
        adjacencyMatrix[v1][v2] = 1;
        if (graphType == GraphType.UNDIRECTED) {
            adjacencyMatrix[v2][v1] = 1;
        }
    }

    @Override
    public List<Integer> getAdjacentVertices(int v) {
        List<Integer> res = new ArrayList<>();
        Arrays.stream(adjacencyMatrix[v]).filter(o -> o == 1).forEach(res::add);
        Collections.sort(res);
        return res;
    }
}
