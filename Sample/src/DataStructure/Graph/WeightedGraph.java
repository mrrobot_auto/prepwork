package DataStructure.Graph;

import java.util.List;

public interface WeightedGraph {
    /**
     * To indicate whether Graph represents an undirected or directed graph
     */
    enum GraphType{
        DIRECTED,
        UNDIRECTED
    }

    /**
     * An edge lies between two vertices - vertices are represented as two numbers
     * @param source : 1st vertex
     * @param destination : 2nd vertex
     * @param weight : weight value
     */
    void addEdge(int source, int destination, int weight);

    /**
     * Helper to get the adjacent vertices for any vertex.
     * This method is required for all algorithms involving Graphs
     * @param v : vertex
     * @return : list of adjacent vertices
     */
    List<Node> getAdjacentVertices(int v);

    class Node{
        public int destination;
        public int weight;

        public Node(int destination, int weight) {
            this.destination = destination;
            this.weight = weight;
        }
    }
    class Edge{
        public int source;
        public int destination;
        public int weight;

        public Edge(int source, int destination, int weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }
    }
}
