package DataStructure.Graph;

import java.util.List;

public interface Graph {
    /**
     * To indicate whether Graph represents an undirected or directed graph
     */
    enum GraphType{
        DIRECTED,
        UNDIRECTED
    }

    /**
     * An edge lies between two vertices - vertices are represented as two numbers
     * @param v1 : 1st vertex
     * @param v2 : 2nd vertex
     */
    void addEdge(int v1, int v2);

    /**
     * Helper to get the adjacent vertices for any vertex.
     * This method is required for all algorithms involving Graphs
     * @param v : vertex
     * @return : list of adjacent vertices
     */
    List<Integer> getAdjacentVertices(int v);
}
