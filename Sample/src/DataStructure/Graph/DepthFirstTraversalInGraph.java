package DataStructure.Graph;

import java.util.List;

public class DepthFirstTraversalInGraph {
    public static void depthFirstTraversal(Graph graph, int[] visited,int currentVertex){
        if(visited[currentVertex] == 1){
            return;
        }
        visited[currentVertex] = 1;
        List<Integer> list = graph.getAdjacentVertices(currentVertex);
        list.forEach(v -> depthFirstTraversal(graph, visited, currentVertex));
        System.out.print(currentVertex +"-->");
    }
}
