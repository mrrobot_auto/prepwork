package DataStructure;

public class BinarySearch {
    public static void main(String[] args) {
        int[] arr = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200};
        int value = 200;
        int index = binarySearch(arr, 0, arr.length, value);
        System.out.println(value + " is at index :: " + index);
        int index1 = binarySearch1(arr,value,0,arr.length);
        System.out.println(value + " is at index :: " + index1);
    }

    public static int binarySearch(int[] arr, int L, int R, int value) {
        int M = (L + R) / 2;
        if (arr[M] == value) return M;
        System.out.println("Middle is :: " + M);
        if (L == M) return -1;
        if (arr[M] < value) L = M ;
        if (arr[M] > value) R = M ;
        return binarySearch(arr, L, R, value);
    }
    public static int binarySearch1(int[] sortedArray, int number, int min, int max) {
        if (min > max) {
            return -1;
        }

        int mid = min + (max - min) / 2;
        if (sortedArray[mid] == number) {
            return mid;
        }
        System.out.println("Middle is :: " + mid);
        if (sortedArray[mid] > number)  {
            return binarySearch1(sortedArray, number, min, mid - 1);
        } else {
            return binarySearch1(sortedArray, number, mid + 1, max);
        }
    }
}
