package DataStructure.Sorts;

/**
 * Created by jananiravi on 11/15/15.
 */
public class MergeSort {

    private static int listToSort[] = new int[]{3, 5, 6, 8, 10, 1, 2, 4, 7, 9};

    public static void main(String[] args) {
        sort(listToSort,0, listToSort.length);
        /*print(listToSort);
        mergeSort(listToSort);*/
    }

    static void sort(int[] a, int l, int r){
        if(l>r) return;
        int m = l+(r-l)/2;
        sort(a,l,m);
        sort(a,m+1,r);
        merge(a,l,m,r);
    }
    static void merge(int[]a, int l, int m, int r){
        //get the size
        int n1 = m-l-1;
        int n2 = r-m;

        //declare 2 arrays to be merged
        int[] LS = new int[n1];
        int[] RS = new int[n2];
        //form arrays to be merged
        for(int i = 0; i< n1 ; i++){
            LS[i] = a[i];
        }
        for(int i = m; i< r ; i++){
            RS[i] = a[i];
        }
        int i = 0 ,j = 0;
        //merge the arrays and sort them along the way
        int k = l;
        while(i<n1 && j<n2){
            if (LS[i] <= RS[j]) {
                a[k] = LS[i];
                i++;
            }else{
                a[k] = RS[j];
                j++;
            }
        }
        while (i<n1){
            a[k] = LS[i];
            i++;
            k++;
        }
        while(j<n2){
            a[k] = RS[j];
            j++;
            k++;
        }
    }
    public static void print(int[] listToSort) {
        for (int el : listToSort) {
            System.out.print(el + ",");
        }
        System.out.println();
    }

    public static void mergeSort(int[] listToSort) {
        if (listToSort.length == 1) {
            return;
        }

        int midIndex = listToSort.length / 2 + listToSort.length % 2;
        int[] listFirstHalf = new int[midIndex];
        int[] listSecondHalf = new int[listToSort.length - midIndex];
        split(listToSort, listFirstHalf, listSecondHalf);

        mergeSort(listFirstHalf);
        mergeSort(listSecondHalf);

        merge(listToSort, listFirstHalf, listSecondHalf);
        print(listToSort);
    }

    public static void split(int[] listToSort, int[] listFirstHalf, int[] listSecondHalf) {
        int index = 0;
        int secondHalfStartIndex = listFirstHalf.length;
        for (int elements : listToSort) {
            if (index < secondHalfStartIndex) {
                listFirstHalf[index] = listToSort[index];
            } else {
                listSecondHalf[index - secondHalfStartIndex] = listToSort[index];
            }
            index++;
        }
    }

    public static void merge(int[] listToSort, int[] listFirstHalf, int[] listSecondHalf) {
        int mergeIndex = 0;
        int firstHalfIndex = 0;
        int secondHalfIndex = 0;
        while (firstHalfIndex < listFirstHalf.length && secondHalfIndex < listSecondHalf.length) {
            if (listFirstHalf[firstHalfIndex] < listSecondHalf[secondHalfIndex]) {
                listToSort[mergeIndex] = listFirstHalf[firstHalfIndex];
                firstHalfIndex++;
            } else if (secondHalfIndex < listSecondHalf.length) {
                listToSort[mergeIndex] = listSecondHalf[secondHalfIndex];
                secondHalfIndex++;
            }
            mergeIndex++;
        }

        if (firstHalfIndex < listFirstHalf.length) {
            while (mergeIndex < listToSort.length) {
                listToSort[mergeIndex++] = listFirstHalf[firstHalfIndex++];
            }
        }
        if (secondHalfIndex < listSecondHalf.length) {
            while (mergeIndex < listToSort.length) {
                listToSort[mergeIndex++] = listSecondHalf[secondHalfIndex++];
            }
        }
    }

}
