package DataStructure.Sorts;

import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        int[] listToSort = {3, 2, 8, 4, 1, 0, 9};
        /*int[] rev = {9,0,1,4,8,2,3};
        bubbleSort(rev);*/
        System.out.println("------------------");
          /*  for (int i = 0; i < listToSort.length; i++) {
                if (listToSort[i] > listToSort[(i + 1) % listToSort.length]) {
                    int temp = listToSort[i];
                    listToSort[i] = listToSort[(i + 1) % listToSort.length];
                    listToSort[(i + 1) % listToSort.length] = temp;
                    Arrays.stream(listToSort).forEach(System.out::print);
                }else {
                    return;
                }
                System.out.println();

            }*/
        bubbleSort(listToSort,7);
        int f = -1;
        for (int i = 0; i < listToSort.length; i++) {
            f = 1;
            for (int j = 0; j < listToSort.length - 1; j++) {
                if (listToSort[j] > listToSort[j + 1]) {
                    int temp = listToSort[j];
                    listToSort[j] = listToSort[j + 1];
                    listToSort[j + 1] = temp;

                    f = 0;
                }

            }
            print(listToSort);
            if (f == 1) {
                break;
            }
        }
    }
    public static void print(int[] listToSort) {
        for (int el : listToSort) {
            System.out.print(el + ",");
        }
        System.out.println();
    }

    public static void swap(int[] listToSort, int iIndex, int jIndex) {
        int temp = listToSort[iIndex];
        listToSort[iIndex] = listToSort[jIndex];
        listToSort[jIndex] = temp;
    }

    public static void bubbleSort(int[] listToSort) {
        for (int i = 0; i < listToSort.length; i++) {
            boolean swapped = false;
            for (int j = listToSort.length - 1; j > i; j--) {
                if (listToSort[j] < listToSort[j - 1]) {
                    swap(listToSort, j, j - 1);
                    swapped = true;
                }
            }
            print(listToSort);
            if (!swapped) {
                break;
            }
        }
    }
    //@GFG: Prefer this over other methods
    public static void bubbleSort(int a[], int n)
    {
        //code here
        for(int i = 0; i<n ; i++){
            for(int j = 0; j < n-i-1; j ++){
                if(a[j]>a[j+1]){
                    swap(j,j+1,a);
                }
            }
        }
    }

    static void swap(int i, int j, int[] a){
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
