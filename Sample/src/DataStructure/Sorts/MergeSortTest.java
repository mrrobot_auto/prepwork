package DataStructure.Sorts;

public class MergeSortTest {
    private static final int[] a = new int[]{3, 5, 6, 8, 10, 1, 2, 4, 7, 9};


    public static void mergeSort(int[] a) {
        if (a.length == 1) {
            return;
        }
        int[] p = new int[a.length / 2 + a.length % 2];
        int[] q = new int[a.length / 2];
        spilt(a, p, q);
        mergeSort(p);
        mergeSort(q);

        merge(a, p, q);
        print(a);
    }

    public static void spilt(int[] mainList, int[] firstHalf, int[] secondHalf) {
        int index = 0;
        int firstIndexSecondHalf = firstHalf.length;
        for (int i : mainList) {
            if (index < firstIndexSecondHalf) {
                firstHalf[index] = i;
            } else {
                secondHalf[index - firstIndexSecondHalf] = i;
            }
            index++;
        }
    }

    public static void main(String[] args) {
        mergeSort(a);
    }

    public static void merge(int[] a, int[] firstHalf, int[] secondHalf) {
        int index = 0;
        int firstHalfIndex = 0;
        int secondHalfIndex = 0;

        while (firstHalfIndex < firstHalf.length && secondHalfIndex < secondHalf.length) {
            if (firstHalf[firstHalfIndex] < secondHalf[secondHalfIndex]) {
                a[index] = firstHalf[firstHalfIndex];
                firstHalfIndex++;
            } else {
                a[index] = secondHalf[secondHalfIndex];
                secondHalfIndex++;
            }
            index++;
        }
        if (firstHalfIndex < firstHalf.length) {
            while (index < a.length) {
                a[index++] = firstHalf[firstHalfIndex++];
            }
        }
        if (secondHalfIndex < secondHalf.length) {
            while (index < a.length) {
                a[index++] = secondHalf[secondHalfIndex++];
            }
        }

    }

    public static void print(int[] listToSort) {
        for (int el : listToSort) {
            System.out.print(el + ",");
        }
        System.out.println();
    }

}