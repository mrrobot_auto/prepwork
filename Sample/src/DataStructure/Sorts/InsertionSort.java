package DataStructure.Sorts;

public class InsertionSort {
    public static void main(String[] args) {

        int[] l = {3, 2, 8, 4, 1, 0, 9, 7, 5, 6, 58, 65, 90, 85};
        insertionSort(l,l.length);
        /*int interval = 3;
        for (int j = 0; j < l.length; j++) {
            for (int i = j; i < l.length - interval; i += interval) {
                if (l[i] > l[i + interval]) {
                    int temp = l[i];
                    l[i] = l[i + interval];
                    l[i + interval] = temp;
                    print(l);
                }
            }
        }
        System.out.println("in sort");
        insertionSort(l);*/
    }

    public static void insertionSort(int[] listToSort) {
        for (int i = 0; i < listToSort.length - 1; i++) {
            if (listToSort[i] > listToSort[i + 1]) {
                for (int j = i + 1; j > 0; j--) {
                    if (listToSort[j] < listToSort[j - 1]) {
                        int temp = listToSort[j];
                        listToSort[j] = listToSort[j - 1];
                        listToSort[j - 1] = temp;
                    } else break;
                }
                print(listToSort);
            }
        }
    }

    static void print(int[] list) {
        for (int i : list) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }
    public static void insertionSort(int arr[], int n)
    {
        //code here
        for(int i = 0; i < n-1 ; i++){
            if(arr[i]>arr[i+1]){
                int key = arr[i+1];
                for(int j=i+1; j>0; j--){
                    if(arr[j]<arr[j-1]){
                        swap(j,j-1,arr);
                    }else break;
                }
            }
        }
    }
    public static void swap(int i, int j, int[] a){
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
