package DataStructure.TrieDS;

public interface ITrie {
    boolean search(String word);

    void insert(String word);

    boolean startsWith(String word);
    boolean starts(String word);

}