package DataStructure.TrieDS;

public class CallerTest {
    public static void main(String[] args) {
        ITrie trie = new Trie();
        trie.insert("sujay");
        trie.insert("ghorpade");
        trie.search("sujay");
        trie.startsWith("gho");
        trie.starts("su...");
    }
}
