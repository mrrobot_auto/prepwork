package DataStructure.TrieDS;

public class Node {
    public char c;
    public boolean isWord;
    public Node[] characters;

    public Node(char c) {
        this.c = c;
        isWord = false;
        characters = new Node[26];
    }
}
