package DataStructure.TrieDS;

public class Trie implements ITrie {
    private Node root;

    public Trie() {
        root = new Node('\0');
    }

    @Override
    public boolean search(String word) {
        return getNode(word) != null && getNode(word).isWord;
    }

    @Override
    public void insert(String word) {
        Node curr = root;
        for (int i = 0; i < word.length(); i++) {
            char character = word.charAt(i);
            if (curr.characters[character - 'a'] == null) {
                curr.characters[character - 'a'] = new Node(character);
            }
            curr = curr.characters[character - 'a'];
        }
        curr.isWord = true;
    }

    private Node getNode(String word) {
        Node curr = root;
        for (int i = 0; i < word.length(); i++) {
            char character = word.charAt(i);
            if (curr.characters[character - 'a'] == null) return null;
            curr = curr.characters[character - 'a'];
        }
        return curr;
    }

    public boolean starts(String word) {
        return this.starts(root, word, 0);
    }

    private boolean starts(Node node, String word, int index) {
        if (index == word.length()) {
            return node.isWord;
        }
        char character = word.charAt(index);
        if (character == '.') {
            if (node != null) {
                for (Node n : node.characters) {
                    return starts(n, word, index + 1);
                }
            }
        } else {
            Node curr = node.characters[character - 'a'];
            if (curr == null) {
                return false;
            }
            return starts(curr, word, index + 1);
        }
        return false;
    }

    @Override
    public boolean startsWith(String word) {
        return getNode(word) != null;
    }
}
