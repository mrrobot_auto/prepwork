package morgan;

import java.io.Serializable;

public class AB extends AC implements Serializable {
    public AB() {
        this(5);
        System.out.println("AB here");

    }

    public AB(int a) {
        System.out.println("int here : " + a);
    }

    @Override
    public void m1() {
        System.out.println("AB.m1");
    }

    public static void m2() {

        System.out.println("I'm static here in AB");
    }

    public static void main(String[] args) {

    }
}
