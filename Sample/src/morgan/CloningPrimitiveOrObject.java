package morgan;

import java.util.HashSet;
import java.util.Set;

public class CloningPrimitiveOrObject {
	public static void main(String[] args) {
		int[][] data = {{123 },{4,5,6 }};
		//123 0 0 
		//4 5 6
		System.out.println(data[0][0]);
		//System.out.println(data[0][1]);
		//System.out.println(data[0][2]);
		/*System.out.println(data[1][0]);
		System.out.println(data[1][1]);
		System.out.println(data[1][2]);*/

		int[][] copy = data.clone();
		copy[0][0] = 100;
		System.out.println(data[0][0]);//123
		System.out.println(copy[0][0]);//100
		copy[1] = new int[] { 300, 400, 500 };
		System.out.println(data[1][1]);//5
		System.out.println(copy[1][1]);//400

	System.out.println("*******************************************************");	
		char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e','i', 'n', 'a', 't', 'e', 'd' };  

		char[] copyTo = new char[7];
		System.arraycopy(copyFrom, 2, copyTo, 0, copyTo.length);
		System.out.println(copyTo);
	System.out.println("*******************************************************");	
         
	     Set shortSet = new HashSet();
	     for(short i=0;i<100;i++){
	    	 shortSet.add(i);
	    	 shortSet.remove((short)i-1);
	    	 
	     }
		System.out.println(shortSet);
System.out.println("********************Int Set***********************************");	
		
		  Set intSet = new HashSet();
		     for(int i=0;i<100;i++){
		    	 shortSet.add(i);
		    	 shortSet.remove(i-1);
		    	 
		     }
			System.out.println(intSet);
		
	}
}
