package morgan;

public class SingletonClass {
    private static SingletonClass single_instance = null;

    public String s;

    private SingletonClass() {
        s = "Hello I am a string part of Singleton class";
        System.out.println(s);
    }

    public static SingletonClass getInstance() {
        if (single_instance == null)
            single_instance = new SingletonClass();
        return single_instance;
    }

    public static void main(String[] args) {
        System.out.println(SingletonClass.getInstance());
        System.out.println(SingletonClass.getInstance());
        System.out.println(SingletonClass.getInstance());
    }
}
