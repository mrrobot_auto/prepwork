package leetcode;


import java.util.*;

public class MinimumDistanceBetweenBSTNodes {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(27);
        node.right= new TreeNode(34);
        node.right.right = new TreeNode(58);
        node.right.right.left = new TreeNode(50);
        node.right.right.left.left = new TreeNode(44);
        new MinimumDistanceBetweenBSTNodes().minDiffInBST(node);
    }
    public int minDiffInBST(TreeNode root) {
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        List<Integer> li = new ArrayList<>();
        while(!q.isEmpty()){
            TreeNode n = q.poll();
            if(n == null) continue;
            li.add(n.val);
            q.offer(n.left);
            q.offer(n.right);
        }
        li.sort(Comparator.reverseOrder());
        int diff = Integer.MAX_VALUE;
        for (int i = 0; i < li.size()-1; i++) {
            diff = Math.min(diff,(li.get(i)-li.get(i+1)));
        }
        return diff;
    }
    public int max(TreeNode root){
        if(root == null) return 0;
        return Math.max(root.val, Math.max(max(root.left),max(root.right)));
    }
    public int max(TreeNode root, int high){
        if(root == null) return 0;
        if(root.val == high) return 0;
        return Math.max(root.val, Math.max(max(root.left), max(root.right)));
    }
    public int min(TreeNode root){
        if(root == null) return Integer.MAX_VALUE;
        return Math.min(root.val, Math.min(min(root.left),min(root.right)));
    }
}
