package leetcode;

public class DeleteDuplicatesFromSortedLinkedList {
    public static void main(String[] args) {
        ListNode head = new ListNode();
        head.val = 1;
        head.next = new ListNode();
        head.next.val = 2;
        head.next.next = new ListNode();
        head.next.next.val = 2;
        /*head.next.next.next = new ListNode();
        head.next.next.next.val = 3;
        head.next.next.next.next = new ListNode();
        head.next.next.next.next.val = 4;
        head.next.next.next.next.next = new ListNode();
        head.next.next.next.next.next.val = 4;
        head.next.next.next.next.next.next = new ListNode();
        head.next.next.next.next.next.next.val = 5;
*/
        new DeleteDuplicatesFromSortedLinkedList().deleteDuplicatesDistinct(head);
    }

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode curr = head;
        while (curr.next != null) {
            if (curr.val != curr.next.val) {
                curr = curr.next;
            } else {
                curr.next = curr.next.next;
            }
        }
        return head;
    }

    public ListNode deleteDuplicatesDistinct(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode curr = head;
        boolean flag = false;
        while (curr.next != null) {
            if (curr.val != curr.next.val) {
                if (flag) {
                    ListNode temp = curr.next;
                    curr.val = temp.val;
                    curr.next = temp.next;
                    flag = false;
                } else
                    curr = curr.next;
            } else {
                curr.next = curr.next.next;
                flag = true;
            }
        }
        if (flag) {
            curr.val = -101;
            ListNode listNode = head;
            if (listNode.val == -101) {
                return null;
            }
            while (listNode.next != null) {
                if (listNode.next.val == -101) {
                    listNode.next = null;
                } else
                    listNode = listNode.next;
            }
        }
        return head;
    }
}
