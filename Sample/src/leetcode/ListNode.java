package leetcode;

public class ListNode {
    public int val;
    public ListNode next;
    public ListNode(int[] values){
        for(int i : values){
            if(next == null){
                this.val = i;
                this.next = new ListNode();
            }else {
                ListNode temp = next;
                while(temp.next != null){
                    temp = temp.next;
                }
                temp.next = new ListNode(i);
            }
        }
    }
    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
    public ListNode(int val){
        this.val = val;
        this.next = null;
    }
    public ListNode(){}
    public ListNode addArrayInListNode(int[] values){
        ListNode head = null;
        for(int i : values){
            if(head == null){
                head = new ListNode(i);
            }else {
                ListNode temp = head;
                while(temp.next != null){
                    temp = temp.next;
                }
                temp.next = new ListNode(i);
            }
        }
        return head;
    }
    public void printNodes(ListNode head){
        ListNode temp = head;
        while(temp != null){
            System.out.print(temp.val + " ");
            temp = temp.next;
        }
        System.out.println();
    }
}
