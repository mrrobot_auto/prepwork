package leetcode;

public class NextGreaterElement {
    public static void main(String[] args) {
        new NextGreaterElement().nextGreaterElement(new int[]{4, 1, 2},new int[]{1,3,4,2});
    }

    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int[] res = new int[nums1.length];
        int index = 0;
        boolean flag = false;
        for (int j : nums1) {
            for (int k : nums2) {
                if (j == k && !flag) {
                    flag = true;
                }
                if (flag) {
                    if (j < k) {
                        res[index] = k;
                        index++;
                        flag = false;
                        break;
                    }
                }
            }
            if (flag) {
                res[index] = -1;
                index++;
                flag = false;
            }
        }
        return res;
    }
}
