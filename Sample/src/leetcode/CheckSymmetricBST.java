package leetcode;

import geeksforgeeks.TreeNode;

import java.util.Random;

public class CheckSymmetricBST {
    public static void main(String[] args) {

    }
    public boolean isSymmetric(TreeNode root) {

        if(root == null) return true;
        if(root.left == null && root.right == null) return true;
        if(root.left == null) return false;
        if(root.right == null) return false;
        if(root.right.data == root.left.data) return true;
        return isSymmetric(root.left) && isSymmetric(root.right);
    }
}
