package leetcode;

import java.util.Scanner;

public class GoldenRectangle {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int numberOfTriangles = s.nextInt();
        int count = 0;
        for (int i = 0; i < numberOfTriangles; i++) {
            double width = s.nextInt();
            double height = s.nextInt();
            double ratio;
            if (width > height) {
                ratio = width / height;
            } else {
                ratio = height / width;
            }
            if (ratio >= 1.6 && ratio <= 1.7)
                count++;
        }
        System.out.println(count);
    }

    public static int countGoldenRectangles(int n) {
        if (n <= 0) return -1;
        int count = 0;
        Scanner s = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            double width = s.nextDouble();
            double height = s.nextDouble();
            double ratio;
            if (width > height) {
                ratio = width / height;
            } else {
                ratio = height / width;
            }
            if (ratio >= 1.6 && ratio <= 1.7)
                count++;
        }
        return count;
    }
}
