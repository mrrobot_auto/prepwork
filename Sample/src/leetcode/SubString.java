package leetcode;

public class SubString {
    public static void main(String[] args) {
        System.out.println(substring("beadcfabcdefabcfkh", "*****h"));
    }

    public static int substring(String s, String x) {
        char[] xCharArr = x.toCharArray();
        int i = 0;
        char[] sCharArr = s.toCharArray();
        int j = 0;
        int result = -1;
        while (i < xCharArr.length) {
            if (j == s.length()) {
                return -1;
            }
            if (xCharArr[i] == '*') {
                result = j;
                i++;
                j++;
            } else {
                if (xCharArr[i] != sCharArr[j]) {
                    result = -1;
                    i = 0;
                    j++;
                } else {
                    result = j;
                    i++;
                    j++;
                }
            }
        }
        return result - (xCharArr.length - 1);

        //        String s1;
//        String s2;
//        int index;
//        if(x.contains("*")){
//            s1 = x.split("\\*")[0];
//            s2 = x.split("\\*")[1];
//            if(s.indexOf(s1) - s.substring(s.indexOf(s1)+s1.length()).indexOf(s2) == s1.length())
//                return s.indexOf(s1);
//        }
//        return s.indexOf(x);
    }
}
