package leetcode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class SolveEquation {
    public static void main(String[] args) {
        new SolveEquation().solveEquation("2x+3x-6x=x+2");
    }

    public String solveEquation(String equation) {
        int sumx = 0;
        Queue<Integer> q = new LinkedList<>();

        int sum = 0;
        String[] eqs = equation.split("=");
        char[] left = eqs[0].toCharArray();
        char[] right = eqs[1].toCharArray();
        Deque<String> nums = new LinkedList<>();
        Deque<String> ops = new LinkedList<>();
        StringBuilder sb = new StringBuilder();
        if (left[0] != '-') {
            ops.push("+");
        }
        for (Character f : left) {
            if (f == '+' || f == '-') {
                if (!sb.toString().equals("")) {
                    nums.push(sb.toString());
                    sb = new StringBuilder();
                }
                ops.push(String.valueOf(f));
            } else {
                sb.append(f);
            }
        }
        if (!sb.toString().equals("")) {
            nums.push(sb.toString());
            sb = new StringBuilder();
        }

        if (right[0] != '-') {
            ops.push("-");
        }
        for (Character r : right) {
            if (r == '+' || r == '-') {
                if (!sb.toString().equals("")) {
                    nums.push(sb.toString());
                    sb = new StringBuilder();
                }
                if (r == '-') ops.push("+");
                else ops.push("-");
            } else {
                sb.append(r);
            }
        }
        if (!sb.toString().equals("")) {
            nums.push(sb.toString());
            sb = new StringBuilder();
        }
        while (!nums.isEmpty()) {
            if (nums.peek().contains("x")) {
                sb.append(nums.pop());
                sb.deleteCharAt(sb.lastIndexOf("x"));
                if (sb.toString().equals("")) sb.append("1");
                sumx = calculate(sumx, ops.pop(), Integer.parseInt(sb.toString()));
                sb = new StringBuilder();
            } else {
                sum = calculate(sum, ops.pop(), Integer.parseInt(nums.pop()));
            }
        }

        if(sumx != 0){
            return "x="+((sum*(-1)) / sumx);
        }
        else if(sumx == 0 && sum==0){
            return "Infinite solutions";
        }

        return "No solution";
    }

    int calculate(int sum, String o, int d) {
        switch (o) {
//            case "+" -> sum += d;
//            case "-" -> sum -= d;
        }
        return sum;
    }
}
