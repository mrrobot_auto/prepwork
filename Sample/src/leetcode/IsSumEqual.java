package leetcode;

import java.util.HashMap;
import java.util.Map;

public class IsSumEqual {
    public static void main(String[] args) {
        new IsSumEqual().isSumEqual("bc","cb","dd");
    }
    public boolean isSumEqual(String firstWord, String secondWord, String targetWord) {
        Map<Character,Integer> map = new HashMap<>();
        map.put('a',0);
        map.put('b',1);
        map.put('c',2);
        map.put('d',3);
        map.put('e',4);
        map.put('f',5);
        map.put('g',6);
        map.put('h',7);
        map.put('i',8);
        map.put('j',9);
        return getInt(firstWord,map)+getInt(secondWord,map) == getInt(targetWord,map);

    }

    int getInt(String s, Map m){
        int res = 0;
        char[] chars = s.toCharArray();
        if(chars.length==0) return -1;
        for(char c : chars){
            res = (res*10) + (int)m.get(c);
        }
        return res;
    }
}
