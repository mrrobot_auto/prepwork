package leetcode;

public class ClimbStairs {
    public static void main(String[] args) {
        new ClimbStairs().fib(5);
    }
    public int climbStairs(int n) {
        if(n <= 2) return n;
        return climbStairs(n-1) + climbStairs(n-2);
    }

    public void fib(int n) {
        if(n<=0) {
            System.out.println(n);
            return;
        }
        System.out.println(n);
        fib(n-1);
    }
}

