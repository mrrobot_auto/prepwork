package leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StringSubset {
    public static void main(String[] args) {
        System.out.println(isSub("Hello I am sujay","I am not"));
    }
    public static boolean isSub(String parent, String child){
        Set<String> set = new HashSet<>(Arrays.asList(parent.split(" ")));
        for(String c : child.split(" ")){
            if(set.add(c)) return false;
        }
        return true;
    }
}
