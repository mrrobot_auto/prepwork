package leetcode;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class findNumberOfLIS {
    public static void main(String[] args) {
        new findNumberOfLIS().findNumberOfLIS(new int[]{2,2,2,2,2});
    }

    public int findNumberOfLIS(int[] nums) {
        Set<String> set = new HashSet<>();
        int maxSize = 0;
        int skipper = 0;
        int counter = 0;
        StringBuilder sb = new StringBuilder();
        sb.append(nums[1]);
        while (skipper < nums.length) {
            if (counter != skipper && nums[counter] > Integer.parseInt(sb.substring(sb.length() - 1))) {
                sb.append(nums[counter]);
            }
            counter++;
            if(counter > nums.length - 1){
                skipper++;
                counter = 0;
                set.add(sb.toString());
                sb = new StringBuilder();
                sb.append(nums[0]);
            }
        }
        int max = set.stream().map(String::length).max(Integer::compareTo).get();
        for(String s : set){
            if(s.length() == max){
                maxSize++;
            }
        }
        return maxSize;
    }
}

