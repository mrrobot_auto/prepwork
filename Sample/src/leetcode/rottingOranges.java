package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class rottingOranges {
    public static void main(String[] args) {
        int[][] grid = {{2, 1, 1}, {1, 1, 0}, {0, 1, 1}};
        new rottingOranges().orangesRotting(grid);
    }

    public int orangesRotting(int[][] grid) {
        Queue<int[]> q = new LinkedList<>();
        int freshOranges = 0;
        int rottenOranges = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 2) {
                    q.offer(new int[]{i, j});
                }
                if (grid[i][j] != 0) {
                    freshOranges++;
                }
            }
        }
        int counter = 0;
        while (!q.isEmpty()) {
            int size = q.size();
            rottenOranges += size;
            for (int i = 0; i < size; i++) {
                int[] p = q.poll();
                if (grid[p[0]][p[1]] == 2) {
                    for (int[] in : getAdjacent(grid, p[0], p[1])) {
                        if (grid[in[0]][in[1]] == 1) {
                            grid[in[0]][in[1]] = 2;
                            q.offer(in);
                        }
                    }
                }
            }
            if (!q.isEmpty()) {
                counter++;
            }
        }
        return freshOranges == rottenOranges ? counter : -1;
    }

    List<int[]> getAdjacent(int[][] grid, int i, int j) {
        int m = grid.length;
        int n = grid[0].length;
        List<int[]> list = new ArrayList<>();
        if ((i >= 0 && j + 1 >= 0) && (i <= m - 1 && j + 1 <= n - 1))
            list.add(new int[]{i, j + 1});
        if ((i + 1 >= 0 && j >= 0) && (i + 1 <= m - 1 && j <= n - 1))
            list.add(new int[]{i + 1, j});
        if ((i >= 0 && j - 1 >= 0) && (i <= m - 1 && j - 1 <= n - 1))
            list.add(new int[]{i, j - 1});
        if ((i - 1 >= 0 && j >= 0) && (i - 1 <= m - 1 && j <= n - 1))
            list.add(new int[]{i - 1, j});
        return list;
    }
}
