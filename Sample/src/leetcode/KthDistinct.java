package leetcode;

import java.util.*;
import java.util.stream.Collectors;

public class KthDistinct {
    public static void main(String[] args) {
        new KthDistinct().kthDistinct(new String[]{"d","b","c","b","c","a"},2);
    }
    public String kthDistinct(String[] arr, int k) {
        Set<String> set = new LinkedHashSet<>();
        List<String> unwantedStrings = new ArrayList<>();
        for(String s : arr){
            if(!set.add(s) || unwantedStrings.contains(s)){
                set.remove(s);
                unwantedStrings.add(s);
            }
        }
        if(k<=set.size()){
            return new ArrayList<>(set).get(k-1);
        }else return "";
    }
}
