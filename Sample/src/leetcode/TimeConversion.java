package leetcode;

public class TimeConversion {
    public static void main(String[] args) {
        System.out.println(timeConversion("12:01:00pm"));
    }

    public static String timeConversion(String s) {
        String res;
        String[] times = s.split(":");
        int hour = Integer.parseInt(times[0]);
        String phase = times[2].substring(2);
        if (phase.equalsIgnoreCase("PM") && hour < 12) {
            hour += 12;
        } else if (phase.equalsIgnoreCase("AM") && hour == 12) {
            hour = 0;
        }
        String strHour = hour < 10 ? "0" + hour : String.valueOf(hour);
        res = strHour + ":" + times[1] + ":" + times[2].substring(0, 2);

        return res;
    }
}
