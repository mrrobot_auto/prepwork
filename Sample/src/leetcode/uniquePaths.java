package leetcode;

import java.util.Arrays;

public class uniquePaths {
    public static void main(String[] args) {
        new uniquePaths().uniquePaths(3,7);
    }
    public int uniquePaths(int m, int n) {
        if(m == 1 && n == 1) return 1;
        int[][] dp = new int[m+1][n+1];
        for(int[] k : dp){
            Arrays.fill(k,-1);
        }
        run(m, n, 0, 0,dp);
        return dp[0][0];
    }

    int run(int m, int n, int i, int j, int[][] dp) {
        if (i < 0 || i > m || j < 0 || j > n) return 0;
        if(dp[i][j] != -1) return dp[i][j];
        if (i == m - 1 && j == n - 1) return 1;
        return dp[i][j] = run(m, n, i, j + 1,dp) + run(m, n, i + 1, j,dp);
    }
}
