package leetcode;

import java.util.*;

public class WallsAndGate {
    public static void main(String[] args) {
        int[][] grid = {{2147483647,-1,0,2147483647},{2147483647,2147483647,2147483647,-1},{2147483647,-1,2147483647,-1},{0,-1,2147483647,2147483647}};
        new WallsAndGate().wallsAndGates(grid);
    }
    int INF = Integer.MAX_VALUE;

    private static final int[][] dirs = {{1,0},{0,1},{-1,0},{0,-1}};
    public void wallsAndGates(int[][] rooms) {
        // write your code here
        Queue<int[]> queue = new LinkedList<>();
        if(rooms == null || rooms.length==0)
            return;
        for(int i=0;i<rooms.length;i++) {
            for(int j=0;j<rooms[0].length;j++) {
                if(rooms[i][j]==0)
                    queue.add(new int[]{i,j});
            }
        }
        while(!queue.isEmpty()) {

            int[] top = queue.poll();
            for(int[] dir : dirs) {

                int i = top[0] + dir[0];
                int j = top[1] + dir[1];
                if(i>=0 && j>=0 && i<rooms.length && j<rooms[0].length) {
                    if(rooms[i][j] == Integer.MAX_VALUE ) {

                        rooms[i][j] = rooms[top[0]][top[1]]+1;
                        queue.add(new int[]{i,j});
                    }
                }
            }
        }
    }
    public void wallsAndGates0(int[][] rooms) {
        // write your code here
        int m = rooms.length;
        int n = rooms[0].length;
        boolean[][] vis = new boolean[m][n];
        for(int i = 0; i < m ; i ++){
            for(int j =0 ; j < n ; j ++){
                if(rooms[i][j] == INF){
                    rooms[i][j] = dfs(rooms,i,j,vis);
                }
            }
        }
    }
    int bfs(int[][] rooms, int i, int j,boolean[][] vis){
        int res = 0;
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{i,j});
        while(!q.isEmpty()) {
            int[] xy = q.poll();
            i = xy[0];
            j = xy[1];
            if (!vis[i][j]) {
                vis[i][j] = true;
                if (rooms[i][j] == 0) return res;
                for (int[] n : getValidN(i, j, rooms)) {
                    if (rooms[n[0]][n[1]] != -1 && !vis[n[0]][n[1]]) {
                        q.offer(n);
                    }
                }
            }
            res++;
        }
        return res;
    }

    int dfs(int[][] rooms, int i, int j, boolean[][] vis){
        int m = rooms.length;
        int n = rooms[0].length;
        if(i<0 || i>=m || j<0 || j>=n) return 0;
        if(vis[i][j]) return 0;
        vis[i][j] = true;
        if(rooms[i][j] == 0) return 1;
        if(rooms[i][j] == -1) return 0;
        return 1+ Math.min(
                Math.min(dfs(rooms,i,j+1,vis),dfs(rooms,i+1,j,vis)),
                Math.min(dfs(rooms,i,j-1,vis), dfs(rooms,i-1,j,vis)));
    }
    List<int[]> getValidN(int i, int j, int[][] nums) {
        List<int[]> list = new ArrayList<>();
        int m = nums.length;
        int n = nums[0].length;
        if ((i >= 0 && j + 1 >= 0) && (i <= m - 1 && j + 1 <= n - 1))
            list.add(new int[]{i, j + 1});
        if ((i + 1 >= 0 && j >= 0) && (i + 1 <= m - 1 && j <= n - 1))
            list.add(new int[]{i + 1, j});
        if ((i >= 0 && j - 1 >= 0) && (i <= m - 1 && j - 1 <= n - 1))
            list.add(new int[]{i, j - 1});
        if ((i - 1 >= 0 && j >= 0) && (i - 1 <= m - 1 && j <= n - 1))
            list.add(new int[]{i - 1, j});
        return list;
    }
}
