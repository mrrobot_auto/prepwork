package leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class twoSums {
    public int[] twoSum(int[] nums, int target) {
        HashMap map = new HashMap<>(); //intialize hashmap
        int complement = 0;  //empty variable to find the other number that equals targer
        for (int i = 0; i < nums.length; i++) { //looping through nums array
            complement = target - nums[i];  //calculating the other integer that equals target when added

            if (map.containsKey(complement)) {  //self explanitory
                return new int[]{(int) map.get(complement), i}; //returning new array for the answer
            }
            map.put(nums[i], i); //putting all integers in array into the hashmap key and value pair are the same number in this situation
        }
        throw new IllegalArgumentException("No matching solution");
    }
        /*int[] res = new int[2];
        for(int i=0;i<nums.length;i++){
            if(nums[i] < target){
                for(int j=0;j<nums.length;j++){
                    if (nums[i]+nums[j] == target){
                        if(i == j) continue;
                        res[0]=i;
                        res[1]=j;
                        return res;
                    }
                }
            }
        }
        System.out.println("Total sum is not available");
        return res;
    }*/

    public static void main(String[] args) {
       /* twoSums n = new twoSums();
        int[] r =n.twoSum(new int[]{3,2,4},6);
        Arrays.stream(r).forEach(System.out::println);*/
      /*  int a = 10, b = 10, c = 10;

        System.out.println(a++ + --b + a + b-- + a++ + b++ + c++ + a + b + c);*/
        twoSums(new int[]{0,4,3,0},0);

    }

    public static int[] twoSums(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length ; i++){
            int complement = target - nums[i];
            if(map.containsKey(nums[i])){
               return new int[] {map.get(nums[i]),i};
            }else map.put(complement,i);
        }
        return new int[]{-1,-1};
    }
}
