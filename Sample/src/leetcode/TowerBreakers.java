package leetcode;

public class TowerBreakers {
    public static void main(String[] args) {
        System.out.println(towerBreakers(1, 4));
    }

    public static int towerBreakers(int numberOfTowers, int towerHeight) {
        int count = 0;
        for (int i = 0; i < numberOfTowers; i++) {
            int heightOfIndividualTower = towerHeight;
            int divider = towerHeight - 1;
            while (heightOfIndividualTower > 1 && divider > 0) {
                {
                    int temp = towerHeight / divider;
                    if (towerHeight % temp == 0) {
                        heightOfIndividualTower = heightOfIndividualTower - divider;
                        count++;
                    } else {
                        divider--;
                    }
                }
            }
        }
        return count % 2 == 0 ? 2 : 1;
    }
}
