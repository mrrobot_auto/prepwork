package leetcode;

import geeksforgeeks.TreeNode;

import java.util.*;

public class KthSmallestElementInBST {
    public static void main(String[] args) {

    }
    public int kthSmallest(TreeNode root, int k) {
        Queue<TreeNode> q = new LinkedList<>();
        Set<Integer> set = new TreeSet<>();
        q.offer(root);
        while(!q.isEmpty()){
            TreeNode node = q.poll();
            set.add(node.data);
            if(node.left != null) q.offer(node.left);
            if(node.right != null) q.offer(node.right);
        }
        return new ArrayList<>(set).get(k-1);
    }
}
