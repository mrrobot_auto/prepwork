package leetcode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ReorderList {
    public static void main(String[] args) {
        ListNode node = new ListNode(1,null);
        node.next = new ListNode(2,null);
        node.next.next = new ListNode(3,null);
        node.next.next.next = new ListNode(4,null);
        node.next.next.next.next = new ListNode(5,null);
        new ReorderList().reorderList(node);
    }
    public void reorderList(ListNode head) {
        ListNode curr = head;
        int size = 0;
        Queue<ListNode> queue = new LinkedList<>();
        Deque<ListNode> stack = new LinkedList<>();
        while (curr.next != null) {
            size++;
            curr = curr.next;
        }
        ListNode node = head;
        int halfSize = size/2;
        for (int i = 0; i <= size; i++) {
            if(i > halfSize){
                stack.push(node);
            }else {
                queue.add(node);
            }
            node = node.next;
        }
        var temp = head;
        queue.poll();
        while(!queue.isEmpty() || !stack.isEmpty()){
           if(!stack.isEmpty()) {
               temp.next = stack.pop();
               temp = temp.next;
           }
           if(!queue.isEmpty()) {
               temp.next = queue.poll();
               temp = temp.next;
           }
        }
        temp.next = null;
    }
}
