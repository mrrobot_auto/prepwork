package leetcode;

import java.util.Scanner;
import java.util.Stack;
import java.util.stream.IntStream;

public class QueueUsing2Stacks {
    static Stack<Integer> s1 = new Stack<>();
    static Stack<Integer> s2 = new Stack<>();

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int q = sc.nextInt();
        IntStream.range(0, q).forEach(p -> {
            int act = sc.nextInt();
            switch (act) {
//                case 1 -> enqueue(sc.nextInt());
//                case 2 -> dequeue();
//                case 3 -> System.out.println(peek());

            }
        });
        sc.close();
    }

    static void enqueue(int num) {
        if (s2.isEmpty()) {
            s1.push(num);
            s2.push(s1.pop());
        } else {
            while (!s2.isEmpty()) {
                s1.push(s2.pop());
            }
            s1.push(num);
            while (!s1.isEmpty()) {
                s2.push(s1.pop());
            }
        }

    }

    static int dequeue() {
        return s2.pop();
    }

    static int peek() {
        return s2.peek();
    }
}
