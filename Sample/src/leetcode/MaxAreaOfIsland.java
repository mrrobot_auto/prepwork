package leetcode;

import leetcode75.MaxArea;

import java.util.Queue;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class MaxAreaOfIsland {
    public static void main(String[] args) {
        int[][] grid = {{0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0}, {0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0}, {0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}};
        new MaxAreaOfIsland().maxAreaOfIsland(grid);
    }

    public int maxAreaOfIsland(int[][] grid) {
        int res = 0;
        int m = grid.length;
        int n = grid[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if(grid[i][j] == 1){
                    res = Math.max(res,dfs(grid,i,j));
                }
            }
        }
        return res;
    }
    public int dfs(int[][] grid, int i, int j){
        int m = grid.length;
        int n = grid[0].length;
        if(i<0 || i>=m || j<0 || j>=n) return 0;
        if(grid[i][j] == 0) return 0;
        grid[i][j] = 0;
        return 1+ dfs(grid, i, j+1)+
                dfs(grid,i+1,j)+
                dfs(grid,i,j-1)+
                dfs(grid,i-1,j);

    }
    /*public int dfs(int[][] grid, int i, int j, boolean[][] visited, Queue<int[]> q, int count,List<Integer> li) {
        if (!visited[i][j]) {
            if (grid[i][j] == 1) {
                count++;
                q.offer(new int[]{i, j});
                while (!q.isEmpty()) {
                    int[] g = q.poll();
                    int x = g[0];
                    int z = g[1];
                    if (!visited[x][z]) {
                        visited[x][z] = true;
                        for (int[] y : getValidNeighbors(x, z, grid)) {
                            if (grid[y[0]][y[1]] == 1) {
                                count++;
                                q.offer(y);
                            }
                        }
                    }
                }
            } else {
                visited[i][j] = true;
                //System.out.println(grid[i][j]);
                for (int[] t : getValidNeighbors(i, j, grid)) {
                    dfs(grid, t[0], t[1], visited, q, count,li);
                }
            }
        }
        System.out.println(count);
        if(count>0) li.add(count);
        return count;
    }

    List<int[]> getValidNeighbors(int i, int j, int[][] nums) {
        List<int[]> list = new ArrayList<>();
        int m = nums.length;
        int n = nums[0].length;
        if ((i >= 0 && j + 1 >= 0) && (i <= m - 1 && j + 1 <= n - 1))
            list.add(new int[]{i, j + 1});
        if ((i + 1 >= 0 && j >= 0) && (i + 1 <= m - 1 && j <= n - 1))
            list.add(new int[]{i + 1, j});
        if ((i >= 0 && j - 1 >= 0) && (i <= m - 1 && j - 1 <= n - 1))
            list.add(new int[]{i, j - 1});
        if ((i - 1 >= 0 && j >= 0) && (i - 1 <= m - 1 && j <= n - 1))
            list.add(new int[]{i - 1, j});
        return list;
    }

    class Pair implements Comparable<Pair> {
        int num;
        int[] coo;

        public Pair(int num, int[] coo) {
            this.num = num;
            this.coo = coo;
        }

        @Override
        public int compareTo(Pair o) {
            return this.num - o.num;
        }
    }*/
}
