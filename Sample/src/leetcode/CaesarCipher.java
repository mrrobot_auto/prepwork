package leetcode;

public class CaesarCipher {
    public static void main(String[] args) {

        System.out.println(cipher("abc-XyZ", 46));

    }

    public static String cipher(String s, int k) {
        k = k%26;
        StringBuilder res = new StringBuilder();
        String[] strs = s.split("-");
        for (String str : strs) {
            char[] chars = str.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                if (chars[i] > 64 && chars[i] < 91) {
                    chars[i] = chars[i] + k > 90 ? (char) (64 + (chars[i] + k) % 45) : (char) (chars[i] + k);
                } else if (chars[i] > 96 && chars[i] < 123) {
                    chars[i] =
                            chars[i] + k > 122 ? (char) (96 + (chars[i] + k) % 61) : (char) (chars[i] + k);
                }
            }
            str = String.valueOf(chars);
            res.append(str).append("-");

        }
        return res.substring(0, res.toString().length() - 1);
    }
}
