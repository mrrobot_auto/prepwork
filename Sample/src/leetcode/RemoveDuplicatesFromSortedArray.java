package leetcode;

import java.util.Arrays;

public class RemoveDuplicatesFromSortedArray {
    public static void main(String[] args) {
        new RemoveDuplicatesFromSortedArray().removeDuplicates(new int[]{1,1,2,2,2,2,3,3,3,3,3,4,4,4});
    }
    public int removeDuplicates(int[] nums) {
        int temp = Integer.MIN_VALUE;
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] != temp){
                nums[count] = nums[i];
                count++;
                temp = nums[i];
            }
        }
        return count;
    }
}
