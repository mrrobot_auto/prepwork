package leetcode;

public class MergeLinkedLists {
    public static void main(String[] args) {
        SinglyLinkedListNode head1 = new SinglyLinkedListNode(1);
        SinglyLinkedListNode n1 = new SinglyLinkedListNode(2);
        SinglyLinkedListNode n2 = new SinglyLinkedListNode(3);
        head1.next = n1;
        head1.next.next = n2;
        SinglyLinkedListNode head2 = new SinglyLinkedListNode(3);
        head2.next = new SinglyLinkedListNode(4);
        SinglyLinkedListNode sorted = mergeLists(head1, head2);
        while(sorted != null){
            System.out.print(sorted.data+" ");
            sorted = sorted.next;
        }

    }

    static SinglyLinkedListNode mergeLists(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {
        SinglyLinkedListNode tailNode = null;
        SinglyLinkedListNode firstNode = null;
        while(head1 != null && head2 != null){
            if(head1.data< head2.data) {
                if (tailNode == null) {
                    tailNode = new SinglyLinkedListNode(head1.data);
                    head1 = head1.next;
                    firstNode = tailNode;
                } else {
                    SinglyLinkedListNode tempHead = tailNode;
                    SinglyLinkedListNode node = new SinglyLinkedListNode(head1.data);
                    tempHead.next = node;
                    tailNode = node;
                    head1 = head1.next;
                }
            }else {
                if (tailNode == null) {
                    tailNode = new SinglyLinkedListNode(head2.data);
                    head2 = head2.next;
                    firstNode = tailNode;
                } else {
                    SinglyLinkedListNode tempHead1 = tailNode;
                    SinglyLinkedListNode node1 = new SinglyLinkedListNode(head2.data);
                    tempHead1.next = node1;
                    tailNode = node1;
                    head2 = head2.next;
                }
            }
        }
        if(head1 != null){
            while(head1 != null){
                SinglyLinkedListNode tempHead = tailNode;
                SinglyLinkedListNode node = new SinglyLinkedListNode(head1.data);
                tempHead.next = node;
                tailNode = node;
                head1 = head1.next;
            }
        }else {
            while(head2 != null){
                SinglyLinkedListNode tempHead1 = tailNode;
                SinglyLinkedListNode node1 = new SinglyLinkedListNode(head2.data);
                tempHead1.next = node1;
                tailNode = node1;
                head2 = head2.next;
            }
        }
        return firstNode;

    }
}

class SinglyLinkedListNode {
    public int data;
    public SinglyLinkedListNode next;

    public SinglyLinkedListNode(int nodeData) {
        this.data = nodeData;
        this.next = null;
    }
}