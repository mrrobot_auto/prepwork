package leetcode;

import java.util.*;

public class MaxIncreseKeepingSkyline {
    public static void main(String[] args) {
        int[][] grid = {{3, 0, 8, 4}, {2, 4, 5, 7}, {9, 2, 6, 3}, {0, 3, 1, 0}};
        new MaxIncreseKeepingSkyline().maxIncreaseKeepingSkyline(grid);
    }

    public int maxIncreaseKeepingSkyline(int[][] grid) {
        int res = 0;
        Map<Integer, ArrayList<ArrayList<Integer>>> map = new HashMap<>();
        int m = grid.length;
        int index = 0;
        for (int[] num : grid) {
            map.put(index, new ArrayList<>());
            map.get(index).add(new ArrayList<>());
            for (int l : num) {
                map.get(index).get(0).add(l);
            }
            index++;
        }
        index = 0;
        for (int i = 0; i < m; i++) {
            map.get(index).add(new ArrayList<>());
            for (int[] ints : grid) {
                map.get(index).get(1).add(ints[i]);
            }
            index++;
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                int val = grid[i][j];
                int valid = Math.min(Collections.max(map.get(i).get(0)),
                        Collections.max(map.get(j).get(1)));
                if(val<valid){
                    res += (valid-val);
                }
            }
        }

        return res;
    }
}
