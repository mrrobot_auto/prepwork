package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GroupAnagram {
    public static void main(String[] args) {
        String[] strs = {"eat","tea","tan","ate","nat","bat"};
        new GroupAnagram().groupAnagrams(strs);
    }
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> res = new ArrayList<>();
        int index = 0;
        res.add(index,new ArrayList<>());
        for (int i = 0; i < strs.length; i++) {
            if(strs[i].equals("")) continue;
            res.get(index).add(strs[i]);
            for (int j = 0; j < strs.length; j++) {
                if(i==j || strs[j].equals("")) continue;
                if(isAnagram(strs[i],strs[j])){
                    res.get(index).add(strs[j]);
                    strs[j] ="";
                }
            }
            strs[i] = "";
            index++;
            res.add(index,new ArrayList<>());
        }
        res.removeIf(l -> l.size() == 0);
        if(res.size() == 0) {
            res =  new ArrayList<>();
            res.add(new ArrayList<>(List.of("")));
        }
        return res;
    }
    boolean isAnagram(String s1, String s2){
        char[] chars1 = s1.toCharArray();
        char[] chars2 = s2.toCharArray();
        Arrays.sort(chars1);
        Arrays.sort(chars2);
        return Arrays.equals(chars1,chars2);
    }
}
