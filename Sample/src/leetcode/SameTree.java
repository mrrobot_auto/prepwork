package leetcode;

import geeksforgeeks.TreeNode;

public class SameTree {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(1);
        node.left = new TreeNode(2);
        node.right = new TreeNode(3);

        TreeNode node1 = new TreeNode(1);
        node1.left = new TreeNode(2);
        node1.right = new TreeNode(3);
        new SameTree().isSameTree(node,node1);
    }
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if(p == null && q == null) return true;
        if(p == null) return false;
        if(q == null) return false;
//        if(p.left == null && q.right == null) return true;
//        if(p.left == null) return false;
//        if(q.right == null) return false;
//        if(p.left.data != q.left.data) return false;
//        if(p.right.data != q.right.data) return false;
        if(p.val != q.val) return false;
        return isSameTree(p.left,q.left) && isSameTree(p.right,q.right);
    }
}
