package leetcode;

public class reverseInt {
    public static void main(String[] args) {
        new reverseInt().reverseNum(10563);


    }
    void printRev(int num){
        int temp;
        if (num < 0) {
            System.out.print("-");
            num = num * -1;
        }
        do {
            temp = num % 10;
            System.out.print(temp);
            num = num / 10;
        } while (num != 0);
    }
    public int reverseNum(int x){
        int result =0;
        int pop;

        while(x!=0){
            pop=x%10;
            x/=10;

            result=(result*10)+pop;
        }

        return result;
    }
}
