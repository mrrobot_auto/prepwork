package leetcode;

public class LengthOfLongestSubstringKDistinct {
    public static void main(String[] args) {
        new LengthOfLongestSubstringKDistinct().lengthOfLongestSubstringKDistinct("world",3);
    }
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // write your code here
        int len = 0;
        int[] ls = new int[26];
        char[] chars = s.toCharArray();
        for(char c : chars){
            ls[c - 'a'] +=1;
        }
        int i = 0;int j = 0;
        int tempk = k;
        int[] tempLs = new int[26];
        StringBuilder st = new StringBuilder();
        while (i < chars.length){
            char ch = chars[i];
            tempLs[ch-'a'] += 1;
            if(tempLs[ch-'a']==ls[ch-'a']){
                tempk--;
            }
            st.append(ch);
            if (tempk==0){
                String t = st.toString();
                st = new StringBuilder();
                len = Math.max(t.length(),len);
                tempLs = new int[26];
                j ++;
                i = j;
                tempk = k;
            }
            i++;
        }
        return len;
    }
}
