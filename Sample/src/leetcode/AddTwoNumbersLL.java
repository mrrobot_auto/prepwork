package leetcode;

import geeksforgeeks.Node;

import java.math.BigInteger;

public class AddTwoNumbersLL {
    public static void main(String[] args) {
        Node node = new Node(2);
        node.next = new Node(4);
        node.next.next = new Node(3);
        Node node1 = new Node(5);
        node1.next = new Node(6);
        node1.next.next = new Node(4);
        new AddTwoNumbersLL().addTwoNos(node,node1);
    }
    public Node addTwoNumbers(Node l1, Node l2) {
        Node res = null;
        Node head = null;
        Node one = reverse(l1);
        Node two = reverse(l2);
        StringBuilder sb = new StringBuilder();
        while (one != null) {
            sb.append(one.value);
            one = one.next;
        }
        BigInteger firstNum = new BigInteger(sb.toString());

        sb = new StringBuilder();
        while (two != null) {
            sb.append(two.value);
            two = two.next;
        }
        BigInteger secondNum = new BigInteger(sb.toString());

        BigInteger result = firstNum.add(secondNum);
        String temp = String.valueOf(result);
        char[] chars = temp.toCharArray();
        for (char c : chars) {
            if (res == null) {
                res = new Node(Integer.parseInt(String.valueOf(c)));
                head = res;
            } else {
                while (res.next != null) {
                    res = res.next;
                }
                res.next = new Node(Integer.parseInt(String.valueOf(c)));
            }
        }
        return reverse(head);
    }

    Node reverse(Node head) {
        Node current = head;
        Node next = null;
        Node prev = null;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

    public Node addTwoNos(Node l1, Node l2) {
        l1 = this.reverse(l1);
        l2 = this.reverse(l2);
        int n1 = this.getNumber(l1);
        int n2 = this.getNumber(l2);
        int n3 = n1 + n2;
        Node res = null;
        while (n3 != 0) {
            int c = n3 % 10;
            n3 = n3 / 10;
            res = this.add(c, res);
        }
        return res;
    }

    private int getNumber(Node head) {
        int num = 0;
        Node curr = head;
        while (curr != null) {
            num = num * 10 + curr.value;
            curr = curr.next;
        }
        return num;
    }

    private Node add(int val, Node head) {
        if(head == null){
            head = new Node(val);
        }else {
            Node curr = head;
            while(curr.next != null){
                curr = curr.next;
            }
            curr.next = new Node(val);
        }
        return head;
    }
}
