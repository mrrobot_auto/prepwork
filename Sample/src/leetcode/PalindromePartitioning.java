package leetcode;

import java.util.LinkedList;

public class PalindromePartitioning {
    public static void main(String[] args) {
        part("");
    }

    public static void part(String s) {
        char[] p = s.toCharArray();
        StringBuilder res = new StringBuilder();
        LinkedList<Character> l = new LinkedList<>();
        for (char c :
                p) {
            l.add(c);
        }
        for (int i = 0; i < l.size(); i++) {
            if(res.length() == 1) {
                System.out.println(res);
            }
            res = new StringBuilder();
            res.append(l.get(i));
            for (int j = i + 1; j < l.size(); j++) {
                res.append(l.get(j));
                if(isPal(res.toString())){
                    System.out.println(res);
                    System.out.println();
                }
            }
        }
    }

    public static boolean isPal(String s) {
        return s.equals(reverse(s));
    }

    public static String reverse(String s) {
        char[] o = s.toCharArray();
        char[] out = new char[o.length];
        int len = o.length - 1;
        for (int i = len; i >= 0; i--) {
            out[len - i] = o[i];
        }
        return String.valueOf(out);
    }
}
