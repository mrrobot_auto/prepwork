package leetcode;

public class ArrayMultiplication {
    public static void main(String[] args) {
        System.out.println(arrayMul(new int[]{1, 2, 3, 4, 5, 6}));
    }

    public static String arrayMul(int[] a) {
        StringBuilder r = new StringBuilder();
        int prod = 1;
        for (int i : a) {
            prod = prod * i;
        }
        for (int j : a) {
            r.append(prod / j).append("-");
        }
        return r.deleteCharAt(r.lastIndexOf("-")).toString();
    }
}
