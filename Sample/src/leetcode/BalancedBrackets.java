package leetcode;

import java.util.*;

public class BalancedBrackets {
    public static void main(String[] args) {
        System.out.println(isBalanced("(()))("));
    }

    public static String isBalanced(String s) {
        // Write your code here

        Map<Character, Character> map = new HashMap<>();
        map.put('(', ')');
        map.put('{', '}');
        map.put('[', ']');
        Queue<Character> s1 = new LinkedList<>();
        Stack<Character> s2 = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c == '(' || c == '{' || c == '[') {
                s1.offer(c);
            } else if (c == ')' || c == '}' || c == ']') {
                s2.push(c);
            }
        }
        if(s1.size()!=s2.size()) return "NO";
        while (!s1.isEmpty() && !s2.isEmpty()) {
            if (!Objects.equals(map.get(s1.poll()), s2.pop())) {
                return "NO";
            }
        }
        return "YES";
    }
}
