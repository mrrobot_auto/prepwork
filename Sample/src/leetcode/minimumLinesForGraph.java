package leetcode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;

public class minimumLinesForGraph {

    public static void main(String[] args) {
        new minimumLinesForGraph().minimumLines(new int[][]{{1,7},{2,6},{3,5},{4,4},{5,4},{6,3},{7,2},{8,1}});
    }
    public int minimumLines(int[][] stockPrices) {
        int m = stockPrices.length;
        if(m<2) return 0;
        if(m == 2) return 1;
        Arrays.sort(stockPrices, Comparator.comparingInt(a -> a[0]));
        int numberOfLines = 1;
        int x = stockPrices[0][0];
        int y = stockPrices[0][1];
        int x1 = stockPrices[1][0];
        int y1 = stockPrices[1][1];
        double slope = ((y1-y)*100)/((x1-x)*1.0);
        for(int i= 2; i <m ; i++){
            int newX = stockPrices[i][0];
            int newY = stockPrices[i][1];
            int diffX = (x1 - newX);
            int diffY = (y1 - newY);
            double newSlope = (diffY*100)/(diffX*1.0);
            if(slope != newSlope){
                numberOfLines++;
            }
            x1 = newX;
            y1 = newY;
            slope = newSlope;
        }
        return numberOfLines;
    }
}
