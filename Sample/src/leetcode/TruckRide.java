package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TruckRide {
    public static void main(String[] args) {
        List<Integer> lis = new ArrayList<>(Arrays.asList(1, 3));
        List<Integer> lis1 = new ArrayList<>(Arrays.asList(10, 3));
        List<Integer> lis2 = new ArrayList<>(Arrays.asList(3, 4));
        List<List<Integer>> list = new ArrayList<>();
        list.add(lis);
        list.add(lis1);
        list.add(lis2);
        System.out.println(truckRide(list));
    }

    static int truckRide(List<List<Integer>> petrolPumps) {
        int cc;
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < petrolPumps.size(); i++) {
            cc = 0;
            for (int j = i; j < petrolPumps.size() + i; j++) {
                int index = j;
                if (index >= petrolPumps.size()) {
                    index = index % (petrolPumps.size());
                }
                cc += petrolPumps.get(index).get(0);
                cc -= petrolPumps.get(index).get(1);
                if (cc < 0) break;
                if (j == petrolPumps.size() - 1) {
                    res = i;
                }
            }
            if (res < Integer.MAX_VALUE) break;
        }
        return res;
    }
}
