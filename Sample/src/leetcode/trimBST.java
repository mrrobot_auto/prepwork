package leetcode;

import geeksforgeeks.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class trimBST {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(3);
        node.left = new TreeNode(0);
        node.right = new TreeNode(4);
        node.left.right = new TreeNode(2);
        node.left.right.left = new TreeNode(1);
        new trimBST().trimBST(node,1,3);
    }
    public TreeNode trimBST(TreeNode root, int low, int high){
        if(root == null) return root;

        if(root.val < low) return trimBST(root.right,low,high);
        if(root.val > high) return trimBST(root.left,low,high);

        root.left = trimBST(root.left,low,high);
        root.right = trimBST(root.right,low,high);
        return root;
    }
    public TreeNode trimBST0(TreeNode root, int low, int high) {
        TreeNode res =null;
        TreeNode result = new TreeNode();
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        List<Integer> list = new ArrayList<>();
        while(!q.isEmpty()){
            TreeNode n = q.poll();
            if(n.val <= high && n.val >= low){
                list.add(n.val);
            }
            if(n.left != null) q.offer(n.left);
            if(n.right != null) q.offer(n.right);
        }
        for(int n : list){
            if(res == null){
                res = new TreeNode(n);
                result = res;
            }else{
                while(res != null){
                    if(res.val < n){
                        if(res.right != null)
                            res = res.right;
                        else break;
                    }else{
                        if(res.left != null)
                            res = res.left;
                        else break;
                    }
                }
                if(res.val < n){
                    res.right = new TreeNode(n);
                }else res.left = new TreeNode(n);
            }
        }
        return result;
    }
}
