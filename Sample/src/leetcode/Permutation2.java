package leetcode;

import java.util.*;

public class Permutation2 {
    public static void main(String[] args) {
        List<String> al = new ArrayList<>();
        new Permutation2().getPermutations("abc",al,"");
        Math.pow(10,4);
        List<List<Integer>> res = new Permutation2().permuteUnique(new int[]{1,2,3});

    }
    public List<List<Integer>> permuteUnique1(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        List<String> temp = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for(int i : nums){
            sb.append(i);
        }
        String str = sb.toString();
        temp = this.getPermutations(str,temp,"");
        Set<String> set = new HashSet<>(temp);
        for(String s : set){
            char[] chars = s.toCharArray();
            List<Integer> li = new ArrayList<>();
            for(char c : chars){
                li.add(Integer.parseInt(String.valueOf(c)));
            }
            res.add(li);
        }
        return res;
    }
    public List<String> getPermutations(String str, List<String> res, String ans){

        if(str.length() == 0){
//            if(!res.contains(ans))
                res.add(ans);
        }

        for(int i = 0; i<str.length(); i++){
            char c = str.charAt(i);
            String ros = str.substring(0,i)+str.substring(i+1);
            getPermutations(ros,res,ans+c);
        }

        return res;
    }
    static void printPermutn(String str, String ans)
    {

        // If string is empty
        if (str.length() == 0) {
            System.out.println(ans + " ");
            return;
        }

        for (int i = 0; i < str.length(); i++) {

            // ith character of str
            char ch = str.charAt(i);

            // Rest of the string after excluding
            // the ith character
            String ros = str.substring(0, i) +
                    str.substring(i + 1);
            // Recurvise call
            printPermutn(ros, ans + ch);
        }
    }
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if(nums==null || nums.length==0) return res;
        boolean[] used = new boolean[nums.length];
        List<Integer> list = new ArrayList<>();
        Arrays.sort(nums);
        dfs(nums, used, list, res);
        return res;
    }

    public void dfs(int[] nums, boolean[] used, List<Integer> list, List<List<Integer>> res){
        if(list.size()==nums.length){
            res.add(new ArrayList<>(list));
            return;
        }
        for(int i=0;i<nums.length;i++){
            if(used[i]) continue;
            if(i>0 &&nums[i-1]==nums[i] && !used[i-1]) continue;
            used[i]=true;
            list.add(nums[i]);
            dfs(nums,used,list,res);
            used[i]=false;
            list.remove(list.size()-1);
        }
    }
}
