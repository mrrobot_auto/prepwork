package leetcode;

import java.util.Arrays;

public class Median {
    public static void main(String[] args) {
        int[] arr = {2,4,5,3,1,6};
        System.out.println(getMedian(arr,2,4));
    }
    public static int getMedian(int[] arr, int l, int r){
        int[] sample = new int[(r-l)+1];
        System.arraycopy(arr, l-1, sample, 0, sample.length);
        Arrays.sort(sample);
        int resIndex = sample.length/2;
        if((sample.length & 1) == 1) resIndex += 1;
        return sample[resIndex-1];
    }
}
