package leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class LegoBlocks {
    public static void main(String[] args) {
        System.out.println(legoBlocks(2, 2));
    }

    public static int legoBlocks(int n, int m) {
        // Write your code here
        int res = 0;
        List<List<Integer>> combinations = getMCombination(m);
        int ones = (int)combinations.stream().filter(l -> l.size()==1).count();
        res = n * (combinations.size()-ones);

        return (res & 1) == 1 ? res+1: res-1;
    }

    static List<List<Integer>> getMCombination(int m) {
        List<List<Integer>> res = new ArrayList<>();
        int[] a = {1, 2, 3, 4};
        int i = 0;
        int j = 3;
        for (int k = 1; k <= 4; k++) {
            List<Integer> list = new ArrayList<>();
            int finalK = k;
            if (m % finalK == 0) {
                IntStream.range(0, m / k).forEach(p -> {
                    System.out.print(finalK + " ");
                    list.add(finalK);
                });
                res.add(list);
            }
            System.out.println();
        }
        System.out.println();
        while (j > 0) {
            if (a[i] + a[j] <= m) {
                i++;
                if (i > 3) break;
            } else {
                j--;
            }
            if (a[i] + a[j] == m && i != j) {
                List<Integer> numlist = new ArrayList<>();
                System.out.println(a[i] + "  " + a[j]);
                numlist.add(a[i]);
                numlist.add(a[j]);
                res.add(numlist);
            }
        }
        return res;
    }
}
