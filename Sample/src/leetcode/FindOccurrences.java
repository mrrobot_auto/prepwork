package leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class FindOccurrences {
    public static void main(String[] args) {
        System.out.println(find("successes", 's'));
        System.out.println(find("Monty is a fat-man", "fat"));
        System.out.println(mostRepeated("successes"));
    }

    static int find(String s, char c) {
        int count = 0;
        char[] a = s.toCharArray();
        for (char f : a) {
            if (f == c) {
                count++;
            }
        }
        return count;
    }

    static int find(String s, String x) {
        // Your code here
        if (s.length() < x.length()) return -1;
        if (s.length() == x.length()) {
            if (s.equals(x)) {
                return 0;
            } else {
                return -1;
            }
        }


        int pointer = 0;
        int subP = 0;
        while (pointer < s.length()) {
            if (s.charAt(pointer) == x.charAt(subP)) {
                try {
                    if (s.startsWith(x, pointer)) {
                        return pointer;
                    }
                } catch (Exception e) {
                    return -1;
                }
            }
            pointer++;
        }
        return -1;
    }


    static char mostRepeated(String s) {
        char[] a = s.toCharArray();
        Map<Character, Integer> map = new HashMap<>();
        for (char c : a) {
            if (map.containsKey(c)) {
                int count = map.get(c);
                count += 1;
                map.put(c, count);
            } else {
                map.put(c, 1);
            }
        }
        return map.entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors.toList()).get(map.size() - 1).getKey();
    }
}
