package leetcode;

public class DivideWithoutDivision {
    public static void main(String[] args) {
        int t = new DivideWithoutDivision().divide(-2147483648,1);
        System.out.println(t);
    }
    public int divide(int dividend, int divisor) {
        long ldividend = dividend;
        long ldivisor = divisor;
        long res = 0;
        var en = false;
        var er = false;
        if(ldividend<0){
            en = true;
            ldividend *= -1;
        }
        if(ldivisor<0){
            er = true;
            ldivisor *= -1;
        }
        if(ldivisor == 1){
            res = ldividend;
            res = en ? (res*(-1)) : res;
            res = er ? (res*(-1)) : res;
            res = res == 2147483648L ? 2147483647 : res;
            return (int)res;
        }
        while(ldividend>=ldivisor){
            ldividend -= ldivisor;
            res++;
        }
        res = en ? (res*(-1)) : res;
        res = er ? (res*(-1)) : res;
        res = res == 2147483648L ? 2147483647 : res;
        return (int)res;
    }
}
