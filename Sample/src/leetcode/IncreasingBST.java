package leetcode;
import geeksforgeeks.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class IncreasingBST {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(5);
        node.left = new TreeNode(4);
        node.right = new TreeNode(6);
        node.left.left = new TreeNode(2);
        node.left.right = new TreeNode(3);
        new IncreasingBST().increasingBST(node);
    }
    public TreeNode increasingBST(TreeNode root) {
        TreeNode res = new TreeNode();
        Queue<Integer> q = new LinkedList<>();
        inorder(root,q);

        for (int i: q) {
            TreeNode treeNode = res;
            while(treeNode.left != null){
                treeNode = treeNode.left;
            }
            treeNode.left = new TreeNode(i);
        }
        return res;
    }
    void inorder(TreeNode root,Queue<Integer> res){
        if(root == null) return;
        inorder(root.left,res);

        res.offer(root.val);
        inorder(root.right,res);
    }
}
