package leetcode;

import geeksforgeeks.TreeNode;
import java.util.*;

public class SecondMinimumValueInBT {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(5);
        node.left = new TreeNode(4);
        node.right = new TreeNode(6);
        node.left.left = new TreeNode(2);
        node.left.right = new TreeNode(3);
        new SecondMinimumValueInBT().findSecondMinimumValue(node);
    }
    public int findSecondMinimumValue(TreeNode root) {
        List<Integer> list = new LinkedList<>();
        inorder(root, list);
        if(list.stream().distinct().count() == 1) return -1;
        list.sort(Comparator.reverseOrder());
        return list.get(1);
    }
    void inorder(TreeNode root, List<Integer> res){
        if(root == null) return;
        inorder(root.left,res);
        res.add(root.val);
        inorder(root.right,res);
    }
}
