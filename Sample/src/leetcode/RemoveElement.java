package leetcode;

public class RemoveElement {
    public static void main(String[] args) {
        new RemoveElement().removeElement(new int[]{3, 2, 2, 3}, 3);
    }

    public int removeElement(int[] nums, int val) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[count] = nums[i];
                count++;
            }
        }
        return count;
    }
}