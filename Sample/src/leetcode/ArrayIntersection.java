package leetcode;

import java.util.*;

public class ArrayIntersection {
    public static void main(String[] args) {
        int[] nums1 = new int[]{1,2,2,1};
        int[] nums2 = new int[]{2,2,1,1,1,1,1};
        new ArrayIntersection().intersection(nums1,nums2);
    }
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        for(int n : nums1){
            set1.add(n);
        }
        for(int n : nums2){
            set2.add(n);
        }
        set1.retainAll(set2);
        int[] res = new int[set1.size()];
        int i = 0;
        for(int n: set1){
            res[i] = n;
            i++;
        }
        return res;
    }
}
