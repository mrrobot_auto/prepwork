package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Anagram
{
    public static void main(String[] args) {
        System.out.println(amazeAna("tesla is a new slate"));
    }
    public static int anagram(String s) {
        // Write your code here
        if((s.length() & 1) == 1) return -1;
        String firstHalf = s.substring(0,s.length()/2);
        String secondHalf = s.substring(s.length()/2);
        char[] fistHalfChars = firstHalf.toCharArray();
        char[] secondHalfChars = secondHalf.toCharArray();
        Arrays.sort(fistHalfChars);
        Arrays.sort(secondHalfChars);
        int count = 0;
        for (int i = 0; i < fistHalfChars.length; i++) {
            if(fistHalfChars[i] != secondHalfChars[i]) count++;
        }
        return count;
    }
    static List<String> amazeAna(String s){
        List<String> res = new ArrayList<>();
        char[] one;
        char[] two;
        String[] strs = s.split(" ");
        for(String str : strs){
            for (String st: strs) {
                if(str.equals(st)) break;
                one =str.toCharArray();
                two = st.toCharArray();
                Arrays.sort(one);
                Arrays.sort(two);
                if(one.length == two.length){
                    for (int i = 0; i < one.length; i++) {
                        if(one[i] != two[i])
                            break;
                        if(i == one.length-1){
                            res.add(st);
                            res.add(str);
                        }
                    }

                }
            }
        }
        return res;
    }
}
