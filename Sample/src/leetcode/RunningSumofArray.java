package leetcode;

public class RunningSumofArray {
    public static void main(String[] args) {
        for(int n : new RunningSumofArray().runningSum(new int[]{1,2,3,4,5})){
            System.out.println(n);
        }
    }
    public int[] runningSum(int[] nums) {
        int sum = 0;
        int[] res = new int[nums.length];
        for(int i = 0; i < nums.length; i++){
            sum += nums[i];
            res[i] = sum;
        }
        return res;
    }
}
