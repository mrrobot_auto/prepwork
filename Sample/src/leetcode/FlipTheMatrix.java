package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FlipTheMatrix {
    public static void main(String[] args) {
        List<List<Integer>> matrix = new ArrayList<>();
        matrix.add(Arrays.asList(112, 42, 83, 119));
        matrix.add(Arrays.asList(56, 125, 56, 49));
        matrix.add(Arrays.asList(15, 78, 101, 43));
        matrix.add(Arrays.asList(62, 98, 114, 108));
        /*matrix.add(Arrays.asList(107, 54, 128, 15));
        matrix.add(Arrays.asList(12, 75, 110, 138));
        matrix.add(Arrays.asList(100, 96, 34, 85));
        matrix.add(Arrays.asList(75, 15, 28, 112));*/
        System.out.println("result = " + flip(matrix));
    }

    public static int flip(List<List<Integer>> matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.size() / 2; i++) {
            for (int j = 0; j < matrix.get(0).size() / 2; j++) {
                sum += Math.max
                        (Math.max(matrix.get(i).get(j), matrix.get(i).get(matrix.size() - j - 1))
                                , Math.max(matrix.get(matrix.size() - i - 1).get(j), matrix.get(matrix.size() - i - 1).get(matrix.size() - j - 1)));
            }
        }
        return sum;
    }

    public static int flipTheMatrix(List<List<Integer>> arr) {
        int sum = getSum(arr);
        for (int i = 0; i < arr.size(); i++) {
            if (getColSumOfFirstHalf(arr, i) < getColSumOfSecondHalf(arr, i)) {
                reverseColumn(arr, i);
            }
            sum = Math.max(sum, getSum(arr));
        }
        for (int i = 0; i < arr.size(); i++) {
            if (getRowSumOfFirstHalf(arr, i) < getRowSumOfSecondHalf(arr, i)) {
                reverseRow(arr, i);
            }
            sum = Math.max(sum, getSum(arr));
        }
        sum = Math.max(sum, getSum(arr));
        for (int i = 0; i < arr.size() / 2; i++) {
            reverseColumn(arr, i);
            sum = Math.max(sum, getSum(arr));
            reverseColumn(arr, i);
        }
        /*List<List<Integer>> mat = new ArrayList<>(arr);
        Collections.copy(mat,arr);
        for (int i = 0; i < mat.size(); i++) {
            reverseColumn(mat,i);
            sum = Math.max(getSum(mat), sum);
        }
        mat.clear();
        mat = new ArrayList<>(arr);
        Collections.copy(mat,arr);;
        for (int i = 0; i < mat.size(); i++) {
            reverseRow(mat,i);
            sum = Math.max(getSum(mat), sum);
        }

        mat.clear();
        mat = new ArrayList<>(arr);
        Collections.copy(mat,arr);
        for (int i = 0; i <mat.size(); i++) {
            reverseColumn(mat,i);
            sum = Math.max(getSum(mat), sum);
            for (int j = 0; j < mat.size() ; j++) {
                reverseRow(mat,j);
                sum = Math.max(getSum(mat),sum);
                reverseRow(mat,j);
            }
            reverseColumn(mat,i);
        }

        mat.clear();
        mat = new ArrayList<>(arr);
        Collections.copy(mat,arr);
        for (int i = 0; i <mat.size(); i++) {
            reverseRow(mat,i);
            sum = Math.max(getSum(mat), sum);
            for (int j = 0; j < mat.size() ; j++) {
                reverseColumn(mat,j);
                sum = Math.max(getSum(mat),sum);
                reverseColumn(mat,j);
            }
            reverseRow(mat,i);
        }

        mat.clear();
        mat = new ArrayList<>(arr);
        Collections.copy(mat,arr);
        for (int i = 0; i < mat.size(); i++) {
            reverseColumn(mat,i);
            sum = Math.max(getSum(mat), sum);
            for (int j = 0; j < mat.size() ; j++) {
                if(i!=j)reverseRow(mat,j);
                sum = Math.max(getSum(mat),sum);
                if(i!=j)reverseRow(mat,j);
            }
        }
*/
        return sum;
    }

    public static int getRowSumOfFirstHalf(List<List<Integer>> arr, int index) {
        int sum = 0;
        for (int i = 0; i < arr.size() / 2; i++) {
            sum += arr.get(index).get(i);
        }
        return sum;
    }

    public static int getRowSumOfSecondHalf(List<List<Integer>> arr, int index) {
        int sum = 0;
        for (int i = arr.size() - 1; i >= arr.size() / 2; i--) {
            sum += arr.get(index).get(i);
        }
        return sum;
    }

    public static int getColSumOfFirstHalf(List<List<Integer>> arr, int index) {
        int sum = 0;
        for (int i = 0; i < arr.size() / 2; i++) {
            sum += arr.get(i).get(index);
        }
        return sum;
    }

    public static int getColSumOfSecondHalf(List<List<Integer>> arr, int index) {
        int sum = 0;
        for (int i = arr.size() - 1; i >= arr.size() / 2; i--) {
            sum += arr.get(i).get(index);
        }
        return sum;
    }

    public static void rowSwap(List<List<Integer>> arr, int index1, int index2) {
        List<Integer> temp = arr.get(index1);
        arr.set(index1, arr.get(index2));
        arr.set(index2, temp);
    }

    public static int getSum(List<List<Integer>> arr) {
        int sum = 0;
        for (int i = 0; i < arr.size() / 2; i++) {
            for (int j = 0; j < arr.size() / 2; j++) {
                sum += arr.get(i).get(j);
                System.out.print(arr.get(i).get(j) + " + ");
            }
        }
        System.out.println("Sum = " + sum);
        return sum;
    }

    public static void columnSwap(List<List<Integer>> arr, int index1, int index2) {
        for (int i = 0; i < arr.size(); i++) {
            arr.get(i).set(index1, arr.get(i).get(index1) ^ arr.get(i).get(index2));
            arr.get(i).set(index2, arr.get(i).get(index1) ^ arr.get(i).get(index2));
            arr.get(i).set(index1, arr.get(i).get(index1) ^ arr.get(i).get(index2));
        }
    }

    public static void reverseColumn(List<List<Integer>> arr, int index) {
        for (int i = 0; i < arr.size() / 2; i++) {
            int temp = arr.get(i).get(index);
            arr.get(i).set(index, arr.get((arr.size() - 1) - i).get(index));
            arr.get(((arr.size() - 1) - i)).set(index, temp);
        }
    }

    public static void reverseRow(List<List<Integer>> arr, int index) {
        Collections.reverse(arr.get(index));
    }
}
