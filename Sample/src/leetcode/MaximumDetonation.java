package leetcode;
//@TAG : PRIORITY IMPLEMENTATION
//@CATEGORY : Graph
public class MaximumDetonation {
    public static void main(String[] args) {
        int[][] bombs = {{2,1,3},{6,1,4}};
        new MaximumDetonation().maximumDetonation(bombs);
    }
    boolean check(int[] bomb,int[] bomb1){
        int x = bomb[0];
        int y = bomb[1];
        int rad = bomb[2];
        int x1 = bomb1[0];
        int y1 = bomb1[1];
        int rad1 = bomb1[2];
        int distance =(int)Math.sqrt(((x1-x) * (x1-x)) + ((y1-y) * (y1-y)));
        return (rad + rad1) >= distance;
    }
    public int maximumDetonation(int[][] bombs) {
        int counter = 0;
        int m = bombs.length;
        int n = bombs[0].length;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j <m ; j++) {
                if(check(bombs[i],bombs[j]) && i!=j){
                    counter++;
                }
            }
            min = Math.min(counter,min);
        }
        return min;
    }
}
