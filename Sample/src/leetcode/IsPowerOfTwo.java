package leetcode;
//@CATEGORY : BitManipulation
public class IsPowerOfTwo {
    public static void main(String[] args) {
        System.out.println(new IsPowerOfTwo().isPowerOfTwo(Integer.parseInt(args[0])));
    }

    public boolean isPowerOfTwo(int n) {
        // if(n == 1) return true;
        // if(n <= 0) return false;
        // if(n % 2 != 0) return false;
        // while(n > 2){
        //     n /= 2;
        //     if(n%2 != 0) return false;
        // }
        // return true;
        return (n > 0 && ((n & (n - 1)) == 0));
    }
}
