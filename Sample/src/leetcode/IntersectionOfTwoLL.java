package leetcode;

import geeksforgeeks.Node;

public class IntersectionOfTwoLL {
    public static void main(String[] args) {
        Node node = new Node(2);
        node.next = new Node(6);
        node.next.next = new Node(4);
       /* node.next.next.next = new Node(4);
        node.next.next.next.next = new Node(5);
        node.next.next.next.next.next = new Node(6);*/
        Node node2 = new Node(1);
        node2.next = new Node(5);
        //node2.next.next = new Node(3);
        getIntersectionOfNode(node,node2);

    }
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode slow = headA;
        ListNode fast = headB;
        int count = 0;
        while(count < 4 && slow != null && fast != null){
            if(slow == fast) return slow;
            slow = slow.next;
            fast = fast.next;
            if(slow ==null) {
                slow = headB;
                count++;
            }
            if(fast == null) fast = headA;
        }
        return null;
    }
    public static Node getIntersectionOfNode(Node headA, Node headB) {
        Node slow = headA;
        int c = 0;
        Node fast = headB;
        while(c < 4 && slow != null && fast.next != null){
            if(slow == fast) return slow;
            //if(slow.next == null) slow = headB;
            boolean flag = true;
            if(fast.next.next == null){
                fast = headA;
                slow = headB;
                c ++;
            }
            slow = slow.next;
            fast = fast.next;
        }
        return null;
    }
}
