package leetcode;

import java.util.stream.IntStream;

public class SmallestValuePrime {
    public int smallestValue(int n) {
        return 0;
    }

    private boolean isPrime(int n) {
        return IntStream.rangeClosed(2, n).noneMatch(num -> n / num == 0);
    }
}
