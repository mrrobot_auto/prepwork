package leetcode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;

public class LongestStringChain {
    public static void main(String[] args) {
        String[] words = {"a", "b", "ba", "bca", "bda", "bdca"};
        new LongestStringChain().longestStrChain(words);
    }

    public int longestStrChain(String[] words) {
        Arrays.sort(words, Comparator.comparingInt(String::length));
        int count = 0;
        for (int i = 0; i < words.length; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (isChain(words[i], words[j])) {
                    count++;
                    break;
                }
            }
        }
        return count;
    }

    boolean isChain(String a, String b) {
        char[] charsA = a.toCharArray();
        char[] charsB = b.toCharArray();
        boolean fc = false;
        int ai = 0;
        int bi = 0;
        if (charsA.length + 1 == charsB.length) {
            while (ai < charsA.length && bi < charsB.length) {
                if (charsA[ai] == charsB[bi]) {
                    ai++;
                    bi++;
                    continue;
                } else if (!fc) {
                    fc = true;
                    bi++;
                    continue;
                }

                if (charsA[ai] != charsB[bi]) {
                    return false;
                }
            }
        } else return false;
        return true;
    }
}
