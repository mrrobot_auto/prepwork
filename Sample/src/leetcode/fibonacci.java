package leetcode;

public class fibonacci {
    public static void main(String[] args) {
        new fibonacci().fibo(6);
    }
    public int fib(int n) {
        int num1 = 0;
        int num2 = 1;
        int sum = 0;
        if(n == 1 || n ==2) return 1;
        for(int i = 2; i <= n; i++){
            sum = num1+num2;
            num1 = num2;
            num2 = sum;
        }
        return sum;
    }

    int fibo(int n){
        if(n == 1 || n == 2) {
            return 1;
        }
        return fibo(n-1) + fibo(n-2);
    }
}
