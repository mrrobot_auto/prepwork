package leetcode;

import java.util.Arrays;

public class swapNumsByK {
    public static void main(String[] args) {
        int i = 10;
        int j = 20;

        swap(i,j);
        /*int[] arr = {1, 2, 3, 4, 5, 6};
        swap(arr, 4);
        Arrays.stream(arr).forEach(System.out::println);*/
    }

    public static void swap(int[] a, int k) {
        if (k == 0) return;
        int n = a.length;
        for (int i = 0; i < n; i = i + k) {
            int left = i;
            int right = i + k - 1;
            if (right >= n) break;
            int temp;
            while (left < right) {
                temp = a[right];
                a[right] = a[left];
                a[left] = temp;
                left++;
                right--;
            }
        }
    }
    static void swap(int i, int j){
        i = i^j;
        j = i^j;
        i = i^j;
        System.out.println("i ="+i +" j="+ j);
    }
}
