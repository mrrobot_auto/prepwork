package leetcode;

import geeksforgeeks.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class LeafSimilar {
    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        //write your code here.
        List<Integer> r1L = new ArrayList<>();
        List<Integer> r2L = new ArrayList<>();
        getLeaves(root1,r1L);
        getLeaves(root2,r2L);
        return r1L.equals(r2L);

    }
    void getLeaves(TreeNode node, List<Integer> dp){
        if(node == null) return;
        getLeaves(node.left,dp);
        getLeaves(node.right,dp);
        if(node.right == null && node.left ==null){
            dp.add(node.val);
        }
    }
}
