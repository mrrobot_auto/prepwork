package leetcode;
//@CATEGORY : Kadanes Algorithm
public class KadanesAlgorithm {
    public static void main(String[] args) {
        maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4});
    }
    static int maxSubArray(int[] arr){
        int ans = 0;
        int sum = 0;
        for (int j : arr) {
            sum = Math.max(0, sum + j);
            ans = Math.max(ans, sum);
        }
        return ans;
    }
}
