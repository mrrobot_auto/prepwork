package leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class RotateImage {
    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        new RotateImage().rotate(matrix);
    }
    public void rotate(int[][] matrix) {
        Queue<int[]> q = new LinkedList<>();
        int m = matrix.length;
        int n = matrix[0].length;
        int k = m-1;
        for(int[] mat : matrix){
            int[] copy = new int[n];
            int index = 0;
            for(int l : mat){
                copy[index] = l;
                index++;
            }
            q.offer(copy);
        }
        q.forEach(o->{
            for(int in : o){
                System.out.print(in + " ");
            }
            System.out.println();
        });
        while(!q.isEmpty()){
            int[] res = q.poll();
            for(int i = 0; i < n; i++){
                matrix[i][k] = res[i];
            }
            k--;
        }
    }
}