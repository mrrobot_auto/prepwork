package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MinimumBribes {
    public static void main(String[] args) {
        minimumBribes(Arrays.asList(2, 1, 5, 3, 4));
                                //  1, 2, 3, 4, 5
    }

    public static void minimumBribes(List<Integer> q) {
        int count = 0;
        List<Integer> ints = new ArrayList<>();
        for (int i = 0; i < q.size(); i++) {
            if (Math.abs(q.get(i) - i - 1) >= 3) {
                System.out.println("Too chaotic");
                return;
            }
            if (Math.abs(q.get(i) - i - 1) > 0){
                if(!ints.contains(q.get(i))){
                    ints.add(q.get(i));
                }
                count++;
            }

        }
        System.out.println(count);

    }
}
