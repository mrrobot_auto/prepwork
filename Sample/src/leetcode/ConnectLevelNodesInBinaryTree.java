package leetcode;

import geeksforgeeks.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class ConnectLevelNodesInBinaryTree {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(3,2,1);
        new ConnectLevelNodesInBinaryTree().connect(node);
    }

    public void connect(TreeNode root)
    {
        // Your code goes here.
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        while(!q.isEmpty()){
            int n = q.size();
            for(int i = 0; i < n ; i++){
                TreeNode temp = q.poll();
                if(temp.left != null)
                    q.offer(temp.left);
                if(temp.right != null)
                    q.offer(temp.right);
                if(i == n-1) temp.nextRight = null;
                else temp.nextRight = q.peek();
            }
        }
    }
}
