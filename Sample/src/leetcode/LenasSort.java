package leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LenasSort {
    public static void main(String[] args) {

        List<Integer> arr = new ArrayList<>();
        new Random().ints(0 , 500).limit(5).forEach(arr::add);
        int comp = 5;
        int count =sort(arr, arr.size(), comp, 0);
        if(count == comp){
            arr.forEach(System.out::println);
        }else {
            System.out.println(-1);
        }

    }

    public static int sort(List<Integer> arr, int size, int comparisons, int count) {
        if (size <= 1) return 0;

        List<Integer> less = new ArrayList<>();
        List<Integer> more = new ArrayList<>();
        int pivot = arr.get(0);
        for (int i = 1; i < size; i++) {
            count++;
            if (pivot > arr.get(i)) {
                less.add(arr.get(i));
            } else {
                more.add(arr.get(i));

            }
        }
        sort(less, less.size(), comparisons, count);
        sort(more, more.size(), comparisons, count);
        System.out.println(less +" "+ pivot+ " "+ more);
        return count;


    }
}
