package leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class ShortestPathInBinaryMatrix {
    public static void main(String[] args) {
        new ShortestPathInBinaryMatrix().shortestPathBinaryMatrix(new int[][]{{0,0,0},{1,0,0},{1,1,0}});
    }

    public int shortestPathBinaryMatrix(int[][] grid) {
        int x = 0;
        int y = 0;
        if(grid[x][y] != 0 || grid[grid.length-1][grid[0].length-1] != 0) return -1;
        int[][] dirs = new int[][]{{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{0, 0});
        var level = 0;
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        visited[0][0] = true;
        while (!q.isEmpty()){
            int size = q.size();
            for(int i = 0 ; i < size ; i++){
                int[] dir = q.poll();
                if((dir[0] == grid.length-1) && (dir[1] == grid[0].length-1)){
                    return level+1;
                }
                for (int[] d: dirs) {
                    x = dir[0]+d[0];
                    y = dir[1]+d[1];
                    if(check(x,y,grid,visited)){
                        visited[x][y] = true;
                        q.offer(new int[]{x,y});
                    }
                }
            }
            level++;
        }
        return -1;
    }
    boolean check(int x, int y, int[][] grid, boolean[][] visited){
        return x>=0 && y>=0 && x < grid.length && y < grid[0].length && grid[x][y] == 0 && !visited[x][y];
    }


}
