package leetcode;

import java.util.*;

public class CountWords {
    public static void main(String[] args) {
        new CountWords().countWords(new String[]{"leetcode","amazing","is","as","is"},
                new String[]{"leetcode","amazing","is"});
    }
    public int countWords(String[] words1, String[] words2) {
        Set<String> set1 = new HashSet<>();
        Set<String> set2 = new HashSet<>();
        List<String> nkoBaba = new ArrayList<>();
        for(String s : words1){
            if(!set1.add(s) || nkoBaba.contains(s)){
                set1.remove(s);
                nkoBaba.add(s);
            }
        }
        for(String s : words2){
            if(!set2.add(s) || nkoBaba.contains(s)){
                set2.remove(s);
                nkoBaba.add(s);
            }
        }
        set1.retainAll(set2);
        return set1.size();
    }
}
