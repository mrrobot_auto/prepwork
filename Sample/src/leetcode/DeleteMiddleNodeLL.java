package leetcode;

public class DeleteMiddleNodeLL {
    public ListNode deleteMiddle(ListNode head) {
        deleteNode(getMiddle(head));
        return head;
    }
    public void deleteNode(ListNode node){
        ListNode temp = node.next;
        node.val = temp.val;
        node.next = temp.next;
    }
    public ListNode getMiddle(ListNode head){
        ListNode slow = head;
        ListNode fast = head;
        while(fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
}
