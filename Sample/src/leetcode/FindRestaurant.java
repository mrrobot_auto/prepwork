package leetcode;

import java.util.*;

public class FindRestaurant {
    public static void main(String[] args) {
        String[] s1 = {"Shogun", "Tapioca Express", "Burger King", "KFC"};
        String[] s2 = {"KFC", "Burger King", "Tapioca Express", "Shogun"};
        new FindRestaurant().findRestaurant(s1,s2);
    }

    public String[] findRestaurant(String[] list1, String[] list2) {
        List<String> l1 = new ArrayList<>(Arrays.asList(list1));
        List<String> l2 = new ArrayList<>(Arrays.asList(list2));
        List<String> l3 = new ArrayList<>(l1);
        List<String> l4 = new ArrayList<>(l2);
        l1.retainAll(l2);
        Map<Integer, List<String>> map = new TreeMap<>();
        for (String l : l1) {
            int a = l3.indexOf(l);
            int b = l4.indexOf(l);
            if (map.containsKey(Math.abs(a + b))) {
                map.get(Math.abs(a + b)).add(l);
            }else
                map.put(Math.abs(a + b), new ArrayList<>(List.of(l)));
        }
        int s = map.entrySet().iterator().next().getKey();
        String[] res = new String[map.get(s).size()];
        int count = 0;
        for (String str : map.get(s)) {
            res[count] = str;
            count++;
        }
        return res;
    }
}
