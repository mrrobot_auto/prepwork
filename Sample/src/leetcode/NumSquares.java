package leetcode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class NumSquares {
    public static void main(String[] args) {
        new NumSquares().numSquares(12);
    }

    public int numSquares(int n) {
        List<Integer> list = new ArrayList<>();
        int temp = n;
        if (n <= 0) {
            return 0;
        }
        while (n > 0) {
            while (!isPerfectSquare(n)) {
                n--;
            }
            list.add(n);
            n--;
        }
        list.sort(Comparator.reverseOrder());
        return minCoin(temp,list);
    }

    boolean isPerfectSquare(int n) {
        int root = (int) Math.sqrt(n);
        return root * root == n;
    }

    int minCoin(int n, List<Integer> a){
        if(n == 0) return 0;
        int ans = Integer.MAX_VALUE;
        for(int i : a){
            if((n-i)>=0){
                int subAns = minCoin(n-i,a);
                if(subAns != Integer.MAX_VALUE && subAns+1 < ans){
                    ans = subAns + 1;
                }
            }
        }
        return ans;
    }
}
