package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CountingSort {

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>(Arrays.asList(1,1,2,3,1));
        System.out.println(countingSort(ints));
    }
    public static List<Integer> countingSort(List<Integer> arr) {
        // Write your code here
        List<Integer> intlist = new ArrayList<>(Arrays.asList(new Integer[100]));
        Collections.fill(intlist,0);
        int v;
        return arr.parallelStream().map(i->intlist.set(i,intlist.get(i)+1)).collect(Collectors.toList());
        /*for(int i : arr){
            v = intlist.get(i);
            intlist.set(i, v + 1);
        }*/
    }
}
