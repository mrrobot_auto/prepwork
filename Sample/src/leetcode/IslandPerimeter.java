package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class IslandPerimeter {
    public static void main(String[] args) {
        int[][] grid = {{0, 1, 0, 0}, {1, 1, 1, 0}, {0, 1, 0, 0}, {1, 1, 0, 0}};
        new IslandPerimeter().islandPerimeter(grid);
    }

    public int islandPerimeter(int[][] grid) {
        int count = 0;
        int m = grid.length;
        int n = grid[0].length;
        boolean[][] visited = new boolean[m][n];
        boolean gotFirstOne = false;
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{0, 0});
        while (!q.isEmpty()) {
            int[] a = q.poll();
            int x = a[0];
            int y = a[1];
            getValidNeighbors(x, y, grid, m, n).forEach(s -> System.out.println(grid[s[0]][s[1]]));
            int val = grid[x][y];
            if (!visited[x][y] && val == 1) {
                count += 4 - getValidNeighbors(x, y, grid, m, n).stream().filter(o -> grid[o[0]][o[1]]==1).count();
            }
            if (!visited[x][y]) {
                getValidNeighbors(x, y, grid, m, n).forEach(q::offer);
            }
            visited[x][y] = true;
        }
        return count;
    }

    List<int[]> getValidNeighbors(int i, int j, int[][] nums, int m, int n) {
        List<int[]> list = new ArrayList<>();
        if ((i >= 0 && j + 1 >= 0) && (i <= m - 1 && j + 1 <= n - 1))
            list.add(new int[]{i, j + 1});
        if ((i + 1 >= 0 && j >= 0) && (i + 1 <= m - 1 && j <= n - 1))
            list.add(new int[]{i + 1, j});
        if ((i >= 0 && j - 1 >= 0) && (i <= m - 1 && j - 1 <= n - 1))
            list.add(new int[]{i, j - 1});
        if ((i - 1 >= 0 && j >= 0) && (i - 1 <= m - 1 && j <= n - 1))
            list.add(new int[]{i - 1, j});
        return list;
    }
}
