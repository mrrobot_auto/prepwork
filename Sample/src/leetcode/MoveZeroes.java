package leetcode;

public class MoveZeroes {
    public static void main(String[] args) {

    }
    public void moveZeroes(int[] nums) {
        int[] copy = new int[nums.length];
        int index = 0;
        for(int n : nums){
            if(n != 0){
                copy[index] = n;
                index++;
            }
        }
        index =0;
        for(int n: copy){
            nums[index] = n;
            index++;
        }

    }
}
