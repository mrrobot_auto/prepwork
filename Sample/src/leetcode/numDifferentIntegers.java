package leetcode;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

public class numDifferentIntegers {
    public static void main(String[] args) {
        new numDifferentIntegers().numDifferentIntegers("2393706880236110407059624696967828762752651982730115221690437821508229419410771541532394006597463715513741725852432559057224478815116557380260390432211227579663571046845842281704281749571110076974264971989893607137140456254346955633455446057823738757323149856858154529105301197388177242583658641529908583934918768953462557716z97438020429952944646288084173334701047574188936201324845149110176716130267041674438237608038734431519439828191344238609567530399189316846359766256507371240530620697102864238792350289978450509162697068948604722646739174590530336510475061521094503850598453536706982695212493902968251702853203929616930291257062173c79487281900662343830648295410");
    }

    public int numDifferentIntegers(String word) {
        Set<BigInteger> set = new HashSet<>();
        char[] chars = word.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : chars) {
            if (c >= '0' && c <= '9') {
                sb.append(c);
            } else {
                sb.append(" ");
            }
        }
        String s = sb.toString();
        sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ') {
                sb.append(s.charAt(i));
                if(i == s.length()-1){
                    set.add(new BigInteger(sb.toString()));
                    sb = new StringBuilder();
                }
            } else {
                if (!sb.toString().equals("")) {
                    set.add(new BigInteger(sb.toString()));
                    sb = new StringBuilder();
                }
            }
        }
        return set.size();
    }
}
