package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class FloodFill {
    public static void main(String[] args) {
        int[][] image = {{1, 1, 1}, {1, 1, 0}, {1, 0, 1}};
        new FloodFill().floodFill(image, 0, 0, 2);
    }

    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        int oldColor = image[sr][sc];
        int m = image.length;
        int n = image[0].length;
        boolean[][] res = new boolean[m][n];
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{sr, sc});
        while (!q.isEmpty()) {
            int[] i = q.poll();
            if (image[i[0]][i[1]] != oldColor || res[i[0]][i[1]]) {
                continue;
            }
            res[i[0]][i[1]] = true;
            image[i[0]][i[1]] = newColor;
            List<int[]> cons = getValidNeighbors(i[0], i[1], image, m, n, oldColor);
            for (int[] con : cons) {
                q.offer(con);
            }
        }
        return image;
    }

    List<int[]> getValidNeighbors(int i, int j, int[][] nums, int m, int n, int oldC) {
        List<int[]> list = new ArrayList<>();
        if ((i >= 0 && j + 1 >= 0) && (i <= m - 1 && j + 1 <= n - 1) && nums[i][j + 1] == oldC)
            list.add(new int[]{i, j + 1});
        if ((i + 1 >= 0 && j >= 0) && (i + 1 <= m - 1 && j <= n - 1) && nums[i + 1][j] == oldC)
            list.add(new int[]{i + 1, j});
        if ((i >= 0 && j - 1 >= 0) && (i <= m - 1 && j - 1 <= n - 1) && nums[i][j - 1] == oldC)
            list.add(new int[]{i, j - 1});
        if ((i - 1 >= 0 && j >= 0) && (i - 1 <= m - 1 && j <= n - 1) && nums[i - 1][j] == oldC)
            list.add(new int[]{i - 1, j});
        return list;
    }
}
