package leetcode;

public class LongestCommonPrefix {
    public static void main(String[] args) {
        new LongestCommonPrefix().longestCommonPrefix(new String[] {"flower", "flight", "flow"});
    }
    public String longestCommonPrefix(String[] strs) {
        String prefix = strs[0];
        for (String s : strs) {
            while(prefix.length()>0) {
                if (!s.startsWith(prefix)) {
                    prefix = prefix.substring(0, prefix.length() - 1);
                }else {
                    break;
                }
            }
            if(prefix.length() == 0){
                return "";
            }
        }
        return prefix;
    }
}
