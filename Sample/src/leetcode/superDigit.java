package leetcode;

public class superDigit {
    public static void main(String[] args) {
        System.out.println(superDigit(57469 * 100000));

    }

    /* public static BigInteger superDigit(BigInteger n){
         if(n.mod(BigInteger.valueOf(10)).equals(n)) return n;
         return superDigit(n.mod(BigInteger.valueOf(10)).add(superDigit(n.divide(BigInteger.valueOf(10)))));
     }*/
    public static int superDigit(int n) {
        if (n % 10 == n) return n;
        return superDigit(n % 10 + superDigit(n / 10));
    }
}
