package leetcode;

import java.util.Deque;
import java.util.LinkedList;

public class BaseBallGame {
    public static void main(String[] args) {
        new BaseBallGame().calPoints(new String[]{"5","-2","4","C","D","9","+","+"});
    }

    public int calPoints(String[] operations) {
        int res = 0;
        Deque<String> stack = new LinkedList<>();
        for (String o : operations) {
            switch (o) {
                case "C":
                    if (!stack.isEmpty()) {
                        stack.pop();
                    }
                    break;
                case "D":
                    if (!stack.isEmpty()) {
                        int oldScore = Integer.parseInt(stack.peek());
                        stack.push(String.valueOf(oldScore * 2));
                    }
                    break;
                case "+":
                    if (!stack.isEmpty()) {
                        int score1 = Integer.parseInt(stack.pop());
                        int score2 = 0;
                        if (!stack.isEmpty()) {
                            score2 = Integer.parseInt(stack.peek());
                        }
                        stack.push(String.valueOf(score1));
                        stack.push(String.valueOf(score1 + score2));
                    }
                    break;
                default:
                    stack.push(o);
                    break;
            }
        }
        while (!stack.isEmpty()) {
            res += Integer.parseInt(stack.pop());
        }
        return res;
    }
}
