package leetcode;

import java.util.LinkedList;
import java.util.Queue;
//@TAG : Rework for letters and special characters
public class longestSubstring {
    public static void main(String[] args) {
        System.out.println(new longestSubstring().lengthOfLongestSubstring("pwwkew"));

    }

    public int lengthOfLongestSubstring(String s) {
        Queue<Character> q = new LinkedList<>();
        char[] chars = s.toCharArray();
        var index = 0;
        var max = 0;
        int[] inv = new int[26];

        while(index<s.length()){
            q.offer(chars[index]);
            if(inv[chars[index] - 'a'] == 0){
                inv[chars[index] - 'a'] = 1;
                max = Math.max(max,q.size());
            }else {
                while(!q.isEmpty()){
                    char temp = q.poll();
                    if(temp == chars[index]){
                        break;
                    }
                }
            }
            index++;
        }
        return max;
    }
}
