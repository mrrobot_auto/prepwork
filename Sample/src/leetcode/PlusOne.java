package leetcode;

import java.util.ArrayList;
import java.util.List;

public class PlusOne {
    public static void main(String[] args) {
        new PlusOne().plusOne(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0});
    }

    public int[] plusOne(int[] digits) {
        long num = 0;
        for (int i = 0; i < digits.length; i++) {
            num = num * 10 + digits[i];
        }
        long res = num + 1;
        long nm1 = res;
        int count = 0;
        while (res != 0) {
            res = res / 10;
            count++;
        }
        int[] sum = new int[count];
        int index = count - 1;
        while (nm1 != 0) {
            var c = nm1 % 10;
            nm1 = nm1 / 10;
            sum[index] = (int) c;
            index--;
        }
        return sum;
    }
}
