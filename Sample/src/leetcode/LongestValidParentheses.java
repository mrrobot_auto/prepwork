package leetcode;

import java.util.*;

public class LongestValidParentheses {
    public static void main(String[] args) {
        new LongestValidParentheses().longestValidParentheses("(())()((");
    }

    public int longestValidParentheses(String s) {
        int l = 0;
        int r = s.length();
        int[] dp = new int[r+1];
        Arrays.fill(dp,-1);
        int t = para(s,l,r,dp);
        return t;
    }
    int para(String s, int l, int r, int[] dp){
        if(l>r) return 0;
        if(dp[r-l] != -1) return dp[r-l];
        if(!isValid(s.substring(l,r))){
            return Math.max(para(s,l+1,r,dp),para(s,l,r-1,dp));
        }
        return dp[r-l] = r-l;
    }
    public int longestValidParentheses0(String s) {
        int left = 0;
        int right = s.length();
        while (left<right) {
            if(isValid(s.substring(left, right))) break;
            left++;
            if(isValid(s.substring(left, right))) break;
            right--;
            if(isValid(s.substring(left, right))) break;
        }
        int diff = right-left;
        left = 0;
        right = s.length();
        while(!isValid(s.substring(left,right))){
            left++;
        }
        int maxLeft = right - left;
        left = 0;
        while(!isValid(s.substring(left,right))){
            right--;
        }
        int maxRight = right;
        int carry = Math.max(maxLeft,maxRight);
        return Math.max(diff,carry);
    }

    boolean isValid(String s) {
        char[] chars = s.toCharArray();
        Deque<Character> stack = new LinkedList<>();
        Map<Character,Character> map = new HashMap<>();
        map.put(')','(');
        for(char c : chars){
            if(!stack.isEmpty()){
                char p = stack.peek();
                char q = map.getOrDefault(c,'*');
                if(p == q){
                    stack.pop();
                }else{
                    stack.push(c);
                }
            }else {
                if(c == ')') return false;
                stack.push(c);
            }
        }
        return stack.isEmpty();
    }
}
