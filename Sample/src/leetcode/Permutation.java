package leetcode;

import java.util.*;

public class Permutation {
    static int fact = 1;
    static Set<int[]> set = new HashSet<>();

    public static void main(String[] args) {
       printPermutn("ABC","");
    }

    public static void getP(int[] arr, int fixed) {
        if (fixed >= arr.length) return;
//        int[] arrx = Arrays.stream(arr).toArray();
        for (int i = 0; i < arr.length; i++) {
            set.add(swap(arr, fixed, i));
        }
        fixed++;
        getP((int[]) set.toArray()[fixed], fixed);
    }


    public static void getPerms(int[] arr) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < getFact(arr.length) / arr.length; j++) {
                set.add(arr[i]);
                set.add(arr[getValidIndex(arr, j)]);
                swap(arr, getValidIndex(arr, j), getValidIndex(arr, j + 1));
            }
            set.forEach(System.out::println);
        }
    }

    public static int[] swap(int[] arr, int i, int j) {
        int[] swappedArr = Arrays.stream(arr).toArray();
        int temp = swappedArr[j];
        swappedArr[j] = swappedArr[i];
        swappedArr[i] = temp;
        return swappedArr;
    }

    public static int getValidIndex(int[] arr, int i) {
        if (i >= arr.length) {
            int j = Math.min(arr.length - i, i - arr.length);
            return j < 0 ? j * -1 : j;
        }
        return i;
    }

    public static int getFact(int i) {
        fact = fact * (i);
        i--;
        if (i != 1) getFact(i);
        return fact;
    }

    static void printPermutn(String str, String ans)
    {

        // If string is empty
        if (str.length() == 0) {
            System.out.println(ans + " ");
            return;
        }

        for (int i = 0; i < str.length(); i++) {

            // ith character of str
            char ch = str.charAt(i);

            // Rest of the string after excluding
            // the ith character
            String ros = str.substring(0, i) +
                    str.substring(i + 1);
            // Recurvise call
            printPermutn(ros, ans + ch);
        }
    }
    static List<String> list = new ArrayList<>();

    public static List<String> getPermutn(String str, String ans)
    {

        // If string is empty
        if (str.length() == 0) {
            list.add(ans);
            System.out.println(ans + " ");
            return list;
        }

        for (int i = 0; i < str.length(); i++) {

            // ith character of str
            char ch = str.charAt(i);

            // Rest of the string after excluding
            // the ith character
            String ros = str.substring(0, i) +
                    str.substring(i + 1);
            list.add(ros+ans+ch);
            // Recurvise call
            printPermutn(ros, ans + ch);
        }
        return list;
    }
}
