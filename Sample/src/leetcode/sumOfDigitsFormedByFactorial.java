package leetcode;

public class sumOfDigitsFormedByFactorial {
    public static void main(String[] args) {
        System.out.println(factorial(10));
        System.out.println(getSum(10));
    }
    static int factorial(int i){
        if(i == 0) return i;
        i = i*(i-1);
        factorial(i-1);
        return i;
    }
    static int getSum(int in) {
        int sum = 0;
        int res = 1;
        for (int i = 1; i <= in; i++) {
            res *= i;
        }
        char[] j = String.valueOf(res).toCharArray();
        for (char t : j) {
            int temp = Integer.parseInt(String.valueOf(t));
            sum += temp;
        }
        return sum;
    }
}
