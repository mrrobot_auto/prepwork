package leetcode;

import java.util.ArrayList;
import java.util.List;

public class CarPooling {
    public static void main(String[] args) {
        int[][] trips = {{3, 2, 7}, {3, 7, 9}, {8, 3, 9}};
        System.out.println(isTripPossible(trips, 11));
    }

    public static boolean isTripPossible(int[][] trips, int capacity) {
        List<Integer> res = new ArrayList<>();
        List<Integer> pickup = new ArrayList<>();
        List<Integer> drop = new ArrayList<>();
        for (int[] trip : trips) {
            res.add(trip[0]);
            pickup.add(trip[1]);
            drop.add(trip[2]);
            if (res.size() > 1) {
                if (drop.get(0) > pickup.get(1) && addList(res) > capacity) return false;
                res.remove(0);
                pickup.remove(0);
                drop.remove(0);
            }
        }
        return true;
    }

    public static int addList(List<Integer> a) {
        int res = 0;
        for (int i = 0; i < a.size() - 1; i++) {
            res = a.get(i) + a.get(i + 1);
        }
        return res;
    }
}
