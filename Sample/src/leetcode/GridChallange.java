package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GridChallange {
    public static void main(String[] args) {
        gridChallenge(Arrays.asList("fed","abc"));
    }

    public static String gridChallenge(List<String> grid) {
        // Write your code here
        char[] arr;
        List<String> temp = new ArrayList<>();
        for (int i = 0; i < grid.size(); i++) {
            arr = grid.get(i).toCharArray();
            Arrays.sort(arr);
            temp.add(i,String.valueOf(arr));
        }

        grid = new ArrayList<>(temp);
        temp.clear();
        temp = new ArrayList<>(grid);
        Collections.sort(temp);
        return grid.equals(temp)?"YES":"NO";

    }

}
