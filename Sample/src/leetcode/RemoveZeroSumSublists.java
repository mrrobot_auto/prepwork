package leetcode;

import geeksforgeeks.Node;

import java.util.ArrayList;
import java.util.List;

public class RemoveZeroSumSublists {
    public static void main(String[] args) {
        Node head = new Node(5);
        head.next = new Node(-3);
        head.next.next = new Node(-4);
        head.next.next.next = new Node(1);
        head.next.next.next.next = new Node(6);
        head.next.next.next.next.next = new Node(-2);
        head.next.next.next.next.next.next = new Node(-5);
        new RemoveZeroSumSublists().removeZeroSumSublists(head);
        //[5,-3,-4,1,6,-2,-5]
    }
    public Node removeZeroSumSublists(Node head) {
        Node res = null;
        Node resHead = null;
        Node first = head;
        Node second = head;
        boolean flag = false;
        List<Integer> removedItems = new ArrayList<>();
        while (first != null || second != null ) {
            int sum = first.value;
            while (second != null) {
                sum += second.value;
                if (sum != 0) {
                    flag = true;
                } else {
                    while(first != second){
                        first = first.next;
                    }
                    flag = false;
                    if (removedItems.contains(first.value)) {
                        removedItems.remove(Integer.valueOf(first.value));
                        flag = true;
                    } else {
                        removedItems.add(first.value);
                        break;
                    }
                }
                second = second.next;
            }
            if (flag) {
                if (res == null) {
                    res = new Node(first.value);
                    resHead = res;
                } else {
                    while (res.next != null) {
                        res = res.next;
                    }
                    res.next = new Node(first.value);
                }
            }
            first = first.next;
            if(first != null && first.next != null){
                second = first.next;
            }else {
                second = first;
            }
        }
        return resHead;
    }
}
