package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LonelyInteger {

    public static void main(String[] args) {
        List<Integer> arr = new ArrayList<>(Arrays.asList(0, 0, 1, 2, 1));
        System.out.println(lonelyinteger(arr));
    }
    public static int lonelyinteger( List<Integer> a) {
        // Write your code here
        int res = a.get(0);
        for(int i = 0 ; i < a.size()-1; i++){
            res = res ^ a.get(i+1);
        }
        return res;
    }
}
