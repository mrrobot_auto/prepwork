package leetcode;

import java.util.Deque;
import java.util.LinkedList;

public class AsteroidCollision {
    public static void main(String[] args) {
        new AsteroidCollision().asteroidCollision(new int[]{10, 2, -5});
    }

    public int[] asteroidCollision(int[] asteroids) {
        Deque<Integer> stack = new LinkedList<>();
        for (int i : asteroids) {
            if (stack.isEmpty()) {
                stack.push(i);
                continue;
            }
            if (i < 0) {
                int num = stack.peek();
                if (num < 0) {
                    stack.push(i);
                    continue;
                }
                if (Math.abs(i) > Math.abs(num)) {
                    stack.pop();
                    stack.push(i);
                } else if (Math.abs(i) == Math.abs(num)) {
                    stack.pop();
                }
            } else {
                stack.push(i);
            }
        }
        int[] res = new int[stack.size()];
        int index = stack.size() - 1;
        while (!stack.isEmpty()) {
            res[index] = stack.pop();
            index--;
        }
        return res;
    }
}
