package leetcode;

import geeksforgeeks.Node;

public class DeleteNode {
    public static void main(String[] args) {
        Node node = new Node(1);
        node.next = new Node(2);
        node.next.next = new Node(3);
        node.next.next.next = new Node(4);
        node.next.next.next.next = new Node(5);
        node.next.next.next.next.next = new Node(6);
        new DeleteNode().deleteNode(node.next.next);
    }
    public void deleteNode(Node node) {
        Node temp = node.next;
        node.value = temp.value;
        node.next = temp.next;
    }
}
