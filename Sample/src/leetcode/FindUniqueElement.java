package leetcode;

public class FindUniqueElement {
    public static void main(String[] args) {
        int[] test = {1, 2, 3, 3, 2, 1, 4, 5, 6, 5, 6, 7, 7, 6, 6};
        System.out.println(findOne(test));
    }

    static int findOne(int[] a) {
        int res = -1;
        for (int i : a) {
            if (count(a, i) == 0) res = i;
        }
        return res;
    }

    static int count(int[] a, int b) {
        int count = -1;
        for (int o : a) {
            if (o == b) count++;
        }
        return count;
    }
}
