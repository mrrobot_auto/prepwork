package leetcode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.IntStream;

public class ExcelSheetColumnTitle {
    public static void main(String[] args) {
        IntStream.rangeClosed(1,52).forEach(o -> System.out.print(new ExcelSheetColumnTitle().convertToTitle(o)+"|"));
    }

    public String convertToTitle(int columnNumber) {
        if (columnNumber < 27) {
            return con(columnNumber);
        }
        Deque<Integer> stack = new LinkedList<>();
        StringBuilder res = new StringBuilder();
        int pref = columnNumber;
        stack.add(columnNumber);
        do {
            boolean flag = pref % 26 == 0;
            pref = pref / 26;
            if(flag) pref -= 1;
            if (pref > 26) {
                stack.push(pref);
            }
        } while (pref > 26);
        res.append(con(pref));
        while (!stack.isEmpty()) {
            res.append(con(stack.pop() % 26));
        }
        return res.toString();
    }

    String con(int num) {
        if (num == 0) return "Z";
        char c = (char) (64 + num);
        return String.valueOf(c);
    }
}
