package leetcode;

public class ClimbingStairs {
    public static void main(String[] args) {
        System.out.println(new ClimbingStairs().climbStairs(Integer.parseInt(args[0])));
    }

    public int climbStairs(int n) {
        if(n == 1 || n ==2 || n == 3) return n;
        return fib(n+1);
    }
    public int fib(int n) {
        int num1 = 0;
        int num2 = 1;
        int sum = 0;
        for(int i = 2; i <= n; i++){
            sum = num1+num2;
            num1 = num2;
            num2 = sum;
        }
        return sum;
    }
}
