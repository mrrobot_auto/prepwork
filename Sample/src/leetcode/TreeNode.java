package leetcode;

public class TreeNode {
    public Integer val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(Integer val) {
        this.val = val;
        this.left = null;
        this.right = null;
    }

    public TreeNode(Integer val, Integer left, Integer right) {
        this.val = val;
        this.left = left != null ? new TreeNode(left) : null;
        this.right = right != null ? new TreeNode(right) : null;
    }
}
