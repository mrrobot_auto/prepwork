package leetcode;

import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;

public class CoinChange {
    public static void main(String[] args) {
        new CoinChange().coinChange(new int[]{186,419,83,408}, 6249);
    }

    /*
    [186,419,83,408]
    6249
     */
    public int coinChangeO(int[] coins, int amount) {
        Arrays.sort(coins);
        int sum = 0;
        int pointer = 1;
        int index = coins.length - 1;
        Deque<Integer> deq = new LinkedList<>();
        while (sum != amount) {

            if (sum < amount) {
                deq.push(coins[index]);
            } else {
                sum -= coins[index];
                index--;
            }
            if (index < 0) {
                pointer++;
                index = coins.length - pointer;
                sum = 0;
                if (pointer > coins.length) {
                    return -1;
                }

            }
            sum += coins[index];
        }
        return deq.size();
    }

    public int coinChange(int[] coins, int amount) {
        int res;
        int realAmount = amount;
        int count = 1;
        int ret = 0;
        Arrays.sort(coins);
        while (count < coins.length) {
            for (int i = coins.length - count; i >= 0; i--) {
                res = amount / coins[i];
                ret += res;
                amount -= res * coins[i];
                for (int j = coins.length-1; j >=0 ; j--) {
                    if(i != j) {
                        res = amount/coins[j];
                        ret += res;
                        amount -= res * coins[j];
                        if(amount<0) break;
                        if (amount == 0) return ret;
                    }
                }
                amount = realAmount;
                ret = 0;
            }
            count++;
        }

        return -1;
    }
}
