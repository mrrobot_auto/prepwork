package leetcode;

import java.util.*;

public class ThreeSum {
    public static void main(String[] args) {
        new ThreeSum().threeSum0(new int[]{-1,0,1,2,-1,-4});
    }
    public  List<List<Integer>> threeSum0(int[] nums) {
        Set<List<Integer>> res  = new HashSet<>();
        if(nums.length==0) return new ArrayList<>(res);
        Arrays.sort(nums);
        for(int i=0; i<nums.length-2;i++){
            int j =i+1;
            int  k = nums.length-1;
            while(j<k){
                int sum = nums[i]+nums[j]+nums[k];
                if(sum==0)res.add(Arrays.asList(nums[i],nums[j++],nums[k--]));
                else if ( sum >0) k--;
                else if (sum<0) j++;
            }

        }
        return new ArrayList<>(res);

    }
    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> list = new ArrayList<>();
        int len = nums.length;
        for (int i = 0; i < len; i++) {
            for (int j = i+1; j < len; j++) {
                if(i != j) {
                    for (int k = j+1; k < len; k++) {
                        if (j != k) {
                            if(nums[i] + nums[j] + nums[k] == 0){
                                System.out.println(i +" "+j+" "+k);
                                list.add(new ArrayList<>(List.of(nums[i],nums[j],nums[k])));
                            }
                        }
                    }
                }
            }
        }
        return list;
    }
}
