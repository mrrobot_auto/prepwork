package leetcode;

import java.util.Random;

public class LinkedListRandomNode {
    static class ListNode {
        private final int val;
        private final ListNode next;
        public ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
    static Random random = new Random();
    static ListNode head = null;
    static int size = 0;

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        addArrayToLinkedList(arr);
        getRandom();
        getRandom();
        getRandom();
        getRandom();
    }

    public static int get(int index) {
        ListNode localNode = head;
        for (int i = 0; i < index; i++) {
            localNode = localNode.next;
        }
        return localNode.val;
    }

    public static void getRandom() {
        int randomIndex = random.nextInt(size);
        System.out.print(get(randomIndex));
    }

    public static void addArrayToLinkedList(int[] arr) {
        for (int i : arr) {
            head = new ListNode(i, head);
            size++;
        }
    }

    public static void printLL() {
        ListNode local = head;
        while (local != null) {
            System.out.println(local.val);
            local = local.next;
        }
    }
}
