package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KthPalindrome {
    public static void main(String[] args) {
        new KthPalindrome().kthPalindrome(new int[]{36,29,871708115,917133972,452709562,7,326446953,100,959017606}, 7);
    }

    public long[] kthPalindrome(int[] queries, int intLength) {
        long cap = (long) Math.pow(10, (intLength - 1));
        long upCap = (long) Math.pow(10, (intLength));
        long[] res = new long[queries.length];
        int counter = 0;
        List<Long> q = new ArrayList<>();
        int[] copy = Arrays.copyOf(queries,queries.length);
        Arrays.sort(copy);
        int temp = 0;
        for(int u : copy){
            if(u>=upCap){
                break;
            }
            temp = u;
        }
        for (long l = cap; l < upCap; l++) {
            if (isPalindrome(l)) {
                q.add(l);
            }
            if(q.size()>=temp)break;
        }
        for (int i : queries) {
            if (counter >= queries.length) break;
            if (i >= upCap) {
                res[counter] = -1;
            } else res[counter] = q.get(i - 1);
            counter++;
        }
        return res;
    }

    public boolean isPalindrome(long x) {

        return x == this.reverse(x);
    }

    public long reverse(long x) {
        long temp = 0;
        while (x > 0) {
            long t = x % 10;
            x /= 10;
            temp = (temp * 10) + t;
        }
        return temp;
    }
}
