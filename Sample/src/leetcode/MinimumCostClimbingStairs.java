package leetcode;

public class MinimumCostClimbingStairs {
    public static void main(String[] args) {
        MinimumCostClimbingStairs min = new MinimumCostClimbingStairs();
        int[] camel = new int[]{10,15,20};
        min.minCostClimbingStairs(new int[]{1,100,1,1,1,100,1,1,100,1});
       }

    public int minimCostClimbingStairs(int[] cost) {
        int minCost = 0;
        int len = cost.length;
        int j = len - 1;
        while (j > 0) {
            int index = cost[j] < cost[j - 1] ? j : j - 1;
            System.out.println(j +" "+cost[j] +" ?<> "+ (j-1) +" "+cost[j-1] );
            System.out.println(index +" : "+cost[index]);
            minCost += cost[index];
            j = index -1;
        }
        return minCost;
    }

    public int miniCostClimbingStairs(int[] cost, int start) {
        int minCost = 0;
        int len = cost.length;
        int j = start;
        while (j < len-1) {
            int index = cost[j] < cost[j + 1] ? j : j + 1;
            System.out.println(j +" "+cost[j] +" ?<> "+ (j+1) +" "+cost[j+1] );
            System.out.println(index +" : "+cost[index]);
            minCost += cost[index];
            j = index + 1;
        }
        return minCost;
    }
    public int minCostClimbingStairs(int[] cost) {
        int n = cost.length;
        int dp[] = new int[n+1];
        dp[n] = 0;
        dp[n-1] = cost[n-1];
        for(int i = n-2; i>=0; i--) {
            dp[i] = cost[i] + Math.min(dp[i+1], dp[i+2]);
        }
        return Math.min(dp[0], dp[1]);
    }
}
