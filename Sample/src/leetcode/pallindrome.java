package leetcode;

public class pallindrome {
    public static void main(String[] args) {
        String p = "hgygsvlfcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcwflvsgygh";
        System.out.println(palindromeIndex(p));
    }

    public static int palindromeIndex(String s) {
        boolean res;
        int r = s.length() - 1;
        int l = 0;
        while (l <= r) {
            res = s.charAt(l) == (s.charAt(r));
            if (!res) {
                if (s.charAt(l + 1) == s.charAt(r)) {
                    return l;
                } else {
                    return r;
                }
            }
            l++;
            r--;
        }
        return -1;
    }

    public static boolean isPal(String s) {
        boolean res = true;
        int r = s.length() - 1;

        int l = 0;
        while (l <= r) {
            res = s.charAt(l) == (s.charAt(r));
            if (!res) return res;
            l++;
            r--;
        }
        return res;
    }

}

