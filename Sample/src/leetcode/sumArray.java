package leetcode;

public class sumArray {
    public static void main(String[] args) {
        System.out.println("hello");
        int[] arr = {5, 5, 9, 7, 2, 16, 10};
        System.out.print("out -->" + sol(arr));
    }

    public static int sol1(int[] arr) {
        int outSum = 0;
        int inSum = 0;
        for (int i = 0; i < arr.length; i++) {
            outSum += arr[i];
            inSum = 0;
            for (int j = arr.length - 1; j > i + 1; j--) {
                inSum += arr[j];
                if (outSum == inSum && j - i == 2) return j - 1;
            }
        }
        return -1;
    }

    public static int sol(int[] arr) {
        int leftPointer = 0,
                leftSum = arr[leftPointer];
        int rightPointer = arr.length - 1;
        int rightSum = arr[rightPointer];

        while (leftSum != rightSum && rightPointer - leftPointer != 2) {
            if (leftSum < rightSum) {
                leftPointer = leftPointer + 1;
                leftSum = leftSum + arr[leftPointer];
            }
            if (rightSum < leftSum) {
                rightPointer = rightPointer - 1;
                rightSum = rightSum + arr[rightPointer];
            }
        }
        System.out.println(rightPointer - 1);
        System.out.println(rightSum);
        return 0;
    }
}
