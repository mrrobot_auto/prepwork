package leetcode;

import java.util.ArrayList;
import java.util.List;

public class Palindrome {
    List<String> list = new ArrayList<>();
    public static void main(String[] args) {
        System.out.println(isPalindrome("aabaa"));
    }

    public static void part(String s, List<String> li){

    }

    public static boolean isPalindrome(String s) {
        int left = 0;
        int right = s.length() - 1;
        while (left < right) {
            if(s.charAt(left) != s.charAt(right)){
                return false;
            }
            left ++;
            right --;
        }
        return true;
    }
}
