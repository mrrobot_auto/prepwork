package leetcode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SentenceSimilarity {
    public boolean isSentenceSimilarity(String[] words1, String[] words2, List<List<String>> pairs) {
        // write your code here
        int ptr1 = 0;
        int ptr2 = 0;
        Map<String,String> pairMap = new HashMap<>();
        for(List<String> l : pairs){
            pairMap.put(l.get(0),l.get(1));
        }
        while(ptr1<words1.length && ptr2<words2.length){
            if(words1[ptr1].equals(words2[ptr2])){
                ptr1++;
                ptr2++;
                continue;
            }
            if(!(words1[ptr1].equals(pairMap.get(words2[ptr2]))|| words2[ptr2].equals(pairMap.get(words1[ptr1])))){
                return false;
            }

            ptr1++;
            ptr2++;
        }
        return true;
    }
}
