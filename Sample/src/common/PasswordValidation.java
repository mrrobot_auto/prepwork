package common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidation {
	
	private static Pattern pattern ;
	private static Matcher matcher;
	private static final String PASSWORD_PATTERN ="((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";


	static{
		pattern = Pattern.compile(PASSWORD_PATTERN);
	}

	public static boolean validate(String passwrd) {
		System.out.println(passwrd + "::" + pattern);
		matcher = pattern.matcher(passwrd);
		return matcher.matches();
	}
	
	public static void main(String[] args) {
	boolean isValid=	validate("mkyong11AB##");
	System.out.println("Is Passwrd Valid::"+isValid);
	
	
	/*System.out.println(Pattern.matches("[amn]", "abcd"));//false (not a or m or n)  
	System.out.println(Pattern.matches("[amn]", "a"));//true (among a or m or n)  
	System.out.println(Pattern.matches("[amn]", "ammmna"));//false (m and a comes more than once) 
	
	
	System.out.println(Pattern.matches("[amn]+", "abcd"));//false (not a or m or n)  
	System.out.println(Pattern.matches("[amn]+", "a"));//true (among a or m or n)  
	System.out.println(Pattern.matches("[amn]+", "ammmna"));//false (m and a comes more than once)
	
	System.out.println(Pattern.matches("[amn]*", "abcd"));//false (not a or m or n)  
	System.out.println(Pattern.matches("[amn]*", "a"));//true (among a or m or n)  
	System.out.println(Pattern.matches("[amn]*", "ammmna"));//false (m and a comes more than once)
*/
}
	
	
	
}
