package common;

import java.util.LinkedList;

public class Emp {
private String name;
private String id;
private int salary;


public Emp(String name, String id, int salary) {
	super();
	this.name = name;
	this.id = id;
	this.salary = salary;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public int getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary = salary;
}
// Used to print student details in main()
public String toString()
{
    return this.id + " " + this.name +
                       " " + this.salary;
}

}
