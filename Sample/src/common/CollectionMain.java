package common;

import java.util.ArrayList;
import java.util.Collections;

public class CollectionMain {
	public static void main(String[] args) {
		ArrayList<Emp> empList = new ArrayList<Emp>();
		empList.add(new Emp("name1", "id1", 30));
		empList.add(new Emp("name2", "id2", 60));
		empList.add(new Emp("name3", "id3", 50));
		
		System.out.println("****UNSORTED*****");
		for (int i = 0; i < empList.size(); i++) {
        System.out.println(empList.get(i));
		}
		
		Collections.sort(empList, new ComparatorInt());
		System.out.println("***SORTED****");
		for (int i = 0; i < empList.size(); i++) {
	        System.out.println(empList.get(i));
			}
	}
}
