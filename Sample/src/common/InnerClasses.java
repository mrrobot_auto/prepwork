package common;

import java.util.ArrayList;
import java.util.List;

public class InnerClasses {
    public static void main(String[] args) {
        //anonymous classes
        B ob = () -> System.out.println("cal");
        ob.show();
        //member classes
        A a = new A();
        A.C ac = a.new C();
        ac.foo();

        A.D ad = new A.D();
        ad.bar();
    }

}
//single abstract method interface
//functional interface
@FunctionalInterface
interface B {
    void show();

}
class A{
    class C{
        public void foo(){
            List<String> l = new ArrayList();
            System.out.println("inner");
        }
    }
    static class D{
        public void bar(){
            System.out.println("static here");
        }
    }
}


