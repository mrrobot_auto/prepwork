package common;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ListToMapStream {
    public static void main(String[] args) {
        Employee e1 = new Employee("1psujay", "g", 16);
        Employee e2 = new Employee("pooja", "pee", 15);
        Employee e3 = new Employee("1POOJA", "H", 1);
        ArrayList<Employee> li = new ArrayList<>();
        li.add(e1);
        li.add(e2);
        li.add(e3);
        Map<Integer,Employee> map = li.stream().collect(Collectors.toMap(
                Employee::getAge,
                Function.identity()));
        System.out.println(map);

        Map<Integer, List<Employee> >
                multimap = li
                .stream()
                .collect(
                        Collectors
                                .groupingBy(
                                        Employee::getAge));
                                        /*Collectors
                                                .mapping(
                                                        Function.identity(),
                                                        Collectors
                                                                .toList())));*/
        System.out.println(multimap);
    }
}
