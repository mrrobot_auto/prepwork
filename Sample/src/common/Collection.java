package common;

import java.util.Arrays;
import java.util.HashMap;
/**Sort a list of String**/
public class Collection {
public static void main(String[] args) {/*
	
	HashMap<Employee, Address> map = new HashMap<Employee, Address>();
	Employee e1 = new Employee();
	Address a1 = new Address();
	map.put(e1, a1);
	Employee e2 = new Employee();
	map.put(e2, a1);
	System.out.println(map.size());
	System.out.println(map.get(e2));

*/
	

		String[] strNames = new String[] { "John", "alex", "Chris", "williams", "Mark", "Bob" };

		Arrays.sort(strNames);
		for (String strName : strNames) {
			System.out.println(strName);
		}
		String[] strWithInt = new String[] { "1", "2", "4", "5", "3", "8" };

		Arrays.sort(strWithInt);
		for (String strName : strWithInt) {
			System.out.println(strName);
		}
		/**As String checks the ASCII value and check so it wont work**/
		String[] strWithInt1 = new String[]{"1", "22", "4", "5", "3", "8"};
		 System.out.println("String having integer value sorting ::");
        Arrays.sort(strWithInt);
        for(String strName:strWithInt){
            System.out.println(strName);
            }
		
        
        System.out.println("String having integer value sorting RIGHT!!! ::");
        int[] values = new int[strWithInt1.length];
		for (int i=0;i<strWithInt1.length;i++) {
			values[i]=Integer.parseInt(strWithInt1[i]);
		}
		Arrays.sort(values);
		for (int strName : values) {
			System.out.println(strName);
		}
}
}
