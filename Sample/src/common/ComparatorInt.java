package common;

import java.util.Comparator;

public class ComparatorInt implements Comparator<Emp>{

	@Override
	public int compare(Emp arg0, Emp arg1) {
		return arg0.getSalary()-arg1.getSalary();
	}
	
	/*@Override
	public int compare(Emp arg0, Emp arg1) {
		return arg0.getName().compareTo(arg1.getName());
	}*/
}
