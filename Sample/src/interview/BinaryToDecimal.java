package interview;

public class BinaryToDecimal {

	public static void main(String[] args) {
		int binaryNum = 1000;
		int decimal = convertToDecimal(binaryNum);
		System.out.println("Binary::"+binaryNum+":Decimal::"+decimal);
	}

	private static int convertToDecimal(int binaryNum) {
		int digit=0;
		int decimalNumber=0;
		int power=0;
		while (binaryNum > 0) {
        digit =binaryNum%10;
        decimalNumber+=digit*Math.pow(2, power);
        power++;
        binaryNum=binaryNum/10;
		}
		return decimalNumber;
	}
}
