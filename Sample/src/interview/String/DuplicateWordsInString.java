package interview.String;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

//find duplicate + count
public class DuplicateWordsInString {
	public static void main(String[] args) {
		String[] str = { "abc", "prt", "ABC", "lmn", "abc", "prt" };
		Map<String, Integer> mapArr = new HashMap<String, Integer>();
		for (String lst : str) {
			if (mapArr.containsKey(lst.toLowerCase())) {
				mapArr.put(lst, mapArr.get(lst) + 1);
			} else {
				mapArr.put(lst, 1);
			}
		}
		System.out.println("Count with key :" + mapArr);

		/*
		  Set<String> entList=mapArr.keySet();		 
		  for(String lst :entList){		 
		  if(mapArr.get(lst) >1){ 
		  System.out.println("Duplicates::"+lst); 
		  } }
		 */

		Iterator<Entry<String, Integer>> iterator = mapArr.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, Integer> first = iterator.next();
			String key = first.getKey();
			int value = first.getValue();
			if (value > 1) {
				System.out.println("Duplicates::" + key);
			}
		}
	}
}
