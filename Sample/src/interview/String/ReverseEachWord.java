package interview.String;

public class ReverseEachWord {
	public static void main(String[] args) {
		String input = "Hi I am pratibha";
		String reversed=reverse(input);
		System.out.println("Reversed::"+reversed);

	}

	private static String reverse(String input) {
	String output="";
	String[] inp=input.split(" ");
	for(int i=0;i<inp.length;i++){
		output+=reverseInp(inp[i]);
	}
	
	return output;
	}

	private static String reverseInp(String str) {
		String revr ="";
		for(int i=str.length()-1;i>=0;i--){
			revr+=str.charAt(i);
		}
		return revr+" ";
	}

}
