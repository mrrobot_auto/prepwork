package interview.String;

import java.util.Arrays;

public class StringAnagram {
public static void main(String[] args) {
	String str1="anagram";
	String str2="margana";
	boolean isTrue=checkAnagram(str1,str2);
	System.out.println("Y/N   "+isTrue);
}

private static boolean checkAnagram(String str1, String str2) {

	char[] chr1=str1.toLowerCase().toCharArray();
	char[] chr2=str2.toCharArray();
	if(chr1.length!=chr2.length){
		return false;
	}
	Arrays.sort(chr1);
	Arrays.sort(chr2);
	return Arrays.equals(chr2,chr1);
}
}
