package interview.String;

public class StringReversalWithoutAPI {

	public static void main(String[] args) {
		String str=
			"Pikku";
		String reversed =reversedString(str);
		System.out.println("Reversed::"+reversed);
	}

	private static String reversedString(String str) {
		char[] charArr=str.toCharArray();
		String reverse="";
		for(int i=charArr.length-1;i>=0;i--){
			//System.out.println(charArr[i]);
			reverse+=charArr[i];
		}
		return reverse;
	}
}
