package interview.String;

public class RemoveCharacterFromString {
public static void main(String[] args) {
	String str="aditya";
	String org=str;
	char remove ='d';
	String ret = removeCharFromString(str,remove);
	System.out.println("Str::"+ret);
}

private static String removeCharFromString(String str, char remove) {
	int indexToBeRemoved = 0;
	char[] charArr = str.toCharArray();
	for(int i=0;i<charArr.length;i++){
		if(charArr[i]==remove){
			indexToBeRemoved=i;
			break;
		}
	}
	StringBuilder strbld = new StringBuilder(str);
	strbld.deleteCharAt(indexToBeRemoved);
	//org=str.
	return strbld.toString();
}
}
