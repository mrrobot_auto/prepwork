package interview.String;

public class NullISStringOrObject {static {  
	       System.out.println("Static");  
	    }  
	 
	   {  
	        System.out.println("Instance");  
	  
	    }  
	  
	   NullISStringOrObject() {  
	       System.out.println("Constructor");  
	    }  
	  
	    public static void main(String[] args) {  
	 
	    	
	    	String str = null ;
	    	Object obj = null;
	    	System.out.println("Str::"+str+"::obj::"+obj);
	    	NullISStringOrObject sbe = new NullISStringOrObject();  
	    }  
}