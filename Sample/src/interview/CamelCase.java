package interview;

public class CamelCase {
    public static void main(String[] args) {
        String inputStr = "mY nAme is Purvasha";

        StringBuffer stringBuffer = new StringBuffer();
        inputStr = inputStr.toLowerCase();
        //convert entire string to lowercase
        String[] arr = inputStr.split(" ");
        for (int i = 0; i < arr.length; i++) {
            String word = arr[i];
            char[] chars = word.toCharArray();
            chars[0] = Character.toUpperCase(chars[0]);
            word = String.valueOf(chars);
            stringBuffer.append(word);
            if(i != arr.length -1){
                stringBuffer.append(" ");
            }
        }

        System.out.println(stringBuffer);
    }
}
