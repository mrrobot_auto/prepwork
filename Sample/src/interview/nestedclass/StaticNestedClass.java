package interview.nestedclass;

public class StaticNestedClass {

	static int data = 70;
	static class StaticInnerClass{
		void msg(){
			System.out.println("Static inner class"+data);
		}
	}
	public static void main(String[] args) {
		StaticInnerClass inn = new StaticNestedClass.StaticInnerClass();
		inn.msg();
	}
}
