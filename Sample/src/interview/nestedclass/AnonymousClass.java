package interview.nestedclass;

public class AnonymousClass {
	public static void main(String[] args) {
		Person person = new Person() {

			@Override
			void msg() {
				System.out.println("MSG Implemented ...........");
			}
		};
		person.msg();
	}

}

abstract class Person {
	abstract void msg();
}
