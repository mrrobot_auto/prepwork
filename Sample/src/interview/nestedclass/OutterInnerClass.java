package interview.nestedclass;

public class OutterInnerClass {
	private int data=20;
	class Inner{
		void msg(){
			System.out.println("Data :"+data);
		}
	}
	
public static void main(String[] args) {
	OutterInnerClass outer = new OutterInnerClass();
	OutterInnerClass.Inner inn = outer.new Inner();
	inn.msg();
}

}
