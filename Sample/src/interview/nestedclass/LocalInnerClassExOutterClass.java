package interview.nestedclass;

public class LocalInnerClassExOutterClass {
private int data =30;
private void msg() {
class LocalInnerClass{
	public void localmeth() {
		System.out.println(data);
	}

}
LocalInnerClass obj = new LocalInnerClass();
obj.localmeth();
}
public static void main(String[] args) {
	LocalInnerClassExOutterClass outer = new LocalInnerClassExOutterClass();
	outer.msg();
}
}
