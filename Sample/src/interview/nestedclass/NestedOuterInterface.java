package interview.nestedclass;

public interface NestedOuterInterface {
void show();
	public interface InnerInterface{
		void message();
	}

class NestedClass implements NestedOuterInterface,InnerInterface{

	@Override
	public void show() {
		
	}

	@Override
	public void message() {
      System.out.println("Message implemented");		
	}
	
}
}
