package interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringReversalUsingRecursion {
public static void main(String[] args) {
	String name ="pratibha";

    // reverse(name);
	String reverse=reverseUsingString(name);
	System.out.println("Reversed Number::"+reverse);
}

private static String reverseUsingString(String name) {
	String reverse="";
	if(name.length()==1){
		return name;
	}
	else{
		reverse+=name.charAt(name.length()-1)+reverseUsingString(name.substring(0,name.length()-1));
	}
	return reverse;
}

private static void reverse(String name) {

	char[] array = name.toCharArray();
	
	System.out.println(" Name ::"+array);

	List<Character> chrList = new ArrayList<Character>();
	for(int i=array.length-1;i>=0;i--){
		chrList.add(array[i]);
		if(i==0){
			break;
		}
	}

	System.out.println("Reversed Name ::"+chrList);
}
}
