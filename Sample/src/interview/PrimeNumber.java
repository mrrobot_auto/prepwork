package interview;

import java.util.stream.IntStream;

public class PrimeNumber {
    public static void main(String[] args) {
	/*int num=7;
	boolean isPrime =isPrime(num);
	System.out.println(num+" is Prime T/F? "+isPrime);*/
        IntStream.rangeClosed(2, 100).forEach(number -> {
            boolean isPrime = IntStream
                    .rangeClosed(2, (int) Math.sqrt(number))
                    .noneMatch(i -> number % i == 0);
            if (isPrime) {
                System.out.println(number);
            }
        });
    }

    private static boolean isPrime(int num) {
        boolean isPrime = true;
        if (num > 2) {
            for (int i = 2; i < num; i++) {
                if ((num % i == 0)) {
                    isPrime = false;

                }
            }
        }
        return isPrime;
    }
}
