package interview;

public class FactorialOfNumber {
public static void main(String[] args) {
	

	
	
	int fact = 5;
	int factorial = factorial(fact);
	System.out.println("Factorial for number "+fact+" is "+factorial);
	
}

private static int factorial(int fact) {
	int sum =1;
	while(fact>0){
		sum = fact*sum;
		fact = fact-1;
	}
	
	return sum;
}

}  
