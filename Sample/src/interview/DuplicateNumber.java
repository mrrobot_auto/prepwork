package interview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DuplicateNumber {
public static void main(String[] args) {
	List<Integer> listOfint = new ArrayList<Integer>();
	for(int i=0;i<20;i++){
		listOfint.add(i);
	}
	listOfint.add(5);
	listOfint.add(7);
	//int duplicate=findDupicate(listOfint);//Using HASHMap
	//int duplicate=findDupicateUsingForLoop(listOfint);//Using HASHMap
	int duplicate=findDupicateUsingSet(listOfint);
	System.out.println("First Duplicate::"+duplicate);
	
}

private static int findDupicateUsingSet(List<Integer> listOfint) {
	int duplicate=0;
	Set<Integer> set = new HashSet<Integer>();
	for(Integer num:listOfint){
		if(set.add(num)==false){
			duplicate=num;
			break;
		}
	}
	return duplicate;
}

private static int findDupicateUsingForLoop(List<Integer> listOfint) {
	 
	int duplicate=0;
	for(int i=0;i<listOfint.size();i++){
		for(int j=i+1;j<listOfint.size();j++){
			if(listOfint.get(i).equals(listOfint.get(j))){
				duplicate=listOfint.get(j);
			}
		}
	}
	return duplicate;
}

private static int findDupicate(List<Integer> listOfint) {
	int duplicate=0;
	HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
	for(Integer list:listOfint){
		if(map.containsKey(list)){
			duplicate=list;
			break;
		}else
		{
			map.put(list, 1);
			continue;
		}
	}
		
	return duplicate;
}
}
