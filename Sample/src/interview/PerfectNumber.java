package interview;
//i.e equl to sum of its proper divisors 
//6 : 1,2,3
public class PerfectNumber {
	public static void main(String[] args) {
		int num = 6;
		System.out.println("Is "+num+" Perfect?");
		boolean isPerfect= isPerfect(num);
		System.out.println("T/F::"+isPerfect);
	}

	private static boolean isPerfect(int num) {
		 
		boolean isPerfect=false;
		int sum=1;
		for(int i=2;i<num;i++){
		if(num%i==0){
			sum+=i;
		}
		}
		if(sum==num){
			isPerfect=true;
		}
		return isPerfect;
	}

}
