package interview.inheritance;

public class Child extends Parent {

	/*public Child(int j) {
		super(5);
		System.out.println("Child Constructor............");
	}*/

	/*@Override
	public void msg() {
		super.msg();
		System.out.println("msg......");
	}*/
	public void msg1() {
		super.msg();
		System.out.println("msg1......");
	}

	public static void main(String[] args) {
		//Parent parent = new Child(6);
		//parent.msg();
		Child parent = new Child();
		parent.msg();

	}

	public static void methodExc() {

		try {
			try {
				int[] intlst = {2, 3};
				System.out.println(intlst[3]);
				System.out.println("CODE A");

			} catch (ArrayIndexOutOfBoundsException ar) {
				System.out.println("CODE B");
				//throw new Exception();

			} catch (Exception ex) {
				System.out.println("CODE C");
			} finally {
				System.out.println("CODE D");
			}

		} finally {

		}

	}
}
