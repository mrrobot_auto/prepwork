package interview;

import java.util.HashMap;
import java.util.Map.Entry;

public class RepeatedNumber {
public static void main(String[] args) {
	int rep = 1234512367;
	HashMap<Integer, Integer> map = new HashMap<Integer,Integer>();
	while(rep>0){
		int lst = rep%10;
		if(map.containsKey(lst)){
			map.put(lst, map.get(lst)+1);
		}
		else
			map.put(lst, 1);
		
		rep =rep/10;
		
	}
	
	for(Entry<Integer, Integer> lst: map.entrySet()){
		System.out.println(lst.getKey()+":::"+lst.getValue());
		
	}
}
}
