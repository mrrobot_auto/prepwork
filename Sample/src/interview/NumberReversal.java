package interview;

public class NumberReversal {
    public static void main(String[] args) {
        int num = numberReverse(12345);
        System.out.println("Number Reversed" + num);
    }

    private static int numberReverse(int num) {
        int reverse = 0;
        while (num != 0) {
            reverse = reverse * 10 + num % 10;
            num = num / 10;

        }
        return reverse;
    }
}
