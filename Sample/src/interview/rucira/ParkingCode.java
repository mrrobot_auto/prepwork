package interview.rucira;// You parked your car in a parking lot and want to compute the total cost of the ticket. The billing
// rules are as follows:
//
// The entrance fee of the car parking lot is 2; The first full or partial hour costs 3; Each
// successive full or partial hour (after the first) costs 4.  You entered the car parking lot at time
// E and left at time L. In this task, times are represented as strings in the format "HH:MM" (where
// "HH" is a two-digit number between 0 and 23, which stands for hours, and "MM" is a two-digit number
// between 0 and 59, which stands for minutes).
//
// Write a function:
//
// class Solution { public int solution(String E, String L); }
//
// that, given strings E and L specifying points in time in the format "HH:MM", returns the total cost
// of the parking bill from your entry at time E to your exit at time L. You can assume that E
// describes a time before L on the same day.
//
// For example, given "10:00" and "13:21" your function should return 17, because the entrance fee
// equals 2, the first hour costs 3 and there are two more full hours and part of a further hour, so
// the total cost is 2 + 3 + (3 * 4) = 17. Given "09:42" and "11:42" your function should return 9,


/*
 * Click `Run` to execute the snippet below!
 */

import java.util.ArrayList;
import java.util.Scanner;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */

class ParkingCode {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the entry time: ");
        String E = sc.next();
        System.out.println("Enter the exit time: ");
        String L = sc.next();
        sc.close();
        int res = solution(E, L);
        System.out.print("total cost " + res);
    }

    public static int solution(String E, String L) {
        int totalParkingCost = 0;
        String[] entryTime = E.split(":");

        int entryHour = Integer.parseInt(entryTime[0]);
        int entryMinutes = Integer.parseInt(entryTime[1]);

        String[] exitTime = L.split(":");

        int exitHour = Integer.parseInt(exitTime[0]);
        int exitMinutes = Integer.parseInt(exitTime[1]);

        int totalHours = 0;
        if (entryHour != exitHour) {
            totalHours = exitHour - entryHour;
        }
        int totalMinutes = 0;

        if (exitMinutes - entryMinutes != 0) totalMinutes = exitMinutes - entryMinutes;

        if (totalMinutes < 0) totalMinutes *= -1;


        if (entryHour > 0 && totalHours != 0) totalParkingCost += 5;

        if (totalHours > 0) {
            totalParkingCost = totalParkingCost + (4 * (totalHours - 1)) == 0 ?
                    totalParkingCost : totalParkingCost + (4 * (totalHours - 1));
        }
        if (totalMinutes > 0) totalParkingCost += 4;


        return totalParkingCost;


    }
}
